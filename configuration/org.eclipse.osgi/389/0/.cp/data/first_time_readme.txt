VERSION OF DEBUG ADAPTER FW:
This version of Multi-Node Energy Profiler requires debug 
adapter FW version S1015B_wireless_stk_firmware_package_1v3p3b927.emz 
or later. Studio will prompt you that a newer version of FW 
is available for your adapter when your development kit 
connects, and Mult-Node Energy Profiler will also notify you 
if you start a capture with a development kit that does not 
have this version of FW.

DATA SYNCHRONIZATION:
This version of Multi-Node Energy Profiler in conjunction 
with the debug adapter synchronizes data received from multiple 
devices. IMPORTANT: USB synchronization between two device 
captures is performed at the host level and provides an accuracy
of +/- 1 mS. USB synchronization will only be applied if the 
time difference between two connected USB devices is greater 
than the value in the preference Window->Preferences->
Network Analyzer->Capture Configuration:T0 Correction threshold. 
This setting is 2s by default. To ensure USB devices are 
synchronized, be sure to connect your USB devices spaced 
by 2 seconds. For IP connections synchronization is performed 
at the debug adapter with an accuracy of +/- 200 uS. Therefore, 
communication between devices with times faster than the 
synchronized tolerance may be observed as out of order.

CODE CORRELATION:
Code correlation is only supported on EFM32 and EFR32 devices. 
EFM8 devices do not support this feature. EFM32 SDK examples 
and demos will work as provided by the SDKs, but EFR32 devices 
will not. Minor changes are needed to EFR32 examples for the 
code correlation feature to work. For more details on how to 
modify example source code, see the help section “Customer 
Hardware and Software Design Information->Software Configuration-> 
EFR32 Software Configuration.”
