import sys
import Studio
from Studio.template import *
from Studio.project import *
from Studio.workbench import *

# this variable is passed in from arguments
templateId = sys.argv[1]

# Get the part and boards args
options = None
if len(sys.argv) > 2:
	options = {
		OPTION_PART_ID : sys.argv[2],
		OPTION_BOARD_IDS : sys.argv[3:]
	}

print 'Importing project',templateId

template = findEntitiesById(templateId)[0]

project = createTemplateProject(template, options, None)

initOpened = project.getName()
if template.getProperty(INITIALLY_OPENED_RESOURCE) != None:
	initOpened = initOpened+"/"+template.getProperty(INITIALLY_OPENED_RESOURCE)

if not PERSPECTIVE_DEVELOPMENT == getCurrentPerspective():
	switchPerspective(PERSPECTIVE_DEVELOPMENT)

openResource(initOpened)
