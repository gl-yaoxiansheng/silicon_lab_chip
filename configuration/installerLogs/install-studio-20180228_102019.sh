STUDIO=c:/SiliconLabs/SimplicityStudio/v2/

DEST=D:\silicon\

( cd "$STUDIO" && 
./studio_c -application org.eclipse.equinox.p2.director -nosplash --launcher.suppressErrors -profile studio \
	-profileproperties org.eclipse.update.install.features=true\
	-destination "$DEST"/studio -bundlepool "$DEST"/studio/common -shared "$DEST"/studio/common/p2 \
	-repository "jar:file:/D:/silicon/offline/archive/control/stacks/MosKRNX/krnx_archive.zip!/updates/" \
	-repository "https://devtools.silabs.com/studio/v4/updates/" \
	-repository "file:/D:/silicon/p2/org.eclipse.equinox.p2.core/cache/" \
	-repository "https://devtools.silabs.com/studio/v4/control/stacks/PrivateBLEMeshGA/updates/" \
	-repository "https://devtools.silabs.com/studio/v4/control/stacks/PublicGA/updates/" \
	-repository "file:/D:/silicon/" \
	-repository "https://devtools.silabs.com/studio/v4/control/stacks/PrivateGA/updates/" -installIUs \
com.silabs.stack.znet.v6.2.feature.feature.group/6.2.0.0,
)
