STUDIO=c:/SiliconLabs/SimplicityStudio/v2/

DEST=D:\silicon\

( cd "$STUDIO" && 
./studio_c -application org.eclipse.equinox.p2.director -nosplash --launcher.suppressErrors -profile studio \
	-profileproperties org.eclipse.update.install.features=true\
	-destination "$DEST"/studio -bundlepool "$DEST"/studio/common -shared "$DEST"/studio/common/p2 \
	-repository "https://devtools.silabs.com/studio/v4/updates/" \
	-repository "file:/D:/silicon/p2/org.eclipse.equinox.p2.core/cache/" \
	-repository "https://devtools.silabs.com/studio/v4/control/stacks/PublicGA/updates/" \
	-repository "jar:file:/D:/silicon/offline/archive/control/stacks/MosKRNX/krnx_metadata_archive.zip!/updates/" -installIUs \
com.silabs.metadata.solutions.feature.feature.group/4.0.5.201710241856-83,com.silabs.metadata.examples.feature.feature.group/4.0.2.201710201448-74,com.silabs.metadata.ffd.feature.feature.group/4.0.12.201709260524-102,com.silabs.metadata.efm8.feature.feature.group/4.0.10.201802220248-120,com.silabs.metadata.efm32.feature.feature.group/4.0.29.201802211246-154,com.silabs.metadata.efr32.feature.feature.group/4.0.36.201802200423-168,com.silabs.installer.usbxpress_winusb_driver.feature.feature.group/4.0.2.201802191904-87,com.silabs.metadata.hwtools.feature.feature.group/4.0.54.201802190141-216,com.silabs.apack.inspect_usbxpress.feature.feature.group/4.0.10.201802161231-98,com.silabs.metadata.c8051.feature.feature.group/4.0.5.201709280958-82,com.silabs.apack.runtime.feature.feature.group/4.0.1.201712031318-76,com.silabs.metadata.iots.feature.feature.group/4.0.3.201710210846-70,com.silabs.metadata.common.feature.feature.group/4.0.4.201802160305-95,com.silabs.metadata.ezr32.feature.feature.group/4.0.5.201710240028-86,com.silabs.metadata.emxxx.feature.feature.group/4.0.6.201710211303-83,com.silabs.metadata.sensors.feature.feature.group/4.0.5.201707200109-45,com.silabs.apack.usb.feature.feature.group/4.0.4.201712031736-85,
)
