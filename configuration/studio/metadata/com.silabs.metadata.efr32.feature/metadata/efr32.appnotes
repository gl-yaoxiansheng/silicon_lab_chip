<?xml version="1.0" encoding="UTF-8"?>
<contentItems>
  <contentItemData label="AN933: EFR32 Minimal BOM" itemURI="asset://efr32/appnote/AN933/AN933.pdf" uiPath="APPNOTES|EFR32|AN933">
    <property key="partCompatibility" value="mcu.arm.efr32.*"/>
    <description>The purpose of this application note is to illustrate bill-of-material (BOM)-optimized solutions for sub-GHz and 2.4 GHz applications using the EFR32 Wireless Gecko Portfolio.</description>
  </contentItemData>
  <contentItemData label="AN1042: Using the Silicon Labs Bluetooth® Stack in Network Co-Processor Mode" itemURI="asset://efr32/appnote/AN1042/AN1042.pdf" uiPath="APPNOTES|EFR32|AN1042">
    <property key="partCompatibility" value="mcu.arm.efr32.*"/>
    <description>This document is an essential reference for everybody developing a system for the Silicon Labs Wireless Gecko products using the Silicon Labs Bluetooth Stack in Network Co-Processor (NCP) mode. The document covers the C language application development flow and the use of BGTool and Simplicity Studio, then walks through the examples included in the stack and shows how to customize them.</description>
  </contentItemData>
  <contentItemData label="AN1045: Bluetooth® Over-the-Air Device Firmware Update for EFR32xG1 and BGM11x Series Products" itemURI="asset://efr32/appnote/AN1045/AN1045.pdf" uiPath="APPNOTES|EFR32|AN1045">
    <property key="partCompatibility" value="mcu.arm.efr32.*"/>
    <description>This application note describes the legacy OTA (Over-the-Air) firmware update mechanism used in the Silicon Labs Blue Gecko Bluetooth SDK (Software Development Kit) for EFR32xG1 SoCs and BGM11x and BGM121/BGM123 modules.</description>
  </contentItemData>
  <contentItemData label="AN1046: Bluetooth® Radio Frequency Physical Layer Evaluation" itemURI="asset://efr32/appnote/AN1046/AN1046.pdf" uiPath="APPNOTES|EFR32|AN1046">
    <property key="partCompatibility" value="mcu.arm.efr32.*"/>
    <description>This application note provides an overview of how to perform Bluetooth-based radio frequency (RF) physical layer (PHY) evaluation with Bluetooth-enabled EFR32xG system-on-chips (SoCs) and BGM/MGM modules using Silicon Labs’ software tools and dedicated firmware.</description>
  </contentItemData>
  <contentItemData label="AN1053: Bluetooth® Device Firmware Update over UART for EFR32xG1 and BGM11x Series Products" itemURI="asset://efr32/appnote/AN1053/AN1053.pdf" uiPath="APPNOTES|EFR32|AN1053">
    <property key="partCompatibility" value="mcu.arm.efr32.*"/>
    <description>This application note describes the legacy UART DFU (Device Firmware Update) mechanism used in the Silicon Labs Bluetooth SDK (Software Development Kit) for EFR32xG1 SoCs and BGM11x and BGM121/BGM123 modules. UART DFU enables deployment of firmware updates to devices in the field, making it possible to introduce new features or other changes after a product has been launched.</description>
  </contentItemData>
  <contentItemData label="AN1085: Using the Gecko Bootloader with Silicon Labs Connect" itemURI="asset://efr32/appnote/AN1085/AN1085.pdf" uiPath="APPNOTES|EFR32|AN1085">
    <property key="partCompatibility" value="mcu.arm.efr32.*"/>
    <description>This application note includes detailed information on using the Silicon Labs Gecko Bootloader with the Silicon Labs Connect stack, part of the Silicon Labs Flex SDK (Software Development Kit). It supplements the general Gecko Bootloader implementation information provided in UG266: Silicon Labs Gecko Bootloader User’s Guide. If you are not familiar with the basic principles of performing a firmware upgrade or want more information about upgrade image files, refer to UG103.6: Application Development Fundamentals: Bootloading.</description>
  </contentItemData>
  <contentItemData label="AN1086: Using the Gecko Bootloader with the Silicon Labs Bluetooth® Applications" itemURI="asset://efr32/appnote/AN1086/AN1086.pdf" uiPath="APPNOTES|EFR32|AN1086">
    <property key="partCompatibility" value="mcu.arm.efr32.*"/>
    <description>This application note includes detailed information on using the Silicon Labs Gecko Bootloader with Silicon Labs Bluetooth applications. It supplements the general Gecko Bootloader implementation information provided in UG266: Silicon Labs Gecko Bootloader User’s Guide. If you are not familiar with the basic principles of performing a firmware upgrade or want more information about upgrade image files, refer to UG103.6: Application Development Fundamentals: Bootloading.</description>
  </contentItemData>
</contentItems>
