<?xml version="1.0" encoding="UTF-8"?>
<contentItems>
  <contentItemData label="AN0828 Capacitive Sensing Library Overview" itemURI="asset://c8051/appnote/AN0828/AN0828.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f970 mcu.8051.c8051.f990 mcu.8051.efm8.sb1"/>
    <description>&lt;p>
    AN0828 provides a detailed overview of the structure and components of the capacitive sensing library.  The library includes:&lt;/p>
&lt;ul>
&lt;li>	Scanning routines that buffer raw samples&lt;/li>
&lt;li>	Filters that process and shape raw data&lt;/li>
&lt;li>	Baselining routines that maintain an expected inactive sensor capacitive value&lt;/li>
&lt;li>	Threshold detection routines that qualify touches using application-defined thresholds, choosing between filtered data types dynamically depending on the system’s signal to noise ratio&lt;/li>
&lt;li>	Interference characterization routines used to control touch qualification&lt;/li>
&lt;li>	Low power control routines that transition the system between active mode and low power “sleep” mode&lt;/li>&lt;/ul>

This document discusses the configuration of the library as well as the APIs and data structures the library exposes and maintains for access by an application layer of firmware.
    </description>
  </contentItemData>
  <contentItemData label="AN0829 Capacitive Sensing Library Configuration Guide" itemURI="asset://c8051/appnote/AN0829/AN0829.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f970 mcu.8051.c8051.f990 mcu.8051.efm8.sb1"/>
    <description>This document describes configuration strategies and recommendations for using the capacitive sensing library.  AN0829 includes a walkthrough demonstrating how to use the Capacitive Sense Profiler tool in conjunction with the library, ways to maximum performance for the library, how to configure hardware and sliders, and detailed discussions of the Capacitive Sense Profiler tool.</description>
  </contentItemData>
  <contentItemData label="AN111 Using C8051Fxxx in 5 Volt Systems" itemURI="asset://c8051/appnote/AN111/an111.pdf" uiPath="APPNOTES|C8051|AN111">
    <property key="partCompatibility" value="mcu.8051"/>
    <description>This application note describes how to incorporate Silicon Lab's C8051Fxxx family of devices in existing 5 V systems. When using a 3 volt device in a 5 volt system, provide a 3 V power supply, consider compatibility between the 5 V device driving a 3 V input, and consider compatibility between the 3 V device driving a 5 V input.</description>
  </contentItemData>
  <contentItemData label="AN114 Hand Soldering Tutorial for the Fine Pitch QFP Devices" itemURI="asset://c8051/appnote/AN114/an114.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051"/>
    <description>This document is intended to help designers create an initial prototype system using Silicon Lab's TQFP and LQFP devices where surface mount assembly equipment is not readily available. This application note assumes that the reader has at least basic hand soldering skills for through-hole soldering. The example presented will be the removal,cleanup and replacement of a TQFP with 48 leads and 0.5 mm lead pitch.</description>
  </contentItemData>
  <contentItemData label="AN119 Calculating Settling Time For Switched Capacitor ADC's" itemURI="asset://c8051/appnote/AN119/an119.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051"/>
    <description>Many of Silicon Lab's devices feature an on-chip SAR analog-to-digital converter (ADC). These ADC's use a sample capacitor that is charged to the voltage of the input signal that is used by the SAR logic to perform its data conversion. Due to the ADC's sample capacitance, input impedance, and the external input circuitry, there will be a settling time required for the sample capacitor to assume the measured input signal voltage. This application note describes a method for calculating the required settling time for good ADC measurements and methods to achieve meeting settling time requirements.</description>
  </contentItemData>
  <contentItemData label="AN124 Pin Sharing Techniques for the C2 Interface" itemURI="asset://c8051/appnote/AN124/an124.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051"/>
    <description>Some Silicon Labs 8051 devices include an on-chip Silicon Labs 2-Wire (C2) Interface for in-system programming and debugging. Two signals are associated with the C2 Interface: C2 Clock (C2CK) and C2 Data (C2D). To preserve package pins, the C2CK and C2D pins also function as the /RST and a GPIO pins, respectively. To enable in-system programming and/or debugging, external resistors are typically used to isolate C2 traffic from the external system. The isolation configuration depends on the application function associated with the /RST and GPIO pins on the target device. This application note discusses C2 isolation configurations for each user function. If /RST and the GPIO pins are not occupied by user functions, no isolation circuitry is needed.</description>
  </contentItemData>
  <contentItemData label="AN136 Production Programming Options for Silicon Labs Devices" itemURI="asset://c8051/appnote/AN136/an136.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051"/>
    <description>This application note gives an overview of production programming options available for Silicon Labs devices. The two main categories for programming uninitialized devices are in-system programming and pre-programming. The most appropriate type of programming depends on the number of devices being programmed and whether access is available to the debug pins (JTAG or C2 interface) of the device. Once devices have been programmed once, they may be updated from application code using the UART or another interface.</description>
  </contentItemData>
  <contentItemData label="AN203 C8051Fxxx Printed Circuit Board Design Notes" itemURI="asset://c8051/appnote/AN203/AN203.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051"/>
    <description>The tips and techniques included in this application note will help to ensure successful printed circuit board (PCB) design. Problems in design can result in noisy and distorted analog measurements, error-prone digital communications, latch-up problems with port pins, excessive electromagnetic interference (EMI), and other undesirable system behavior.</description>
  </contentItemData>
  <contentItemData label="AN367 Understanding Capacitive Sensing Signal to Noise Ratios and Setting Reliable Thresholds" itemURI="asset://c8051/appnote/AN367/AN367.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f970 mcu.8051.c8051.f990"/>
    <description>This application note is written to assist embedded designers as they create and compare the performance of capacitive sensing systems. These systems require a thorough understanding of signal, noise, and threshold levels to insure proper operational conditions of the end product.</description>
  </contentItemData>
  <contentItemData label="AN376 Effects of ESD Protection Devices on Capacitive Sensing Performance" itemURI="asset://c8051/appnote/AN376/AN376.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f970 mcu.8051.c8051.f990 mcu.8051.efm8.sb1"/>
    <description>Silicon Laboratories' capacitive sensing techniques are compatible with external ESD protection methods. Designers should be aware of parasitic parameters in these circuits which affect capacitive sensing performance when implementing these protection methods. While most of the methods affect sensing performance in some way, in almost all cases an acceptable balance between performance, protection, and cost is easily achievable. This application note describes several typical ESD protection techniques, their mode of operation, and the effects of commonly-introduced parasitic parameters on the different methods of capacitive sensing provided by Silicon Laboratories including charge rate measurement/relaxation oscillator (RO) methods and direct capacitive measurement (CS0).</description>
  </contentItemData>
  <contentItemData label="AN431 C8051F93x and C8051F90x Software Porting Guide" itemURI="asset://c8051/appnote/AN431/AN431.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f930 mcu.8051.c8051.f912 mcu.8051.c8051.f911"/>
    <description>Microcontrollers in the C8051F93x-C8051F90x product families are designed to be software compatible with each other. This allows a single code base to be developed which targets MCUs that range from 8 to 64 kB in program memory size. This porting guide is designed to help the programmer easily port code between devices in the product family or write processor-independent software that can execute on any device in the product family. This porting guide highlights differences between the various MCUs in the product family.</description>
  </contentItemData>
  <contentItemData label="AN447 Printed Circuit Design Notes for Capacitive Sensing with the CS0 Module" itemURI="asset://c8051/appnote/AN447/AN447.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f970 mcu.8051.c8051.f990 mcu.8051.efm8.sb1"/>
    <description>Circuits with capacitive sense elements are affected in many ways by their environment. Packaging, sensor design, signal routing, power system design, and ambient conditions, among other factors, can change the response of capacitive sensors in different, sometimes unexpected, ways. Techniques have been developed to enhance the responsiveness of capacitive sensors by increasing sensitivity and reducing noise. Failure to exercise care in the design of capacitive sensing applications will result in the unintended consequences of lower sensitivity and higher noise. Employing the techniques described in this application note will help ensure that systems with capacitive sensing elements deliver optimal results. This note describes techniques used in the application of the Capacitive Sensing (CS0) converter.</description>
  </contentItemData>
  <contentItemData label="AN507 Low Power Capacitive Sensing" itemURI="asset://c8051/appnote/AN507/AN507.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f970 mcu.8051.c8051.f990"/>
    <description>In many applications, mechanical push-button switches and potentiometers are being replaced by capacitive switches, sliders, and control wheels to implement functions such as contrast, volume control, and power on. This application note discusses how the user interface of ultra low power applications can be designed using capacitive sensing technology.</description>
  </contentItemData>
  <contentItemData label="AN522 Touchless Lavatory Appliance Design" itemURI="asset://c8051/appnote/AN522/AN522.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f930 mcu.8051.c8051.f912 mcu.8051.c8051.f911 mcu.8051.c8051.f960 mcu.8051.c8051.f990"/>
    <description>The Si1141 is a single-channel, reflectance-based proximity detector with an integrated ambient light sensor and can be used as a touchless sensor IC in lavatory appliances. As a proximity detector, the Si1141 has two infrared photodiodes, flexible ADC integration time, and multiple signal ranges. In addition to the configurable ADC sensitivity configuration settings, the Si1141 can dynamically drive an irLED with anywhere from 6 to 400 mA. This high level of flexibility in the ADC, photodiode choice, and irLED drive strength all contribute to allow the end product to operate under various ambient light conditions, required for lavatory applications. The Si1141 achieves low power by taking proximity measurements using a single 25.6 us irLED pulse instead of performing the multiple pulses commonly used in discrete photodiode approaches. In addition to the Si1141 Proximity and Ambient Light Sensor, Silicon Labs offers an array of low-power 8051 MCU devices (C8051F9xx series).</description>
  </contentItemData>
  <contentItemData label="AN530 C8051F90x/91x/98x/99x Software Porting Guide" itemURI="asset://c8051/appnote/AN530/AN530.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f912 mcu.8051.c8051.f911 mcu.8051.c8051.f990"/>
    <description>The C8051F91x-90x and C8051F99x-98x device families have many similarities, and code written for one platform can easily be ported to the other family with minor modifications. The C8051F91x-90x family has devices that range from 16 kB to 8 kB flash, and the C8051F99x-98x family has devices that range from 8 kB to 2 kB flash. This application note is designed to help the programmer easily port code between the two product families and primarily highlights differences between the device families. Two devices will be used as the reference parts for comparison, the C8051F902 and C8051F996. Differences within each product family will also be indicated where applicable.</description>
  </contentItemData>
  <contentItemData label="AN723 Porting Considerations from C8051F330-5 and C8051F336/37/38/39 to C8051F39x/37x" itemURI="asset://c8051/appnote/AN723/AN723.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f330 mcu.8051.c8051.f336 mcu.8051.c8051.f390"/>
    <description>This application note highlights the differences among the C8051F330-5, C8051F336-9, and C8051F39x/37x MCUs. These devices are designed to be code-compatible and pin-compatible, and thus require very minor changes when porting firmware and hardware between MCUs in these three families. The 'F39x/37x is the newest among the three families and includes an enhanced feature set in addition to all of the peripherals of the 'F330-5 and 'F336-9.</description>
  </contentItemData>
  <contentItemData label="AN789 Porting Considerations from C8051F34x to C8051F38x" itemURI="asset://c8051/appnote/AN789/AN789.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f340 mcu.8051.c8051.f380"/>
    <description>This application note highlights the differences between the C8051F34x and C8051F38x microcontrollers. These devices are designed to be code-compatible and pin-compatible, and thus require very minor changes when porting firmware and hardware between MCUs in these two families. The C8051F38x is the newest family and includes an enhanced feature set in addition to all of the peripherals of the C8051F34x. The C8051F38x devices should function as a drop-in replacement for the C8051F34x devices in most applications.</description>
  </contentItemData>
  <contentItemData label="AN807 Recertifying a Customized Windows HCK Driver Package" itemURI="asset://c8051/appnote/AN807/AN807.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051.c8051.f320 mcu.8051.c8051.f340 mcu.8051.c8051.f380"/>
    <description>This application note discusses the Windows Hardware Quality Labs (WHQL) or WinQual process for recertifying a customized HCK driver package. Driver recertification is required any time a certified driver is modified, which includes the modified VCP or USBXpress driver output from the AN220: C8051F32x and CP210x USB Driver Customization software.</description>
  </contentItemData>
  <contentItemData label="AN0821 Simplicity Studio™ C8051F85x Walkthrough" itemURI="asset://c8051/appnote/AN0821/AN0821.pdf" uiPath="APPNOTES|C8051">
    <property key="partCompatibility" value="mcu.8051"/>
    <description>This document provides a step-by-step walkthrough that shows how to develop a basic embedded project using Simplicity Studio (IDE and Configurator) to run on the C8051F850 ToolStick Daughter Card (TOOLSTICK850-DC).</description>
  </contentItemData>
</contentItems>
