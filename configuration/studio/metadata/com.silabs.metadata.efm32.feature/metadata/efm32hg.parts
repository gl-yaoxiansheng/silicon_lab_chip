<?xml version="1.0" encoding="UTF-8"?>
<collection>
  <group parent="mcu.arm.efm32" name="hg">
    <property key="referenceManuals" value="asset://efm32/reference/efm32hg_reference_manual.pdf"/>
    <property key="hardwareConfigData" value="asset://efm32/configurator/efm32hg/EFM32HG"/>
    <property key="keywords" value=""/>
    <property key="memory.FLASH.addr" value="0"/>
    <property key="memory.FLASH.access" value="rx"/>
    <property key="memory.FLASH.pageSize" value="0x400"/>
    <property key="memory.RAM.addr" value="0x20000000"/>
    <property key="memory.RAM.access" value="rwx"/>
    <property key="ARMCore" value="CORTEX_M0+"/>
    <property key="hardware.part.familyType" value="Happy"/>

    <part name="efm32hg108f32" label="EFM32HG108F32">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG108.json asset://efm32/debug/hg/EFM32HG108F32_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg108_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg108_errata.pdf"/>
      <property key="hardware.part.featureSet" value="108"/>
      <property key="hardware.part.flash" value="32K"/>
    </part>

    <part name="efm32hg108f64" label="EFM32HG108F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG108.json asset://efm32/debug/hg/EFM32HG108F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg108_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg108_errata.pdf"/>
      <property key="hardware.part.featureSet" value="108"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32hg110f32" label="EFM32HG110F32">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG110.json asset://efm32/debug/hg/EFM32HG110F32_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg110_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg110_errata.pdf"/>
      <property key="hardware.part.featureSet" value="110"/>
      <property key="hardware.part.flash" value="32K"/>
    </part>

    <part name="efm32hg110f64" label="EFM32HG110F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG110.json asset://efm32/debug/hg/EFM32HG110F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg110_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg110_errata.pdf"/>
      <property key="hardware.part.featureSet" value="110"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32hg210f32" label="EFM32HG210F32">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG210.json asset://efm32/debug/hg/EFM32HG210F32_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg210_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg210_errata.pdf"/>
      <property key="hardware.part.featureSet" value="210"/>
      <property key="hardware.part.flash" value="32K"/>
    </part>

    <part name="efm32hg210f64" label="EFM32HG210F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG210.json asset://efm32/debug/hg/EFM32HG210F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg210_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg210_errata.pdf"/>
      <property key="hardware.part.featureSet" value="210"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32hg222f32" label="EFM32HG222F32">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG222.json asset://efm32/debug/hg/EFM32HG222F32_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg222_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg222_errata.pdf"/>
      <property key="hardware.part.featureSet" value="222"/>
      <property key="hardware.part.flash" value="32K"/>
    </part>

    <part name="efm32hg222f64" label="EFM32HG222F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG222.json asset://efm32/debug/hg/EFM32HG222F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg222_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg222_errata.pdf"/>
      <property key="hardware.part.featureSet" value="222"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32hg308f32" label="EFM32HG308F32">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG308.json asset://efm32/debug/hg/EFM32HG308F32_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg308_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg308_errata.pdf"/>
      <property key="hardware.part.featureSet" value="308"/>
      <property key="hardware.part.flash" value="32K"/>
    </part>

    <part name="efm32hg308f64" label="EFM32HG308F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG308.json asset://efm32/debug/hg/EFM32HG308F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg308_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg308_errata.pdf"/>
      <property key="hardware.part.featureSet" value="308"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32hg309f32" label="EFM32HG309F32">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG309.json asset://efm32/debug/hg/EFM32HG309F32_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg309_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg309_errata.pdf"/>
      <property key="hardware.part.featureSet" value="309"/>
      <property key="hardware.part.flash" value="32K"/>
    </part>

    <part name="efm32hg309f64" label="EFM32HG309F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG309.json asset://efm32/debug/hg/EFM32HG309F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg309_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg309_errata.pdf"/>
      <property key="hardware.part.featureSet" value="309"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32hg310f32" label="EFM32HG310F32">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG310.json asset://efm32/debug/hg/EFM32HG310F32_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg310_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg310_errata.pdf"/>
      <property key="hardware.part.featureSet" value="310"/>
      <property key="hardware.part.flash" value="32K"/>
    </part>

    <part name="efm32hg310f64" label="EFM32HG310F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG310.json asset://efm32/debug/hg/EFM32HG310F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg310_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg310_errata.pdf"/>
      <property key="hardware.part.featureSet" value="310"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32hg321f32" label="EFM32HG321F32">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG321.json asset://efm32/debug/hg/EFM32HG321F32_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg321_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg321_errata.pdf"/>
      <property key="hardware.part.featureSet" value="321"/>
      <property key="hardware.part.flash" value="32K"/>
    </part>

    <part name="efm32hg321f64" label="EFM32HG321F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG321.json asset://efm32/debug/hg/EFM32HG321F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg321_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg321_errata.pdf"/>
      <property key="hardware.part.featureSet" value="321"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32hg322f32" label="EFM32HG322F32">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG322.json asset://efm32/debug/hg/EFM32HG322F32_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg322_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg322_errata.pdf"/>
      <property key="hardware.part.featureSet" value="322"/>
      <property key="hardware.part.flash" value="32K"/>
    </part>

    <part name="efm32hg322f64" label="EFM32HG322F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG322.json asset://efm32/debug/hg/EFM32HG322F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg322_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg322_errata.pdf"/>
      <property key="hardware.part.featureSet" value="322"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32hg350f32" label="EFM32HG350F32">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG350.json asset://efm32/debug/hg/EFM32HG350F32_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg350_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg350_errata.pdf"/>
      <property key="hardware.part.featureSet" value="350"/>
      <property key="hardware.part.flash" value="32K"/>
    </part>

    <part name="efm32hg350f64" label="EFM32HG350F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/hg/EFM32HG350.json asset://efm32/debug/hg/EFM32HG350F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32hg350_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32hg350_errata.pdf"/>
      <property key="hardware.part.featureSet" value="350"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>
  </group>
</collection>
