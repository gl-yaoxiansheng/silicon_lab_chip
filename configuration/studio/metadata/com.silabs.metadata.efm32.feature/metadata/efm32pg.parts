<?xml version="1.0" encoding="UTF-8"?>
<collection>
  <group parent="mcu.arm.efm32" name="pg1">
    <property key="family" value="efm32pg1b"/>
    <property key="referenceManuals" value="asset://efm32/reference/efm32pg1_reference_manual.pdf"/>
    <property key="hardwareConfigData" value="asset://efm32/configurator/efm32pg1b/EFM32PG1B"/>
    <property key="dataSheets" value="asset://efm32/datasheet/efm32pg1_datasheet.pdf"/>
    <property key="errata" value="asset://efm32/errata/efm32pg1_errata.pdf"/>
    <property key="keywords" value=""/>
    <property key="memory.FLASH.addr" value="0"/>
    <property key="memory.FLASH.access" value="rx"/>
    <property key="memory.FLASH.pageSize" value="0x800"/>
    <property key="memory.RAM.addr" value="0x20000000"/>
    <property key="memory.RAM.access" value="rwx"/>
    <property key="ARMCore" value="CORTEX_M4F"/>
    <property key="FPU" value="VFP_V4"/>
    <property key="hardware.part.familyType" value="Pearl"/>
    <property key="hardware.part.configuration" value="1"/>
    <property key="hardware.part.series" value="1"/>

    <part name="efm32pg1b200f256gm48" label="EFM32PG1B200F256GM48">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/pg/EFM32PG1B200.json asset://efm32/debug/pg/EFM32PG1B200F256GM48_core.json"/>
      <property key="hardware.part.featureSet" value="200"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.series" value="1"/>
      <property key="hardware.part.performance" value="B"/>
      <property key="hardware.part.temp" value="G"/>
      <property key="hardware.part.package" value="M"/>
      <property key="hardware.part.pins" value="48"/>
    </part>

    <part name="efm32pg1b200f128gm48" label="EFM32PG1B200F128GM48">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/pg/EFM32PG1B200.json asset://efm32/debug/pg/EFM32PG1B200F128GM48_core.json"/>
      <property key="hardware.part.featureSet" value="200"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.series" value="1"/>
      <property key="hardware.part.performance" value="B"/>
      <property key="hardware.part.temp" value="G"/>
      <property key="hardware.part.package" value="M"/>
      <property key="hardware.part.pins" value="48"/>
    </part>

    <part name="efm32pg1b200f256gm32" label="EFM32PG1B200F256GM32">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/pg/EFM32PG1B200.json asset://efm32/debug/pg/EFM32PG1B200F256GM32_core.json"/>
      <property key="hardware.part.featureSet" value="200"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.series" value="1"/>
      <property key="hardware.part.performance" value="B"/>
      <property key="hardware.part.temp" value="G"/>
      <property key="hardware.part.package" value="M"/>
      <property key="hardware.part.pins" value="32"/>
    </part>

    <part name="efm32pg1b200f128gm32" label="EFM32PG1B200F128GM32">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/pg/EFM32PG1B200.json asset://efm32/debug/pg/EFM32PG1B200F128GM32_core.json"/>
      <property key="hardware.part.featureSet" value="200"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.series" value="1"/>
      <property key="hardware.part.performance" value="B"/>
      <property key="hardware.part.temp" value="G"/>
      <property key="hardware.part.package" value="M"/>
      <property key="hardware.part.pins" value="32"/>
    </part>

    <part name="efm32pg1b100f256gm32" label="EFM32PG1B100F256GM32">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/pg/EFM32PG1B100.json asset://efm32/debug/pg/EFM32PG1B100F256GM32_core.json"/>
      <property key="hardware.part.featureSet" value="100"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.series" value="1"/>
      <property key="hardware.part.performance" value="B"/>
      <property key="hardware.part.temp" value="G"/>
      <property key="hardware.part.package" value="M"/>
      <property key="hardware.part.pins" value="32"/>
    </part>

    <part name="efm32pg1b100f128gm32" label="EFM32PG1B100F128GM32">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/pg/EFM32PG1B100.json asset://efm32/debug/pg/EFM32PG1B100F128GM32_core.json"/>
      <property key="hardware.part.featureSet" value="100"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.series" value="1"/>
      <property key="hardware.part.performance" value="B"/>
      <property key="hardware.part.temp" value="G"/>
      <property key="hardware.part.package" value="M"/>
      <property key="hardware.part.pins" value="32"/>
    </part>
  </group>
  <group parent="mcu.arm.efm32" name="pg12">
    <property key="family" value="efm32pg12b"/>
    <property key="referenceManuals" value="asset://efm32/reference/efm32pg12_reference_manual.pdf"/>
    <property key="hardwareConfigData" value="asset://efm32/configurator/efm32pg12b/EFM32PG12B"/>
    <property key="dataSheets" value="asset://efm32/datasheet/efm32pg12_datasheet.pdf"/>
    <property key="errata" value="asset://efm32/errata/efm32pg12_errata.pdf"/>
    <property key="keywords" value=""/>
    <property key="memory.FLASH.addr" value="0"/>
    <property key="memory.FLASH.access" value="rx"/>
    <property key="memory.FLASH.pageSize" value="0x800"/>
    <property key="memory.RAM.addr" value="0x20000000"/>
    <property key="memory.RAM.access" value="rwx"/>
    <property key="ARMCore" value="CORTEX_M4F"/>
    <property key="FPU" value="VFP_V4"/>
    <property key="hardware.part.familyType" value="Pearl"/>
    <property key="hardware.part.configuration" value="2"/>
    <property key="hardware.part.series" value="1"/>
    <property key="debugPartName" value="Cortex-M4"/>

    <part name="efm32pg12b500f1024gl125" label="EFM32PG12B500F1024GL125">
      <property key="debugDescriptors" value="asset://efm32/debug/pg/EFM32PG12B500.json asset://efm32/debug/pg/EFM32PG12B500F1024GL125_core.json"/>
      <property key="memory.FLASH.size" value="0x100000"/>
      <property key="memory.RAM.size" value="0x40000"/>
      <property key="hardware.part.series" value="1"/>
      <property key="hardware.part.configuration" value="2"/>
      <property key="hardware.part.performance" value="B"/>
      <property key="hardware.part.featureSet" value="500"/>
      <property key="hardware.part.flash" value="1024K"/>
      <property key="hardware.part.temp" value="G"/>
      <property key="hardware.part.package" value="L"/>
      <property key="hardware.part.pins" value="125"/>
    </part>

    <part name="efm32pg12b500f1024il125" label="EFM32PG12B500F1024IL125">
      <property key="debugDescriptors" value="asset://efm32/debug/pg/EFM32PG12B500.json asset://efm32/debug/pg/EFM32PG12B500F1024IL125_core.json"/>
      <property key="memory.FLASH.size" value="0x100000"/>
      <property key="memory.RAM.size" value="0x40000"/>
      <property key="hardware.part.series" value="1"/>
      <property key="hardware.part.configuration" value="2"/>
      <property key="hardware.part.performance" value="B"/>
      <property key="hardware.part.featureSet" value="500"/>
      <property key="hardware.part.flash" value="1024K"/>
      <property key="hardware.part.temp" value="I"/>
      <property key="hardware.part.package" value="L"/>
      <property key="hardware.part.pins" value="125"/>
    </part>

    <part name="efm32pg12b500f1024gm48" label="EFM32PG12B500F1024GM48">
      <property key="debugDescriptors" value="asset://efm32/debug/pg/EFM32PG12B500.json asset://efm32/debug/pg/EFM32PG12B500F1024GM48_core.json"/>
      <property key="memory.FLASH.size" value="0x100000"/>
      <property key="memory.RAM.size" value="0x40000"/>
      <property key="hardware.part.series" value="1"/>
      <property key="hardware.part.configuration" value="2"/>
      <property key="hardware.part.performance" value="B"/>
      <property key="hardware.part.featureSet" value="500"/>
      <property key="hardware.part.flash" value="1024K"/>
      <property key="hardware.part.temp" value="G"/>
      <property key="hardware.part.package" value="M"/>
      <property key="hardware.part.pins" value="48"/>
    </part>

    <part name="efm32pg12b500f1024im48" label="EFM32PG12B500F1024IM48">
      <property key="debugDescriptors" value="asset://efm32/debug/pg/EFM32PG12B500.json asset://efm32/debug/pg/EFM32PG12B500F1024IM48_core.json"/>
      <property key="memory.FLASH.size" value="0x100000"/>
      <property key="memory.RAM.size" value="0x40000"/>
      <property key="hardware.part.series" value="1"/>
      <property key="hardware.part.configuration" value="2"/>
      <property key="hardware.part.performance" value="B"/>
      <property key="hardware.part.featureSet" value="500"/>
      <property key="hardware.part.flash" value="1024K"/>
      <property key="hardware.part.temp" value="I"/>
      <property key="hardware.part.package" value="M"/>
      <property key="hardware.part.pins" value="48"/>
    </part>
  </group>
</collection>
