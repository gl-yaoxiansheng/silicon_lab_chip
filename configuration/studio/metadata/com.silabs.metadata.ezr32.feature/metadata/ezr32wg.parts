<?xml version="1.0" encoding="UTF-8"?>
<collection>
  <group parent="mcu.arm.ezr32" name="wg">
    <property key="family" value="ezr32wg"/>
    <property key="referenceManuals" value="asset://ezr32/reference/EZR32WG_Reference_Manual.pdf"/>
    <property key="hardwareConfigData" value="asset://ezr32/configurator/ezr32wg/EZR32WG"/>
    <property key="keywords" value=""/>
    <property key="ARMCore" value="CORTEX_M4"/>
    <property key="FPU" value="VFP_V4"/>
    <property key="memory.FLASH.addr" value="0"/>
    <property key="memory.FLASH.pageSize" value="0x800"/>
    <property key="memory.FLASH.access" value="rx"/>
    <property key="memory.RAM.addr" value="0x20000000"/>
    <property key="memory.RAM.access" value="rwx"/>
    <property key="hardware.part.familyType" value="Wonder"/>

    <part name="ezr32wg230f256r55g" label="EZR32WG230F256R55G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F256.json"/>
      <property key="debugPartName" value="EZR32WG230F256Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4455-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f256"/>
    </part>

    <part name="ezr32wg230f256r60g" label="EZR32WG230F256R60G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F256.json"/>
      <property key="debugPartName" value="EZR32WG230F256Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4460-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f256"/>
    </part>

    <part name="ezr32wg230f256r61g" label="EZR32WG230F256R61G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F256.json"/>
      <property key="debugPartName" value="EZR32WG230F256Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4461-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f256"/>
    </part>

    <part name="ezr32wg230f256r63g" label="EZR32WG230F256R63G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F256.json"/>
      <property key="debugPartName" value="EZR32WG230F256Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4463-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f256"/>
    </part>

    <part name="ezr32wg230f256r65g" label="EZR32WG230F256R65G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F256.json"/>
      <property key="debugPartName" value="EZR32WG230F256Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4465-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f256"/>
    </part>

    <part name="ezr32wg230f256r67g" label="EZR32WG230F256R67G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F256.json"/>
      <property key="debugPartName" value="EZR32WG230F256Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4467-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f256"/>
    </part>

    <part name="ezr32wg230f256r68g" label="EZR32WG230F256R68G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F256.json"/>
      <property key="debugPartName" value="EZR32WG230F256Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4468-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f256"/>
    </part>

    <part name="ezr32wg230f256r69g" label="EZR32WG230F256R69G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F256.json"/>
      <property key="debugPartName" value="EZR32WG230F256Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4469-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f256"/>
    </part>

    <part name="ezr32wg230f128r55g" label="EZR32WG230F128R55G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F128.json"/>
      <property key="debugPartName" value="EZR32WG230F128Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4455-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f128"/>
    </part>

    <part name="ezr32wg230f128r60g" label="EZR32WG230F128R60G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F128.json"/>
      <property key="debugPartName" value="EZR32WG230F128Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4460-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f128"/>
    </part>

    <part name="ezr32wg230f128r61g" label="EZR32WG230F128R61G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F128.json"/>
      <property key="debugPartName" value="EZR32WG230F128Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4461-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f128"/>
    </part>

    <part name="ezr32wg230f128r63g" label="EZR32WG230F128R63G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F128.json"/>
      <property key="debugPartName" value="EZR32WG230F128Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4463-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f128"/>
    </part>

    <part name="ezr32wg230f128r65g" label="EZR32WG230F128R65G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F128.json"/>
      <property key="debugPartName" value="EZR32WG230F128Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4465-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f128"/>
    </part>

    <part name="ezr32wg230f128r67g" label="EZR32WG230F128R67G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F128.json"/>
      <property key="debugPartName" value="EZR32WG230F128Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4467-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f128"/>
    </part>

    <part name="ezr32wg230f128r68g" label="EZR32WG230F128R68G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F128.json"/>
      <property key="debugPartName" value="EZR32WG230F128Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4468-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f128"/>
    </part>

    <part name="ezr32wg230f128r69g" label="EZR32WG230F128R69G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F128.json"/>
      <property key="debugPartName" value="EZR32WG230F128Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4469-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f128"/>
    </part>

    <part name="ezr32wg230f64r55g" label="EZR32WG230F64R55G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F64.json"/>
      <property key="debugPartName" value="EZR32WG230F64Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4455-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f64"/>
    </part>

    <part name="ezr32wg230f64r60g" label="EZR32WG230F64R60G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F64.json"/>
      <property key="debugPartName" value="EZR32WG230F64Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4460-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f64"/>
    </part>

    <part name="ezr32wg230f64r61g" label="EZR32WG230F64R61G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F64.json"/>
      <property key="debugPartName" value="EZR32WG230F64Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4461-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f64"/>
    </part>

    <part name="ezr32wg230f64r63g" label="EZR32WG230F64R63G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F64.json"/>
      <property key="debugPartName" value="EZR32WG230F64Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4463-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f64"/>
    </part>

    <part name="ezr32wg230f64r65g" label="EZR32WG230F64R65G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F64.json"/>
      <property key="debugPartName" value="EZR32WG230F64Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4465-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f64"/>
    </part>

    <part name="ezr32wg230f64r67g" label="EZR32WG230F64R67G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F64.json"/>
      <property key="debugPartName" value="EZR32WG230F64Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4467-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f64"/>
    </part>

    <part name="ezr32wg230f64r68g" label="EZR32WG230F64R68G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F64.json"/>
      <property key="debugPartName" value="EZR32WG230F64Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4468-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f64"/>
    </part>

    <part name="ezr32wg230f64r69g" label="EZR32WG230F64R69G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG230_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG230.json asset://ezr32/debug/json/EZR32WG230F64.json"/>
      <property key="debugPartName" value="EZR32WG230F64Rxx"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4469-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg230f64"/>
    </part>

    <part name="ezr32wg330f256r55g" label="EZR32WG330F256R55G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F256.json"/>
      <property key="debugPartName" value="EZR32WG330F256Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4455-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f256"/>
    </part>

    <part name="ezr32wg330f256r60g" label="EZR32WG330F256R60G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F256.json"/>
      <property key="debugPartName" value="EZR32WG330F256Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4460-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f256"/>
    </part>

    <part name="ezr32wg330f256r61g" label="EZR32WG330F256R61G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F256.json"/>
      <property key="debugPartName" value="EZR32WG330F256Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4461-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f256"/>
    </part>

    <part name="ezr32wg330f256r63g" label="EZR32WG330F256R63G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F256.json"/>
      <property key="debugPartName" value="EZR32WG330F256Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4463-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f256"/>
    </part>

    <part name="ezr32wg330f256r65g" label="EZR32WG330F256R65G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F256.json"/>
      <property key="debugPartName" value="EZR32WG330F256Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4465-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f256"/>
    </part>

    <part name="ezr32wg330f256r67g" label="EZR32WG330F256R67G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F256.json"/>
      <property key="debugPartName" value="EZR32WG330F256Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4467-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f256"/>
    </part>

    <part name="ezr32wg330f256r68g" label="EZR32WG330F256R68G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F256.json"/>
      <property key="debugPartName" value="EZR32WG330F256Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4468-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f256"/>
    </part>

    <part name="ezr32wg330f256r69g" label="EZR32WG330F256R69G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F256.json"/>
      <property key="debugPartName" value="EZR32WG330F256Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="256K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4469-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f256"/>
    </part>

    <part name="ezr32wg330f128r55g" label="EZR32WG330F128R55G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F128.json"/>
      <property key="debugPartName" value="EZR32WG330F128Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4455-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f128"/>
    </part>

    <part name="ezr32wg330f128r60g" label="EZR32WG330F128R60G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F128.json"/>
      <property key="debugPartName" value="EZR32WG330F128Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4460-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f128"/>
    </part>

    <part name="ezr32wg330f128r61g" label="EZR32WG330F128R61G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F128.json"/>
      <property key="debugPartName" value="EZR32WG330F128Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4461-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f128"/>
    </part>

    <part name="ezr32wg330f128r63g" label="EZR32WG330F128R63G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F128.json"/>
      <property key="debugPartName" value="EZR32WG330F128Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4463-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f128"/>
    </part>

    <part name="ezr32wg330f128r65g" label="EZR32WG330F128R65G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F128.json"/>
      <property key="debugPartName" value="EZR32WG330F128Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4465-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f128"/>
    </part>

    <part name="ezr32wg330f128r67g" label="EZR32WG330F128R67G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F128.json"/>
      <property key="debugPartName" value="EZR32WG330F128Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4467-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f128"/>
    </part>

    <part name="ezr32wg330f128r68g" label="EZR32WG330F128R68G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F128.json"/>
      <property key="debugPartName" value="EZR32WG330F128Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4468-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f128"/>
    </part>

    <part name="ezr32wg330f128r69g" label="EZR32WG330F128R69G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F128.json"/>
      <property key="debugPartName" value="EZR32WG330F128Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="128K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4469-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f128"/>
    </part>

    <part name="ezr32wg330f64r55g" label="EZR32WG330F64R55G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F64.json"/>
      <property key="debugPartName" value="EZR32WG330F64Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4455-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f64"/>
    </part>

    <part name="ezr32wg330f64r60g" label="EZR32WG330F64R60G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F64.json"/>
      <property key="debugPartName" value="EZR32WG330F64Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4460-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f64"/>
    </part>

    <part name="ezr32wg330f64r61g" label="EZR32WG330F64R61G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F64.json"/>
      <property key="debugPartName" value="EZR32WG330F64Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4461-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f64"/>
    </part>

    <part name="ezr32wg330f64r63g" label="EZR32WG330F64R63G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F64.json"/>
      <property key="debugPartName" value="EZR32WG330F64Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4463-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f64"/>
    </part>

    <part name="ezr32wg330f64r65g" label="EZR32WG330F64R65G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F64.json"/>
      <property key="debugPartName" value="EZR32WG330F64Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4465-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f64"/>
    </part>

    <part name="ezr32wg330f64r67g" label="EZR32WG330F64R67G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F64.json"/>
      <property key="debugPartName" value="EZR32WG330F64Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4467-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f64"/>
    </part>

    <part name="ezr32wg330f64r68g" label="EZR32WG330F64R68G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F64.json"/>
      <property key="debugPartName" value="EZR32WG330F64Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4468-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f64"/>
    </part>

    <part name="ezr32wg330f64r69g" label="EZR32WG330F64R69G">
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32WG330_datasheet.pdf"/>
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32WG330.json asset://ezr32/debug/json/EZR32WG330F64.json"/>
      <property key="debugPartName" value="EZR32WG330F64Rxx"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4469-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.wg.efm32wg330f64"/>
    </part>
  </group>
</collection>
