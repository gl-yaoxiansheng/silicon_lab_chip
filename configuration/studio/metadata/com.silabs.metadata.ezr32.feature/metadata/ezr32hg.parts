<?xml version="1.0" encoding="UTF-8"?>
<collection>
  <group parent="mcu.arm.ezr32" name="hg">
    <property key="family" value="ezr32hg"/>
    <property key="referenceManuals" value="asset://ezr32/reference/EZR32HG_Reference_Manual.pdf"/>
    <property key="hardwareConfigData" value="asset://ezr32/configurator/ezr32hg/EZR32HG"/>
    <property key="keywords" value=""/>
    <property key="ARMCore" value="CORTEX_M0+"/>
    <property key="memory.FLASH.addr" value="0"/>
    <property key="memory.FLASH.pageSize" value="0x400"/>
    <property key="memory.FLASH.access" value="rx"/>
    <property key="memory.RAM.addr" value="0x20000000"/>
    <property key="memory.RAM.access" value="rwx"/>
    <property key="hardware.part.familyType" value="Happy"/>

    <part name="ezr32hg220f32r55g" label="EZR32HG220F32R55G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4455-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f32r60g" label="EZR32HG220F32R60G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4460-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f32r61g" label="EZR32HG220F32R61G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4461-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f32r63g" label="EZR32HG220F32R63G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4463-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f32r67g" label="EZR32HG220F32R67G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4467-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f32r68g" label="EZR32HG220F32R68G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4468-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f32r69g" label="EZR32HG220F32R69G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4469-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f64r55g" label="EZR32HG220F64R55G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4455-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f64r55g" label="EZR32HG220F64R55G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4455-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f64r60g" label="EZR32HG220F64R60G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4460-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f64r61g" label="EZR32HG220F64R61G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4461-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f64r63g" label="EZR32HG220F64R63G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4463-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f64r67g" label="EZR32HG220F64R67G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4467-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f64r68g" label="EZR32HG220F64R68G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4468-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg220f64r69g" label="EZR32HG220F64R69G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG220.json asset://ezr32/debug/json/EZR32HG220F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG220_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg220_errata.pdf"/>
      <property key="hardware.part.featureSet" value="220"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4469-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg220f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f32r55g" label="EZR32HG320F32R55G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4455-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f32r60g" label="EZR32HG320F32R60G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4460-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f32r61g" label="EZR32HG320F32R61G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4461-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f32r63g" label="EZR32HG320F32R63G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4463-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f32r67g" label="EZR32HG320F32R67G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4467-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f32r68g" label="EZR32HG320F32R68G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4468-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f32r69g" label="EZR32HG320F32R69G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F32.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4469-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f32r69g" label="EZR32HG320F32R69G">
      <property key="memory.FLASH.size" value="0x8000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="32K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4469-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f32"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f64r55g" label="EZR32HG320F64R55G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4455-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f64r60g" label="EZR32HG320F64R60G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4460-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f64r61g" label="EZR32HG320F64R61G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4461-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f64r63g" label="EZR32HG320F64R63G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4463-revc2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f64r67g" label="EZR32HG320F64R67G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4467-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f64r68g" label="EZR32HG320F64R68G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4468-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>

    <part name="ezr32hg320f64r69g" label="EZR32HG320F64R69G">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x2000"/>
      <property key="debugDescriptors" value="asset://ezr32/debug/json/EZR32HG320.json asset://ezr32/debug/json/EZR32HG320F64.json"/>
      <property key="dataSheets" value="asset://ezr32/datasheet/EZR32HG320_datasheet.pdf"/>
      <property key="errata" value="asset://ezr32/errata/ezr32hg320_errata.pdf"/>
      <property key="hardware.part.featureSet" value="320"/>
      <property key="hardware.part.flash" value="64K"/>
      <property key="hardware.part.radio_chip" value="radio.si44xx.si4469-reva2a"/>
      <property key="hardware.part.mcu_chip" value="mcu.arm.efm32.hg.efm32hg320f64"/>
      <property key="debugPartName" value="Cortex-M0+"/>
    </part>
  </group>
</collection>
