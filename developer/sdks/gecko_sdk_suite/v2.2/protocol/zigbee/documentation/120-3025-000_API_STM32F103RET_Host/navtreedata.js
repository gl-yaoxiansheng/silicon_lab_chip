var NAVTREE =
[
  [ "EmberZNet API Reference: For the STM32F103RET Host platform", "index.htm", [
    [ "Deprecated List", "deprecated.htm", null ],
    [ "Modules", "modules.htm", "modules" ],
    [ "Data Structures", "annotated.htm", [
      [ "Data Structures", "annotated.htm", "annotated_dup" ],
      [ "Data Fields", "functions.htm", [
        [ "All", "functions.htm", null ],
        [ "Variables", "functions_vars.htm", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.htm", "files" ],
      [ "Globals", "globals.htm", [
        [ "All", "globals.htm", "globals_dup" ],
        [ "Functions", "globals_func.htm", null ],
        [ "Variables", "globals_vars.htm", null ],
        [ "Typedefs", "globals_type.htm", null ],
        [ "Enumerations", "globals_enum.htm", null ],
        [ "Enumerator", "globals_eval.htm", "globals_eval" ],
        [ "Macros", "globals_defs.htm", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"__STM32F103RET__Host__API_8top.htm",
"group__bootloader__eeprom.htm#gafab2ff17bdb194c8af24c4e77438cbeb",
"group__ember__types.htm#ga0cff0d9ee654efea5973938e923578e0",
"group__ember__types.htm#ga9d6e54f6741b2605e8f20407571f9b51",
"group__ember__types.htm#gga0f532d16cb984d8d532a121b0c09fbe1a9dc94ead3400306a8d48962d5e06e5b4",
"group__ember__types.htm#gga9f972a77387b604c47eda6a63188aa3ba36c263f4d6b2972e5f479aa8adc3fea8",
"group__host.htm#ggace58749df14c14b64252eb55f40d2c32a8dec81d54908044ef56016aee3b1b506",
"group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd",
"group__status__codes.htm#gaa2dd1e1d80d2c484615dd7c7880b84e7",
"group__stm32f103ret__spip.htm#ga04ba6fae1a7777aac6626fe35c644897",
"structEmberReleaseTypeStruct.htm#a32d87630d29367e20fe295c902969a36"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';