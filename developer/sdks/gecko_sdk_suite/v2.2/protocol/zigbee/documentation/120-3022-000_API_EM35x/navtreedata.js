var NAVTREE =
[
  [ "EmberZNet API Reference: For the EM35x SoC Platform", "index.htm", [
    [ "Deprecated List", "deprecated.htm", null ],
    [ "Modules", "modules.htm", "modules" ],
    [ "Data Structures", "annotated.htm", [
      [ "Data Structures", "annotated.htm", "annotated_dup" ],
      [ "Data Fields", "functions.htm", [
        [ "All", "functions.htm", "functions_dup" ],
        [ "Variables", "functions_vars.htm", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.htm", "files" ],
      [ "Globals", "globals.htm", [
        [ "All", "globals.htm", "globals_dup" ],
        [ "Functions", "globals_func.htm", "globals_func" ],
        [ "Variables", "globals_vars.htm", null ],
        [ "Typedefs", "globals_type.htm", null ],
        [ "Enumerations", "globals_enum.htm", null ],
        [ "Enumerator", "globals_eval.htm", "globals_eval" ],
        [ "Macros", "globals_defs.htm", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"__EM35x__API_8top.htm",
"group__USB__COMMON.htm#ga06cd3bc995e8bf1d1b4d6958323c98d8",
"group__USB__COMMON.htm#gga889b575b566a663621c33eebf46272c1a7fc3604700f290c08102ab7a60dbeac2",
"group__alone__bootload.htm#gae8ebf8e8a4941e32bcd829da63d2552e",
"group__board.htm#ga536eaf3919213348d366ec9c8a5f034a",
"group__board.htm#gad4e6198e9986cfca8459853774765605",
"group__child.htm#ggadf764cbdea00d65edcd07bb9953ad2b7a4e4e0f711ebc7d467453831e0b5c6fdd",
"group__configuration.htm#gacdb0e597bb8a24ed60388e00f088b171",
"group__ember__types.htm#ga707d29d9ac6b477f9918682467bbcd48",
"group__ember__types.htm#gaede804d3b8354e9e23f4ce8dceffef5b",
"group__ember__types.htm#gga5be13a9999d8301d5cf4424997f62955a690d69cd0b2b18ff2dfab3401a953dab",
"group__ember__types.htm#ggadfde62e4cdd51e13bcf9fc58a2b9d7c7ae04580e6ee791a0127203641f1f03740",
"group__iar.htm#ga71a2a6a526f70740ff022af5d0f92b29",
"group__message.htm#ga7a9b486b6a20e47b47f52e2c6fc25b96",
"group__micro.htm#gadd8e004ec902a5a4e2626a6043c55d1c",
"group__platform__common.htm#ga15d97484a32b62f82b121a40e9385564",
"group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd",
"group__status__codes.htm#ga5593b1028c5981c9dff6a0da5e5ce06f",
"group__token__stack.htm#ga085fbb99a8c398a19b86b6a3abf51480",
"group__zll.htm#ga448e9b721a17f879cd68727f2bfc3e81",
"structEmberMulticastTableEntry.htm",
"token-manufacturing_8h.htm#ac081de9c0b93ab5eef5e9f5f02101d2f"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';