var NAVTREE =
[
  [ "EmberZNet API Reference: For the PC Host platform", "index.htm", [
    [ "Deprecated List", "deprecated.htm", null ],
    [ "Modules", "modules.htm", "modules" ],
    [ "Data Structures", "annotated.htm", [
      [ "Data Structures", "annotated.htm", "annotated_dup" ],
      [ "Data Fields", "functions.htm", [
        [ "All", "functions.htm", null ],
        [ "Variables", "functions_vars.htm", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.htm", "files" ],
      [ "Globals", "globals.htm", [
        [ "All", "globals.htm", "globals_dup" ],
        [ "Functions", "globals_func.htm", null ],
        [ "Variables", "globals_vars.htm", null ],
        [ "Typedefs", "globals_type.htm", null ],
        [ "Enumerations", "globals_enum.htm", null ],
        [ "Enumerator", "globals_eval.htm", "globals_eval" ],
        [ "Macros", "globals_defs.htm", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"__PC__Host__API_8top.htm",
"group__commands2.htm#ga374ac1430efd8c6784af789f1e86fe24",
"group__ember__types.htm#ga57cf9e2d7e3f25cd35e63c59b7c292e3",
"group__ember__types.htm#gad7806488e0739f1cc69d7a4af0f03f2c",
"group__ember__types.htm#gga4a252915c606ae2aed72937bd76bd20ea2cf94f196a9bfc5c066b61af61046524",
"group__ember__types.htm#ggadccc961b628c584381f05b0ddad80e1ba5c6498239284647ab60ff05dac461d84",
"group__platform__common.htm#gad7795912037ecd3781859d0895f051a8",
"group__status__codes.htm#gac30817974171b49bef4c3c6b2f337a94",
"structEmberMulticastTableEntry.htm#a2d0719d1a66516339b4f1883c802ee46"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';