var NAVTREE =
[
  [ "Connect", "index.html", [
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group__iar.html#ga06b30d2f53a41d8e56088a1405fcf4e2",
"group__micro.html#ga752913e161510fa16ceae603bb0b023e",
"group__stack__info.html#ga81602c711b132feefd4332b03bfe6924",
"structEmberMacFrame.html#a4a16295200bc62e551ec8337bfad42da"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';