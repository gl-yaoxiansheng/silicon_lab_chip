// File: tmsp-ncp-utilities.c
//
// Description: Functions to assist processing of TMSP commands and returns.
//
// Copyright 2015 Silicon Laboratories, Inc.                                *80*

#include PLATFORM_HEADER
#include CONFIGURATION_HEADER
#include "stack/core/ember-stack.h"
#include "include/define-ember-api.h"
#include "include/undefine-ember-api.h"
#include "stack/ip/zigbee/key-management.h"
#include "app/util/ip/print-utilities.h"
#include "stack/mac/802.15.4/802-15-4-ccm.h"
#include "stack/ip/association.h"
#include "stack/ip/commission-dataset.h"
#include "stack/ip/ip-address.h"
#include "stack/ip/network-data.h"
#include "stack/ip/tls/dtls-join.h"
#include "stack/ip/zigbee/join.h"
#include "tmsp-ncp-utilities.h"

// no-op not currently used Host-to-NCP.
void emApiHostToNcpNoOp(const uint8_t *bytes, uint8_t bytesLength)
{
}

// Don't have printing enabled in library builds to save space and prevent
// clutter on the output channel
#ifdef IP_MODEM_LIBRARY
  #define IP_MODEM_PRINT(x)
#else
  #define IP_MODEM_PRINT(x) x
#endif

// -----------------------------------------------------------------------------
// Convenience functions for generating multiple callbacks

void sendCommProxyAppPskcHandlerCallback(const uint8_t *pskc)
{
#if (!defined(RTOS) && !defined(EMBER_WAKEUP_STACK))
  if (pskc != NULL && !emIsMemoryZero(pskc, THREAD_PSKC_SIZE)) {
    emApiSetCommProxyAppPskcHandler(pskc);
  }
#endif
}

// We send the network parameters, plus the status and eui64 all at once.
void sendNetworkStateCallback(bool stateRequested)
{
  EmberNetworkParameters params;
  MEMSET(&params, 0, sizeof(EmberNetworkParameters));
  emApiGetNetworkParameters(&params);
  if (stateRequested) {
    emApiStateReturn(&params,
                     &emLocalEui64,
                     (const EmberEui64 *)emMacExtendedId,
                     emApiNetworkStatus());
  } else {
    emApiHostStateHandler(&params,
                          &emLocalEui64,
                          (const EmberEui64 *)emMacExtendedId,
                          emApiNetworkStatus());
  }
#if (!defined(RTOS) && !defined(EMBER_WAKEUP_STACK))
  emApiSetCommProxyAppParametersHandler(params.extendedPanId,
                                        params.networkId,
                                        params.ulaPrefix.bytes,
                                        params.panId,
                                        params.channel,
                                        &emLocalEui64,
                                        (const EmberEui64 *)emMacExtendedId,
                                        emApiNetworkStatus());
#endif
}

void sendSetDriverAddressCallback(uint8_t *address)
{
#ifndef RTOS
  emApiSetDriverAddressHandler(address);
#endif
}

void sendNetworkKeysCallback(void)
{
#ifndef RTOS
  uint8_t storage[EMBER_ENCRYPTION_KEY_SIZE];
  uint32_t sequence;
  emGetNetworkKeySequence(&sequence);
  emApiSetNetworkKeysHandler(sequence,
                             emGetNetworkMasterKey(storage),
                             0,
                             NULL); // driver doesn't need the second key at the moment
#endif
#if (!defined(RTOS) && !defined(EMBER_WAKEUP_STACK))
  // For the border router piece on the host app
  emApiSetCommProxyAppSecurityHandler(emGetNetworkMasterKey(storage), sequence);
#endif
}

void modemAddressConfigurationHandler(void)
{
  // If NULL, by default, this ships the GP64 to the host app and the GP16
  // to the driver.  It needs to change so that the driver also uses the GP64.
  // The GP16 is too volatile.
  if (emberGetNodeId() < EM_USE_LONG_ADDRESS) {
    EmberIpv6Address driverAddress;
    emStoreGp16(emberGetNodeId(), driverAddress.bytes);
    sendSetDriverAddressCallback(driverAddress.bytes);
  }

  EmberIpv6Address mlAddress;
  MEMSET(mlAddress.bytes, 0, 16);
  if (emStoreLocalMl64(mlAddress.bytes)) {
    emApiSetAddressHandler(mlAddress.bytes);
#if (!defined(RTOS) && !defined(EMBER_WAKEUP_STACK))
    emApiSetCommProxyAppAddressHandler(mlAddress.bytes);
#endif
  } else {
    return;
  }
}

// -----------------------------------------------------------------------------
// TMSP pre/post hooks.

void tmspNcpInitReturnPreHook(void)
{
  // Send network state to host
  sendNetworkStateCallback(false);

  if (emApiNetworkStatus() == EMBER_JOINED_NETWORK_ATTACHED) {
    // Send network keys from token to driver
    sendNetworkKeysCallback();
    // Send GP64 address callback to driver and host
    modemAddressConfigurationHandler();
    // If we have a PSKc, inform the host about that as well.
    uint8_t myPskc[THREAD_PSKC_SIZE];
    if (emGeneratePskc(emExtendedPanId, emNetworkId, myPskc)) {
      emApiSetPskcHandler(myPskc);
    }
  }
}

void tmspNcpInitReturnPostHook(void)
{
  // perform a versions callback
  emApiGetVersions();
}

bool tmspNcpAddressConfigurationChangePreHook(const EmberIpv6Address *address)
{
  if (emIsDefaultGlobalPrefix(address->bytes)) {
    // Default mesh local address.
    modemAddressConfigurationHandler();
    return false;
  }
  return true;
}

bool tmspNcpCommissionNetworkPreHook(uint8_t networkIdLength)
{
  // Below is the entire pre-TMSP validation from modemConfigureCommand,
  // commented out except for those items still checked (networkId length).
  //
  // ulaPrefix, extendedPanId, and key are now extracted using
  // emberGetStringArgument(), instead of emberStringCommandArgument(),
  // and the lengths of those parameters are already enforced and do not
  // need further checking here.

  /*
     // network id
     uint8_t networkIdLength;
     const uint8_t *networkId = emberStringCommandArgument(2, &networkIdLength);
   */
  // TMSP update: this was previously '<', seems not to have been corrected
  // when change was made for networkId to consume the full SIZE (instead
  // of reserving last char for null terminator).
  assert(networkIdLength <= EMBER_NETWORK_ID_SIZE);

  /*
     // ula
     uint8_t ulaPrefixLength;
     const uint8_t *ulaPrefix =
     emberStringCommandArgument(4, &ulaPrefixLength);
     assert(ulaPrefixLength == 8);

     // extended pan id
     uint8_t extendedPanIdLength;
     const uint8_t *extendedPanId =
     emberStringCommandArgument(5, &extendedPanIdLength);
     assert(extendedPanIdLength == EXTENDED_PAN_ID_SIZE);

     // network key
     uint8_t networkKeyLength;
     const EmberKeyData *key =
     (EmberKeyData *) emberStringCommandArgument(6, &networkKeyLength);
     assert(networkKeyLength == EMBER_ENCRYPTION_KEY_SIZE);
   */

  return true;
}

bool tmspNcpNetworkStatusHandlerPreHook(EmberNetworkStatus newNetworkStatus)
{
  if (newNetworkStatus == EMBER_JOINED_NETWORK_ATTACHED) {
    sendNetworkKeysCallback();
    sendNetworkStateCallback(false);
    modemAddressConfigurationHandler();
  } else {
    sendNetworkStateCallback(false);
  }
  return true;
}

bool tmspNcpNodeStatusReturnPreHook(const uint8_t **networkFragmentIdentifier, uint8_t *identifier)
{
  // If *pointer is NULL, change it to point to zero-valued identifier
  if (*networkFragmentIdentifier == NULL) {
    *networkFragmentIdentifier = identifier;
  }
  return true;
}

extern bool modemDoesDataPolling;

bool tmspNcpPollForDataReturnPreHook(EmberStatus status)
{
  if (!modemDoesDataPolling || (status != EMBER_SUCCESS)) {
    return true;
  }
  return false;
}

void tmspNcpPollForDataReturnPostHook(EmberStatus status)
{
  if (modemDoesDataPolling
      && status == EMBER_INVALID_CALL) {
    modemDoesDataPolling = false;
  }
}

bool tmspNcpResetMicroHandlerPreHook(void)
{
  IP_MODEM_PRINT(printResetInfo()); // use hal-specific version for good debugging info
  return true;
}

extern EmberEventControl modemDataPollEvent;

bool tmspNcpResetNetworkStatePreHook(void)
{
  emberEventControlSetInactive(modemDataPollEvent);
  return true;
}

bool tmspNcpResetNetworkStateReturnPreHook(EmberStatus status)
{
  if (status == EMBER_SUCCESS) {
    sendNetworkStateCallback(false); // Send an empty network state to clear host cache.
  }
  return true;
}

void tmspNcpSetRadioPowerReturnPostHook(void)
{
  // Network state on host needs to be updated with the new power level.
  sendNetworkStateCallback(false);
}

extern uint8_t emBootloaderMode;
extern uint16_t halGetStandaloneBootloaderVersion(void);

bool tmspNcpLaunchStandaloneBootloaderPreHook(uint8_t mode)
{
  EmberStatus status = EMBER_ERR_FATAL;
  // 0xFFFF == BOOTLOADER_INVALID_VERSION
  if (0xFFFF != halGetStandaloneBootloaderVersion()) {
    // If the bootloader checks out ok, defer executing until we have a
    // chance to send back the TMSP response frame.
    status = EMBER_SUCCESS;
    emBootloaderMode = mode;
  }
  // Never immediately call halLaunchStandaloneBootloader() on an NCP.
  // Let the main loop in ip-modem-app call it when it notices a change
  // to emBootloaderMode. Respond now so that the host knows whether
  // the NCP is launching the bootloader or not.
  emApiLaunchStandaloneBootloaderReturn(status);
  return false;
}

//----------------------------------------------------------------
// Workaround because the network data may not fit in a single TMSP packet.
// The "emApi" is added by the TMSP code generation system.  This is not
// actually an API procedure.

// I experimentally determined that 92 is largest value that lets a
// emApiNcpGetNetworkDataReturn command fit in a TMSP frame.  This
// is two bytes less so there is a little slack.
#define MAX_INCLUDED_NETWORK_DATA 90

void emApiNcpGetNetworkData(uint16_t bufferLength)
{
  uint16_t length;
  uint8_t *data = emGetNetworkData(&length);
  if (length == 0xFFFF) {        // no network data
    emApiNcpGetNetworkDataReturn(EMBER_NETWORK_DOWN, 0, NULL, 0, 0);
  } else if (bufferLength < length) {
    emApiNcpGetNetworkDataReturn(EMBER_BAD_ARGUMENT, 0, NULL, 0, 0);
  } else if (length == 0) {
    emApiNcpGetNetworkDataReturn(EMBER_SUCCESS, 0, NULL, 0, 0);
  } else {
    uint16_t offset = 0;
    while (offset < length) {
      uint8_t send = (MAX_INCLUDED_NETWORK_DATA < (length - offset)
                      ? MAX_INCLUDED_NETWORK_DATA
                      : length - offset);
      emApiNcpGetNetworkDataReturn(EMBER_SUCCESS,
                                   length,
                                   data + offset,
                                   send,
                                   offset);
      offset += send;
    }
  }
}

// Just a stub, because emApiNcpGetNetworkData() gets called in place of
// emberGetNetworkData().
void emApiGetNetworkDataReturn(EmberStatus status,
                               uint8_t *networkData,
                               uint16_t bufferLength)
{
#ifdef EMBER_AF_PLUGIN_THREAD_TEST_HARNESS_CLI
  extern void emberGetNetworkDataReturnTth(EmberStatus status,
                                           uint8_t *networkData,
                                           uint16_t bufferLength);
  emberGetNetworkDataReturnTth(status, networkData, bufferLength);
#endif
}

void emApiNetworkDataChangeHandler(const uint8_t *networkData, uint16_t length)
{
#ifdef EMBER_AF_PLUGIN_THREAD_TEST_HARNESS_CLI
  extern void emberNetworkDataChangeHandlerTth(const uint8_t *networkData, uint16_t length);
  emberNetworkDataChangeHandlerTth(networkData, length);
#endif
  if (length < MAX_INCLUDED_NETWORK_DATA) {
    emApiNcpNetworkDataChangeHandler(length, networkData, length);
  } else {
    emApiNcpNetworkDataChangeHandler(length, NULL, 0);
  }
}

//----------------------------------------------------------------
// Workaround because the ND data may not fit in a single TMSP packet.
//
// The "emApi" in "emApiNcpSetNdData", is added by the TMSP code
// generation system.  It is not actually an API procedure.

static SetLargeValueId currentSetLargeValueOp;
Buffer emNcpSetNdDataBuffer = NULL_BUFFER;
static uint16_t ndDataOffset;

static void(*returnFunctions[])(EmberStatus, uint16_t) =
{
  &emApiSetNdDataReturn,
  &emApiSetLocalNetworkDataReturn,
  &emApiSetVendorTlvsReturn
};

static void(*completeFunctions[])(const uint8_t *, uint16_t) =
{
  &emApiSetNdData,
  &emApiSetLocalNetworkData,
  &emApiSetVendorTlvs
};

void emApiNcpSetLargeData(SetLargeValueId op,
                          uint16_t totalLength,
                          const uint8_t *fragment,
                          uint8_t fragmentLength,
                          uint16_t fragmentOffset)
{
  if (fragmentOffset == 0
      && fragmentLength == totalLength) {
    completeFunctions[op](fragment, fragmentLength);
    emNcpSetNdDataBuffer = NULL_BUFFER;
  } else if (totalLength < fragmentOffset + fragmentLength
             || (fragmentOffset != 0
                 && (emNcpSetNdDataBuffer == NULL_BUFFER
                     || op != currentSetLargeValueOp
                     || totalLength != emGetBufferLength(emNcpSetNdDataBuffer)
                     || fragmentOffset != ndDataOffset))) {
    // Something has gone wrong - give up on the current attempt.
    if (emNcpSetNdDataBuffer != NULL_BUFFER) {
      returnFunctions[op](EMBER_ERR_FATAL, totalLength);
      // Remove the buffer so that the return function will not be called
      // twice for the same call to the set function on the host.
      emNcpSetNdDataBuffer = NULL_BUFFER;
    }
  } else {
    if (fragmentOffset == 0) {
      emNcpSetNdDataBuffer = emAllocateBuffer(totalLength);
      if (emNcpSetNdDataBuffer == NULL_BUFFER) {
        returnFunctions[op](EMBER_NO_BUFFERS, totalLength);
        return;
      }
      currentSetLargeValueOp = op;
      ndDataOffset = 0;
    }
    MEMCOPY(emGetBufferPointer(emNcpSetNdDataBuffer) + ndDataOffset,
            fragment,
            fragmentLength);
    ndDataOffset += fragmentLength;
    if (ndDataOffset == totalLength) {
      completeFunctions[op](emGetBufferPointer(emNcpSetNdDataBuffer),
                            totalLength);
      emNcpSetNdDataBuffer = NULL_BUFFER;
    }
  }
}

void emApiNcpSetNdData(uint16_t totalLength,
                       const uint8_t *fragment,
                       uint8_t fragmentLength,
                       uint16_t fragmentOffset)
{
  emApiNcpSetLargeData(SET_ND_DATA,
                       totalLength,
                       fragment,
                       fragmentLength,
                       fragmentOffset);
}

void emApiNcpSetVendorTlvs(uint16_t totalLength,
                           const uint8_t *fragment,
                           uint8_t fragmentLength,
                           uint16_t fragmentOffset)
{
  emApiNcpSetLargeData(SET_VENDOR_TLVS,
                       totalLength,
                       fragment,
                       fragmentLength,
                       fragmentOffset);
}
