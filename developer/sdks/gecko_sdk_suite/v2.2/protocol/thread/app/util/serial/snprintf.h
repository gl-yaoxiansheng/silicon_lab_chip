// MISRA-compatible snprintf()
//
// MISRA does not allow the use of stdio.h or variadic functions.
// This is a version of snprintf() that uses a variadic macro, which
// MISRA does allow, in place of a variadic function.  The values to
// print are encapsulated in an array of structs, each of which has
// both a value and a tag that gives the type of the value.  The
// tag is used to detect mismatches between the format string and
// the arguments. The value structs are constructed using specific
// macros for the different types.
//
// The end result is that
//   snprintf(s, n, "%c %s %d", 'x', "is", 123)
// becomes
//   emSnprintf(s, n, "%c %s %d", PF_C('x'), PF_S("is"), PF_D(123));

// Types of printable values, plus a marker for the end of the
// argument array.
#define PRINTF_END     0
#define PRINTF_INT     1
#define PRINTF_UINT    2
#define PRINTF_STRING  3
#define PRINTF_CHAR    4

// Argument struct holding a type and a value.
typedef struct {
  uint8_t type;
  union {
    const char *string;
    int integer;
    unsigned int unsignedInt;
    char ch;
  } value;
} PrintfArg;

// Macros for creating values of different types.  The names come from
// the printf '%' directives for the different types.  I'm on the fence
// as to whether longer names would be better.
#define PF_D(x) { PRINTF_INT, { .integer = (x) } }
#define PF_U(x) { PRINTF_UINT, { .unsignedInt = (x) } }
#define PF_S(x) { PRINTF_STRING, { .string = (x) } }
#define PF_C(x) { PRINTF_CHAR, { .ch = (x) } }
#define PF_E()  { PRINTF_END, { .integer =  0  } }

// The actual print function
int emSnprintfFunction(char *to,
                       size_t length,
                       const char *format,
                       const PrintfArg *args);

// The macro that assembles the arguments into an array.
#define emSnprintf(s, n, format, ...) \
  (emSnprintfFunction((s), (n), (format), (PrintfArg[]) { __VA_ARGS__, PF_E() }))

// Ditto, but using a simple struct to hold the string being printed
// to and the limit.  This makes it simpler to build up strings using
// multiple calls.  Returns true if no error occurred, false if one
// did.  Running out of room to print is not considered an error.
//
// emSnprintf() is implemented as a simple wrapper around emPbPrintf().

typedef struct {
  char *finger;
  char *limit;
  bool error;   // set to true if a formatting error occurred
                // must be initialized to false
} PrintBuffer;

#define emPbPrintf(pb, format, ...) \
  (emPbPrintfFunction((pb), (format), (PrintfArg[]) { __VA_ARGS__, PF_E() }))

bool emPbPrintfFunction(PrintBuffer *buffer,
                        const char *format,
                        const PrintfArg *args);
