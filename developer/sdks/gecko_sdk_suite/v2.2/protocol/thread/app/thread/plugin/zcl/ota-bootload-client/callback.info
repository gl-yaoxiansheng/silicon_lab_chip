#include PLATFORM_HEADER
#include CONFIGURATION_HEADER
#include EMBER_AF_API_ZCL_CORE
#include EMBER_AF_API_ZCL_OTA_BOOTLOAD_CORE
#include EMBER_AF_API_ZCL_OTA_BOOTLOAD_CLIENT


/**************************************************************************//**
 * This function attempts to set the cluster attribute values to those in the
 * policy parameters. The attributes affected are 'OTA Current File Version',
 * 'Manufacturer ID', and 'Image Type ID'.
 *
 * @return `true` if there was an error setting one of the attributes
 *
 *****************************************************************************/
bool emberZclOtaBootloadClientSetVersionInfoCallback()
{
  return false;
}

/**************************************************************************//**
 * Check to see if a static IP address should be used for the OTA server
 *
 * @param serverInfo Structure to store the OTA server parameters
 * @return `true` if the device should use a static address for the OTA Server
 *
 * @note If `true` is returned, the OTA Client state machine will move to the
 *       next state, which is querying the OTA Server device for a new OTA
 *       file to download. If `false` is returned, the OTA Client will continue
 *       to try to discover an OTA Server.
 *****************************************************************************/
bool emberZclOtaBootloadClientServerHasStaticAddressCallback(EmberZclOtaBootloadClientServerInfo_t *serverInfo)
{
	return false;
}

/**************************************************************************//**
 * Start a DNS hostname resolution
 *
 * @param serverInfo Updated to the default server parameters
 * @return `true` if the device should use the DNS hostname for the OTA Server
 *
 * @note If `true` is returned, the OTA Client state machine will wait for the
 *       DNS hostname to be resolved.
 *****************************************************************************/
bool emberZclOtaBootloadClientServerHasDnsNameCallback(EmberZclOtaBootloadClientServerInfo_t *serverInfo)
{
	return false;
}

/**************************************************************************//**
 * Initiate a server discovery
 *
 * @param clusterSpec server specifications
 * @param responseHandler function to handle the response to the CoAP message
 * @return `true` if the specification is valid and the request has been sent
 *
 * @note If `true` is returned, the OTA Client state machine will wait until the responseHandler is called
 *****************************************************************************/
bool emberZclOtaBootloadClientServerHasDiscByClusterId(const EmberZclClusterSpec_t *clusterSpec, EmberCoapResponseHandler responseHandler)
{
	return emberZclDiscByClusterId(clusterSpec, responseHandler);
}

/**************************************************************************//**
 * A potential OTA Server has been discovered.
 *
 * @param serverInfo Information about the discovered OTA Server device.
 * @return `true` if the device should be chosen as the OTA Server, `false`
 *         otherwise.
 *
 * @note If `true` is returned, the OTA Client state machine will move to the
 *       next state, which is querying the OTA Server device for a new OTA
 *       file to download. If `false` is returned, the OTA Client will continue
 *       to try to discover an OTA Server.
 *****************************************************************************/
bool emberZclOtaBootloadClientServerDiscoveredCallback(const EmberZclOtaBootloadClientServerInfo_t *serverInfo)
{
  return true;
}

/**************************************************************************//**
 * Get the parameters for a QueryNextImage command.
 *
 * @param fileSpec The OTA file specification to include in the query.
 * @param hardwareVersion The current hardware version of the device, or
 *                        ::EMBER_ZCL_OTA_BOOTLOAD_HARDWARE_VERSION_NULL if this
 *                        information is to be ignored.
 * @return `true` if the device should continue to send a QueryNextImage
 *         command, or `false` if it should reschedule the command for a later
 *         time.
 *****************************************************************************/
bool emberZclOtaBootloadClientGetQueryNextImageParametersCallback(EmberZclOtaBootloadFileSpec_t *fileSpec,
                                                                  EmberZclOtaBootloadHardwareVersion_t *hardwareVersion)
{
  return true;
}

/**************************************************************************//**
 * A download of an OTA file can be started.
 *
 * @param fileSpec The specification for the OTA file to potentially
 *                 be downloaded.
 * @param existingFile A file with this same specification currently exists in
 *                     OTA storage.
 * @return `true` if the device should kick off the download process for this
 *         file, or `false` if it should continue to query for new images.
 *
 * @sa emberZclOtaBootloadStorageFind
 *****************************************************************************/
bool emberZclOtaBootloadClientStartDownloadCallback(const EmberZclOtaBootloadFileSpec_t *fileSpec,
                                                    bool existingFile)
{
  return true;
}

/**************************************************************************//**
 * An OTA file has been downloaded and verified, or failed to do one of the two.
 *
 * @param fileSpec The specification for the OTA file in question.
 * @param status The status of file download and verification.
 * @return A status based on the application's analysis of the downloaded file.
 *         This is an opportunity for the application to verify the downloaded
 *         image and return a related status.
 *
 * @sa emberZclOtaBootloadStorageFind
 *****************************************************************************/
EmberZclStatus_t emberZclOtaBootloadClientDownloadCompleteCallback(const EmberZclOtaBootloadFileSpec_t *fileSpec,
                                                                   EmberZclStatus_t status)
{
  return status;
}

/**************************************************************************//**
 * An OTA file is about to be installed.
 *
 * @param fileSpec The specification for the OTA file that will be installed.
 *
 * @sa emberZclOtaBootloadStorageFind
 *****************************************************************************/
void emberZclOtaBootloadClientPreBootloadCallback(const EmberZclOtaBootloadFileSpec_t *fileSpec)
{
}
