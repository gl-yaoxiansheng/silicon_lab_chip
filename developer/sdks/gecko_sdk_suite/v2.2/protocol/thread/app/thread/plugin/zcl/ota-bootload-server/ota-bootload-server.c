// Copyright 2017 Silicon Laboratories, Inc.

#include PLATFORM_HEADER
#include CONFIGURATION_HEADER
#include EMBER_AF_API_STACK
#include EMBER_AF_API_ZCL_CORE
#include EMBER_AF_API_ZCL_OTA_BOOTLOAD_CORE
#include EMBER_AF_API_ZCL_OTA_BOOTLOAD_STORAGE_CORE

#ifdef EMBER_AF_API_DEBUG_PRINT
  #include EMBER_AF_API_DEBUG_PRINT
#else
  #define emberAfPluginOtaBootloadServerPrint(...)
  #define emberAfPluginOtaBootloadServerPrintln(...)
  #define emberAfPluginOtaBootloadServerFlush()
  #define emberAfPluginOtaBootloadServerDebugExec(x)
  #define emberAfPluginOtaBootloadServerPrintBuffer(buffer, len, withSpace)
  #define emberAfPluginOtaBootloadServerPrintString(buffer)
#endif

#ifdef EMBER_SCRIPTED_TEST
  #include "ota-bootload-server-test.h"
#else
  #include "thread-callbacks.h"
#endif

// TODO: do we need to support QuerySpecificFile command?
// TODO: add a callback for each block received so app can resize block?

// -----------------------------------------------------------------------------
// Constants

// See 11.11.1 (QueryJitter Parameter) from the OTA spec.
// "The server chooses the parameter value between 1 and 100 (inclusively) and
// includes it in the Image Notify Command. On receipt of the command, the
// client will examine other information (the manufacturer code and image type)
// to determine if they match its own values. If they do not, it SHALL discard
// the command and no further processing SHALL continue. If they do match then
// it will determine whether or not it SHOULD query the upgrade server. It does
// this by randomly choosing a number between 1 and 100 and comparing it to the
// value of the QueryJitter parameter received. If it is less than or equal to
// the QueryJitter value from the server, it SHALL continue with the query
// process. If not, then it SHALL discard the command and no further processing
// SHALL continue."
// I picked a value right in the middle of the range, because I am lazy.
#define DEFAULT_QUERY_JITTER 50

// See 11.13.4.2.1 (Query Next Image Request Command Field Control) from the OTA
// spec.
#define QUERY_NEXT_IMAGE_REQUEST_FIELD_CONTROL_HARDWARE_VERSION_PRESENT BIT(0)

// This is super duper arbitrary. It should be updated as needed. However, we
// should think about the MTU of the transport layer below us.
// Increased to 256 which results in a 2min OTA of a 250K file.
#define MAX_RESPONSE_BLOCK_SIZE 256

// -----------------------------------------------------------------------------
// Globals

EmberEventControl emZclOtaBootloadServerImageNotifyEventControl;

// -----------------------------------------------------------------------------
// Private API

static uint8_t getPayloadTypeForFileSpec(const EmberZclOtaBootloadFileSpec_t *fileSpec)
{
  // See section 11.13.3.2.1 ImageNotify Command Payload Type. This logic should
  // be communicated through the documentation for
  // emberZclOtaBootloadServerGetImageNotifyInfoCallback.
  uint8_t payloadType = 0x00;
  if (fileSpec->manufacturerCode != EMBER_ZCL_MANUFACTURER_CODE_NULL) {
    payloadType++;
    if (fileSpec->type != EMBER_ZCL_OTA_BOOTLOAD_FILE_TYPE_WILDCARD) {
      payloadType++;
      if (fileSpec->version != EMBER_ZCL_OTA_BOOTLOAD_FILE_VERSION_NULL) {
        payloadType++;
      }
    }
  }
  return payloadType;
}

static void imageNotifyResponseHandler(EmberZclMessageStatus_t status,
                                       const EmberZclCommandContext_t *context,
                                       const EmberZclClusterOtaBootloadClientCommandImageNotifyResponse_t *response)
{
  // TODO: huh?
}

// -----------------------------------------------------------------------------
// Public API downward

void emZclOtaBootloadServerNetworkStatusCallback(EmberNetworkStatus newNetworkStatus,
                                                 EmberNetworkStatus oldNetworkStatus,
                                                 EmberJoinFailureReason reason)
{
  if (newNetworkStatus == EMBER_JOINED_NETWORK_ATTACHED) {
    emberEventControlSetActive(emZclOtaBootloadServerImageNotifyEventControl);
  } else if (newNetworkStatus == EMBER_NO_NETWORK) {
    emberEventControlSetInactive(emZclOtaBootloadServerImageNotifyEventControl);
  }
}

void emZclOtaBootloadServerImageNotifyEventHandler(void)
{
  // Get ImageNotify stuff from application.
  EmberIpv6Address address = { { 0 } };
  EmberZclOtaBootloadFileSpec_t fileSpec = emberZclOtaBootloadFileSpecNull;
  if (emberZclOtaBootloadServerGetImageNotifyInfoCallback(&address,
                                                          &fileSpec)) {
    EmberZclDestination_t destination = {
      .network = {
        .address = { { 0, } }, // filled in below
        .flags = EMBER_ZCL_HAVE_IPV6_ADDRESS_FLAG,
        .port = EMBER_COAP_PORT,
      },
      .application = {
        .data = {
          .groupId = EMBER_ZCL_GROUP_ALL_ENDPOINTS,
        },
        .type = EMBER_ZCL_APPLICATION_DESTINATION_TYPE_GROUP,
      },
    };
    destination.network.address = address;
    EmberZclClusterOtaBootloadClientCommandImageNotifyRequest_t request = {
      .payloadType = getPayloadTypeForFileSpec(&fileSpec),
      .queryJitter = DEFAULT_QUERY_JITTER,
      .manufacturerId = fileSpec.manufacturerCode,
      .imageType = fileSpec.type,
      .newFileVersion = fileSpec.version,
    };
    EmberStatus status
      = emberZclSendClusterOtaBootloadClientCommandImageNotifyRequest(&destination,
                                                                      &request,
                                                                      imageNotifyResponseHandler);
    // If we fail to send the message, then just try again later.
    (void)status;
  }

  emberEventControlSetDelayMinutes(emZclOtaBootloadServerImageNotifyEventControl,
                                   EMBER_AF_PLUGIN_OTA_BOOTLOAD_SERVER_IMAGE_NOTIFY_PERIOD_MINUTES);
}

// -------------------------------------
// Command handling

void emberZclClusterOtaBootloadServerCommandQueryNextImageRequestHandler(
  const EmberZclCommandContext_t *context,
  const EmberZclClusterOtaBootloadServerCommandQueryNextImageRequest_t *request)
{
  EmberZclOtaBootloadFileSpec_t currentFileSpec = {
    .manufacturerCode = request->manufacturerId,
    .type = request->imageType,
    .version = request->currentFileVersion,
  };
  EmberZclOtaBootloadFileSpec_t nextFileSpec = emberZclOtaBootloadFileSpecNull;
  EmberZclStatus_t zclStatus
    = emberZclOtaBootloadServerGetNextImageCallback(&context->remoteAddress,
                                                    &currentFileSpec,
                                                    &nextFileSpec);
  EmberZclClusterOtaBootloadServerCommandQueryNextImageResponse_t response = {
    .status = zclStatus,
    .manufacturerId = nextFileSpec.manufacturerCode,
    .imageType = nextFileSpec.type,
    .fileVersion = nextFileSpec.version,
    .imageSize = 0, // conditionally filled in below
  };
  if (response.status == EMBER_ZCL_STATUS_SUCCESS) {
    EmberZclOtaBootloadStorageFileInfo_t storageFileInfo;
    EmberZclOtaBootloadStorageStatus_t storageStatus
      = emberZclOtaBootloadStorageFind(&nextFileSpec, &storageFileInfo);
    // TODO: handle this case more gracefully.
    assert(storageStatus == EMBER_ZCL_OTA_BOOTLOAD_STORAGE_STATUS_SUCCESS);
    response.imageSize = storageFileInfo.size;
  }

  EmberStatus status
    = emberZclSendClusterOtaBootloadServerCommandQueryNextImageResponse(context,
                                                                        &response);
  // TODO: what if we fail to send this message?
  assert(status == EMBER_SUCCESS);
}

void emberZclClusterOtaBootloadServerCommandUpgradeEndRequestHandler(
  const EmberZclCommandContext_t *context,
  const EmberZclClusterOtaBootloadServerCommandUpgradeEndRequest_t *request)
{
  EmberZclOtaBootloadFileSpec_t fileSpec = {
    .manufacturerCode = request->manufacturerId,
    .type = request->imageType,
    .version = request->fileVersion,
  };
  uint32_t upgradeTime
    = emberZclOtaBootloadServerUpgradeEndRequestCallback(&context->remoteAddress,
                                                         &fileSpec,
                                                         request->status);
  EmberStatus status;
  if (request->status != EMBER_ZCL_STATUS_SUCCESS) {
    // See 11.13.6.4 for this default response mandate.
    status = emberZclSendDefaultResponse(context, EMBER_ZCL_STATUS_SUCCESS);
  } else {
    // See 11.11.4 for discussion regarding these currentTime and upgradeTime
    // parameters.
    EmberZclClusterOtaBootloadServerCommandUpgradeEndResponse_t response = {
      .manufacturerId = fileSpec.manufacturerCode,
      .imageType = fileSpec.type,
      .fileVersion = fileSpec.version,
      .currentTime = 0, // this means upgradeTime is an offset from now
      .upgradeTime = upgradeTime,
    };
    status
      = emberZclSendClusterOtaBootloadServerCommandUpgradeEndResponse(context,
                                                                      &response);
  }
  // TODO: what if we fail to send this message?
  assert(status == EMBER_SUCCESS);
}

// -------------------------------------
// Block transfer handling

static uint8_t asciiToValue(uint8_t digit)
{
  if (('0' <= digit) && (digit <= '9')) {
    return digit - '0';
  }
  if (('a' <= digit) && (digit <= 'f')) {
    return digit - 'a' + 10;
  }
  if (('A' <= digit) && (digit <= 'F')) {
    return digit - 'A' + 10;
  }
  return 0xFF;
}

static bool readHexUInt16(const uint8_t *str, uint16_t *value)
{
  uint8_t count = 4;
  uint8_t nextDigit;
  *value = 0;
  while (count--) {
    nextDigit = asciiToValue(*(str++));
    if (nextDigit == 0xFF) {
      return true;
    }
    *value = (*value << 4) + nextDigit;
  }
  return false;
}

static bool readHexUInt32(const uint8_t *str, uint32_t *value)
{
  uint8_t count = 8;
  uint8_t nextDigit;
  *value = 0;
  while (count--) {
    nextDigit = asciiToValue(*(str++));
    if (nextDigit == 0xFF) {
      return true;
    }
    *value = (*value << 4) + nextDigit;
  }
  return false;
}

// This function expects a uri to match the format string "%s/%04X-%04X-%08X".
static bool readOtaUri(const uint8_t *uri, EmberZclOtaBootloadFileSpec_t *fileSpec)
{
  if (MEMCOMPARE(EM_ZCL_OTA_BOOTLOAD_UPGRADE_BASE_URI,
                 uri,
                 EM_ZCL_OTA_BOOTLOAD_UPGRADE_BASE_URI_LENGTH)
      != 0) {
    return true;
  }
  uri += EM_ZCL_OTA_BOOTLOAD_UPGRADE_BASE_URI_LENGTH + 1; // account for "<BASE_URI>/"
  if (readHexUInt16(uri, &(fileSpec->manufacturerCode))) {
    return true;
  }
  uri += 5; // account for "xxxx-"
  if (readHexUInt16(uri, &(fileSpec->type))) {
    return true;
  }
  uri += 5; // account for "xxxx-"
  if (readHexUInt32(uri, &(fileSpec->version))) {
    return true;
  }
  uri += 8; // account for "xxxxxxxx"

  return false;
}

void emZclOtaBootloadServerDownloadHandler(EmberCoapCode code,
                                           uint8_t *uri,
                                           EmberCoapReadOptions *options,
                                           const uint8_t *payload,
                                           uint16_t payloadLength,
                                           const EmberCoapRequestInfo *info)
{
  assert(code == EMBER_COAP_CODE_GET);

  EmberStatus status;
  EmberCoapCode responseCode = EMBER_COAP_CODE_501_NOT_IMPLEMENTED;
  EmberCoapOption responseOption;
  uint8_t responseOptionCount = 0;
  uint8_t data[MAX_RESPONSE_BLOCK_SIZE];
  size_t dataSize = 0;
  emberAfPluginOtaBootloadServerPrintln("Next block request");

  EmberCoapBlockOption blockOption;
  if (!emberReadBlockOption(options, EMBER_COAP_OPTION_BLOCK2, &blockOption)) {
    responseCode = EMBER_COAP_CODE_400_BAD_REQUEST;
    emberAfPluginOtaBootloadServerPrintln("Received bad request");
    goto sendResponse;
  }

  EmberZclOtaBootloadFileSpec_t nextImageFileSpec;
  if (readOtaUri(uri, &nextImageFileSpec)) {
    responseCode = EMBER_COAP_CODE_400_BAD_REQUEST;
    emberAfPluginOtaBootloadServerPrintln("Received invalid URI: %s", uri);
    goto sendResponse;
  }

  EmberZclOtaBootloadStorageFileInfo_t fileInfo;
  if (emberZclOtaBootloadStorageFind(&nextImageFileSpec, &fileInfo)
      != EMBER_ZCL_OTA_BOOTLOAD_STORAGE_STATUS_SUCCESS) {
    responseCode = EMBER_COAP_CODE_404_NOT_FOUND;
    emberAfPluginOtaBootloadServerPrintln("Request for non-existent file: %04x-%04x-%08x",
                                          nextImageFileSpec.manufacturerCode,
                                          nextImageFileSpec.type,
                                          nextImageFileSpec.version);
    goto sendResponse;
  }

  size_t blockOffset = emberBlockOptionOffset(&blockOption);
  if (blockOffset > fileInfo.size) {
    responseCode = EMBER_COAP_CODE_400_BAD_REQUEST;
    emberAfPluginOtaBootloadServerPrintln("Received block offset (%d) greater than file size (%d)",
                                          blockOffset,
                                          fileInfo.size);
    goto sendResponse;
  }

  size_t blockSize = emberBlockOptionSize(&blockOption);
  size_t undownloadedFileSize = fileInfo.size - blockOffset;
  dataSize = (undownloadedFileSize < blockSize
              ? undownloadedFileSize
              : blockSize);
  if (dataSize > MAX_RESPONSE_BLOCK_SIZE
      || (emberZclOtaBootloadStorageRead(&nextImageFileSpec,
                                         blockOffset,
                                         data,
                                         dataSize)
          != EMBER_ZCL_OTA_BOOTLOAD_STORAGE_STATUS_SUCCESS)) {
    responseCode = EMBER_COAP_CODE_500_INTERNAL_SERVER_ERROR;
    emberAfPluginOtaBootloadServerPrintln("Unable to read from OTA storage");
    goto sendResponse;
  }

  responseCode = EMBER_COAP_CODE_205_CONTENT;
  emberInitCoapOption(&responseOption,
                      EMBER_COAP_OPTION_BLOCK2,
                      emberBlockOptionValue(undownloadedFileSize > blockSize,
                                            blockOption.logSize,
                                            blockOption.number));
  responseOptionCount = 1;
  emberAfPluginOtaBootloadServerPrintln("Accessing block: %x", blockOption.number);

  sendResponse:
  emberAfPluginOtaBootloadServerPrintln("Sending response: %x", responseCode);
  status = emberCoapRespond(info,
                            responseCode,
                            &responseOption,
                            responseOptionCount,
                            data,
                            dataSize);
  // TODO: what if this fails?
  assert(status == EMBER_SUCCESS);
}
