// Copyright 2016 Silicon Laboratories, Inc.

#ifndef ZCL_ON_OFF_SERVER_H
#define ZCL_ON_OFF_SERVER_H

// Define OnOff plugin Scenes sub-table structure.
typedef struct {
  bool hasOnOffValue;
  bool onOffValue;
} EmZclOnOffSceneSubTableEntry_t;

#endif // ZCL_ON_OFF_SERVER_H
