// Copyright 2017 Silicon Laboratories, Inc.

#ifndef TIME_SERVER_H
#define TIME_SERVER_H

void emAfTimeClusterServerSetCurrentTime(uint32_t utcTime);
uint32_t emAfTimeClusterServerGetCurrentTime(void);

#endif //TIME_SERVER_H
