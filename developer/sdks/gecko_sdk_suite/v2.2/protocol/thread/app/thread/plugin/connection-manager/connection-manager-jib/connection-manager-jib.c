// Copyright 2017 Silicon Laboratories, Inc.

#include PLATFORM_HEADER
#include CONFIGURATION_HEADER
#include EMBER_AF_API_STACK
#include EMBER_AF_API_HAL
#include EMBER_AF_API_CONNECTION_MANAGER
#include EMBER_AF_API_CONNECTION_MANAGER_JIB
#ifdef EMBER_AF_API_DEBUG_PRINT
  #include EMBER_AF_API_DEBUG_PRINT
#endif

#define ROUTER            EMBER_ROUTER
#define END_DEVICE        EMBER_END_DEVICE
#define SLEEPY_END_DEVICE EMBER_SLEEPY_END_DEVICE

#define NETWORK_JOIN_RETRY_DELAY_MS (1024 * 5)

#define TIME_TO_WAIT_FOR_LONG_POLLING_MS 60000

//------------------------------------------------------------------------------
// Event function forward declaration
EmberEventControl emConnectionManagerNetworkStateEventControl;
EmberEventControl emConnectionManagerOrphanEventControl;
EmberEventControl emConnectionManagerOkToLongPollEventControl;

static void resetOkToLongPoll(void);
static void setNextState(uint8_t nextState);
static void joinNetwork(void);

enum {
  INITIAL                              = 0,
  RESUME_NETWORK                       = 1,
  JOIN_NETWORK                         = 2,
  WAIT_FOR_JOIN_NETWORK                = 3,
  JOINED_TO_NETWORK                    = 4,
  RESET_NETWORK_STATE                  = 5,
  ATTACH_NETWORK_STATE                 = 6,
};

static uint8_t state = INITIAL;
static uint8_t joinAttemptsRemaining = 0;
static bool isOrphaned = false;
static bool isSearching = false;

static bool okToLongPoll = false;

static uint8_t *joinKey;
static uint8_t joinKeyLength = 0;

// The rejoin delay will be calculated by the formula, below:
// (ORPHAN_REJOIN_DELAY_MINUTES/(INITIAL_REJOIN_DIVISOR-orphanedJoinAttempts))
// Where orphanedJoinAttempts will increment until a full ORPHAN_REJOIN_DELAY_MINUTES
// occurs between each attempt.  This allows fast responsiveness when a device
// is first orphaned, but eventually decays to a longer polling interval in
// order to conserve battery life.
#define INITIAL_REJOIN_DIVISOR 7
static uint8_t orphanedJoinAttempts = 0;

static void attachNetworkState(void);

void emberConnectionManagerStartConnect(void)
{
  // This function will attempt to rejoin a network in all cases, if it is
  // already attached to a network it will perform a reset which automatically
  // tries to rejoin
  EmberNetworkStatus networkStatus = emberNetworkStatus();
  isSearching = true;

  switch (networkStatus) {
    case EMBER_SAVED_NETWORK:
      joinAttemptsRemaining = EMBER_AF_PLUGIN_CONNECTION_MANAGER_JIB_NUM_REJOIN_ATTEMPTS;
      setNextState(RESUME_NETWORK);
      break;
    case EMBER_NO_NETWORK:
    case EMBER_JOINED_NETWORK_ATTACHED:
    default:
      // If there is no saved network, then get the prefix joining key, set the
      // join attempts to the maximum, and reset the network state to trigger a
      // join/rejoin
      joinKeyLength = emberConnectionManagerJibGetJoinKeyCallback(&joinKey);

      if (joinKeyLength == 0) {
        emberAfCorePrintln("No join key set!");
        emberConnectionManagerConnectCompleteCallback(EMBER_CONNECTION_MANAGER_STATUS_NO_KEY);
        return;
      }

      joinAttemptsRemaining = EMBER_AF_PLUGIN_CONNECTION_MANAGER_JIB_NUM_REJOIN_ATTEMPTS;
      setNextState(RESET_NETWORK_STATE);
      break;
  }
}

void emberConnectionManagerStopConnect(void)
{
  EmberNetworkStatus networkStatus = emberNetworkStatus();

  // if we're already on the network, then a start connect should do nothing
  if (networkStatus == EMBER_JOINED_NETWORK_ATTACHED) {
    return;
  }

  joinAttemptsRemaining = 0;
  isSearching = false;
}

void emConnectionManagerJibNetworkStatusHandler(EmberNetworkStatus newNetworkStatus,
                                                EmberNetworkStatus oldNetworkStatus,
                                                EmberJoinFailureReason reason)
{
  // This callback is called whenever the network status changes, like when
  // we finish joining to a network or when we lose connectivity.  If we have
  // no network, we try joining to one as long as join attempts remain.  Join
  // attempts are reset whenever emberConnectionManagerStartConnect is called.
  // If we have a saved network, we try to resume operations on that network.

  emberEventControlSetInactive(emConnectionManagerNetworkStateEventControl);

  switch (newNetworkStatus) {
    case EMBER_NO_NETWORK:
      if (oldNetworkStatus == EMBER_JOINING_NETWORK) {
        emberAfCorePrintln("ERR: Joining failed: 0x%x", reason);
      }
      setNextState(JOIN_NETWORK);
      break;
    case EMBER_SAVED_NETWORK:
      setNextState(RESUME_NETWORK);
      break;
    case EMBER_JOINING_NETWORK:
      // Wait for either the "attaching" or "no network" state.
      break;
    case EMBER_JOINED_NETWORK_ATTACHING:
      // Wait for the "attached" state.
      if (oldNetworkStatus == EMBER_JOINED_NETWORK_ATTACHED) {
        emberAfCorePrintln("Trying to re-connect...");
        isOrphaned = true;
      }
      break;
    case EMBER_JOINED_NETWORK_ATTACHED:
      emberAfCorePrintln("Connected to network: %s",
                         (state == RESUME_NETWORK
                          ? "Resumed"
                          : (state == JOIN_NETWORK
                             ? "Joined"
                             : "Rejoined")));
      joinAttemptsRemaining = 0;
      isSearching = false;
      isOrphaned = false;
      orphanedJoinAttempts = 0;
      if (EMBER_AF_PLUGIN_CONNECTION_MANAGER_JIB_ROLE == SLEEPY_END_DEVICE) {
        emberEventControlSetDelayMS(emConnectionManagerOkToLongPollEventControl, TIME_TO_WAIT_FOR_LONG_POLLING_MS);
      }
      emberEventControlSetInactive(emConnectionManagerOrphanEventControl);
      setNextState(JOINED_TO_NETWORK);
      emberConnectionManagerConnectCompleteCallback(EMBER_CONNECTION_MANAGER_STATUS_CONNECTED);

      break;
    case EMBER_JOINED_NETWORK_NO_PARENT:
      // Try to attach to the network
      setNextState(ATTACH_NETWORK_STATE);
      break;
    default:
      assert(false);
      break;
  }
}

void emConnectionManagerOkToLongPollHandler(void)
{
  emberAfCorePrintln("%dms has passed, now OK to long poll",
                     TIME_TO_WAIT_FOR_LONG_POLLING_MS);
  emberEventControlSetInactive(emConnectionManagerOkToLongPollEventControl);
  okToLongPoll = true;
}

bool emberConnectionManagerIsOkToLongPoll(void)
{
  return okToLongPoll;
}

void emberConnectionManagerSearchForParent(void)
{
  if (isOrphaned) {
    emberEventControlSetActive(emConnectionManagerOrphanEventControl);
  }
}

void emConnectionManagerOrphanEventHandler(void)
{
  emberEventControlSetInactive(emConnectionManagerOrphanEventControl);
  isOrphaned = true;
  emberAfCorePrintln("Device still orphaned, attempt to reattach");
  emberAttachToNetwork();
}

bool emberConnectionmanagerIsOrphaned(void)
{
  return isOrphaned;
}

bool emberConnectionManagerIsSearching(void)
{
  return isSearching;
}

void emberConnectionManagerLeaveNetwork(void)
{
  emberResetNetworkState();
}

static void resumeNetwork(void)
{
  emberAfCorePrintln("Resuming...");
  emberResumeNetwork();
}

void emberResumeNetworkReturn(EmberStatus status)
{
  // This return indicates whether the stack is going to attempt to resume.  If
  // so, the result is reported later as a network status change. This API will
  // only fail to return EMBER_SUCCESS if the API was called when a device was
  // already on a network.

  if (status != EMBER_SUCCESS) {
    emberAfCorePrintln("WARNING: resume network called, but device was already"
                       " on a network.");
    //Optionally, code could halt on this error:
    //assert(0);
  }
}

static void joinNetwork(void)
{
  // When joining a network, we look for one specifically with our network id.
  // The commissioner must have our join key for this to succeed.

  EmberNetworkParameters parameters = { { 0 } };

  assert(state == JOIN_NETWORK);

  if (joinKeyLength == 0) {
    // It shouldn't be possible to get here without having first gone through
    // the startConnect() call.  In case that somehow happens, provide a path
    // to get the key anyway.  If that still doesn't provide a valid key, then
    // there's nothing we can do to join the network
    joinKeyLength = emberConnectionManagerJibGetJoinKeyCallback(&joinKey);

    if (joinKeyLength == 0) {
      emberAfCorePrintln("No join key set!");
      return;
    }
  }

  if (joinAttemptsRemaining > 0) {
    // Decrement the join counter
    joinAttemptsRemaining--;

    emberAfCorePrintln("Joining network using join passphrase \"%s\", join attempts left = %d",
                       joinKey,
                       joinAttemptsRemaining);

    parameters.nodeType = EMBER_AF_PLUGIN_CONNECTION_MANAGER_JIB_ROLE;
    parameters.radioTxPower = EMBER_AF_PLUGIN_CONNECTION_MANAGER_JIB_RADIO_TX_POWER;
    parameters.joinKeyLength = joinKeyLength;
    MEMCOPY(parameters.joinKey, joinKey, parameters.joinKeyLength);

    emberJoinNetwork(&parameters,
                     (EMBER_NODE_TYPE_OPTION
                      | EMBER_TX_POWER_OPTION
                      | EMBER_JOIN_KEY_OPTION),
                     EMBER_ALL_802_15_4_CHANNELS_MASK);
  } else {
    // No join attempts remain
    isSearching = false;
    emberAfCorePrintln("Unable to join within %d attempts",
                       EMBER_AF_PLUGIN_CONNECTION_MANAGER_JIB_NUM_REJOIN_ATTEMPTS);
    emberConnectionManagerConnectCompleteCallback(EMBER_CONNECTION_MANAGER_STATUS_TIMED_OUT);
  }
}

void emberJoinNetworkReturn(EmberStatus status)
{
  // This return indicates whether the stack is going to attempt to join.  If
  // so, the result is reported later as a network status change.  Otherwise,
  // we just try to join again as long as join attempts remain.

  if (status != EMBER_SUCCESS) {
    emberAfCorePrintln("ERR: Unable to join: 0x%x", status);
    setNextState(JOIN_NETWORK);
  } else {
    setNextState(WAIT_FOR_JOIN_NETWORK);
  }
}

void emberAttachToNetworkReturn(EmberStatus status)
{
  // This return indicates whether the stack is going to attempt to attach.  If
  // so, the result is reported later as a network status change.  If we cannot
  // currently attach, wait for another ORPHAN_REJOIN_DELAY_MINUTES before
  // triggering a fresh join attempt

  // Subsequent attach attempts are made at longer timeouts to save batteries.
  if ((INITIAL_REJOIN_DIVISOR - orphanedJoinAttempts) > 1 ) {
    orphanedJoinAttempts++;
  }

  // Handle the error.
  if (status != EMBER_SUCCESS) {
    emberAfCorePrintln("ERR: Stack unable to schedule reattach: 0x%x", status);
    switch (emberNetworkStatus()) {
      case EMBER_JOINED_NETWORK_NO_PARENT:
        setNextState(ATTACH_NETWORK_STATE);
        break;
      case EMBER_SAVED_NETWORK:
      case EMBER_NO_NETWORK:
        // Someone tried to attach without a network. This is a fatal
        // programming error.
        emberAfCorePrintln("ERROR: attempted to attach to a network before"
                           " joining.");
        assert(0);
        break;
      case EMBER_JOINING_NETWORK:
      case EMBER_JOINED_NETWORK_ATTACHING:
        // An attach was scheduled while a pending attach or join had not yet
        // completed. Silently do nothing: the network state machine handles it
        break;
      case EMBER_JOINED_NETWORK_ATTACHED:
        // Somebody forgot to disable the orphan event controller.
        emberAfCorePrintln("WARNING: orphan event control entered on a"
                           " non-orphaned device.");
        emberEventControlSetInactive(emConnectionManagerOrphanEventControl);
        setNextState(JOINED_TO_NETWORK);
        break;
      default:
        assert(0);
        break;
    }
  }
}

static void attachNetworkState(void)
{
  uint8_t delay
    = EMBER_AF_PLUGIN_CONNECTION_MANAGER_JIB_ORPHAN_REJOIN_DELAY_MINUTES
      / (INITIAL_REJOIN_DIVISOR - orphanedJoinAttempts);

  if (orphanedJoinAttempts > 0) {
    // If orphaned previously, wait to attempt a re-attach
    emberAfCorePrintln("Retrying attach in %d minutes", delay);
    emberEventControlSetDelayMinutes(emConnectionManagerOrphanEventControl, delay);
  } else {
    // If never orphaned before, attempt a rejoin immediately
    orphanedJoinAttempts = 1;
    if (emberNetworkStatus() == EMBER_JOINED_NETWORK_NO_PARENT) {
      emberAttachToNetwork();
    }
  }
}

static void resetNetworkState(void)
{
  emberAfCorePrintln("Resetting...");
  emberResetNetworkState();
}

void emberResetNetworkStateReturn(EmberStatus status)
{
  // If we ever leave the network, we go right back to joining again.  This
  // could be triggered by an external CLI command.

  if (status != EMBER_SUCCESS) {
    emberResetNetworkState();
  } else {
    joinAttemptsRemaining = EMBER_AF_PLUGIN_CONNECTION_MANAGER_JIB_NUM_REJOIN_ATTEMPTS;
    isSearching = false;
    isOrphaned  = false;
    emberEventControlSetInactive(emConnectionManagerOrphanEventControl);
    orphanedJoinAttempts = 0;
  }
}

void emConnectionManagerNetworkStateEventHandler(void)
{
  emberEventControlSetInactive(emConnectionManagerNetworkStateEventControl);

  switch (state) {
    case RESUME_NETWORK:
      resetOkToLongPoll();
      resumeNetwork();
      break;
    case JOIN_NETWORK:
      resetOkToLongPoll();
      joinNetwork();
      break;
    case WAIT_FOR_JOIN_NETWORK:
      // Wait for joinNetwork to complete
      break;
    case JOINED_TO_NETWORK:
      break;
    case RESET_NETWORK_STATE:
      resetOkToLongPoll();
      resetNetworkState();
      break;
    case ATTACH_NETWORK_STATE:
      resetOkToLongPoll();
      attachNetworkState();
      break;
    default:
      assert(false);
  }
}

static void resetOkToLongPoll(void)
{
  emberAfCorePrintln("Resetting OK to Long Poll");
  emberEventControlSetInactive(emConnectionManagerOkToLongPollEventControl);
  okToLongPoll = false;
}

static void setNextState(uint8_t nextState)
{
  state = nextState;
  emberEventControlSetActive(emConnectionManagerNetworkStateEventControl);
}
