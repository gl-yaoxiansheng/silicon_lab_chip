// Copyright 2018 Silicon Laboratories, Inc.

#include PLATFORM_HEADER
#include CONFIGURATION_HEADER
#include EMBER_AF_API_STACK
#include EMBER_AF_API_ZCL_CORE_WELL_KNOWN

#include "resource-directory-client.h"

#ifdef EMBER_AF_API_DEBUG_PRINT
  #include EMBER_AF_API_DEBUG_PRINT
#endif

#define MAX_PATH_WITH_QUERY (64) // This needs to be more sophisticated. Picking a number for now.

void emAfPluginResourceDirectoryClientRegisterCommand(void);

static uint8_t constructRegistrationUri(uint8_t *uri);
static int16_t constructRegistrationPayload(uint8_t *payload);

void emberAfPluginResourceDirectoryClientRegister(EmberIpv6Address *resourceDirectoryIp, uint16_t resourceDirectoryPort)
{
  emberAfCorePrintln("Registering with resource directory");

  uint8_t uri[MAX_PATH_WITH_QUERY];
  constructRegistrationUri(uri);

  uint8_t payload[EM_ZCL_MAX_WELL_KNOWN_REPLY_PAYLOAD];
  int16_t payloadLength = constructRegistrationPayload(payload);

  if (payloadLength == -1) {
    return;
  }

  EmberCoapSendInfo info = { 0 };
  info.remotePort = resourceDirectoryPort;

  EmberStatus status = emberCoapPost(resourceDirectoryIp,
                                     uri,
                                     payload,
                                     payloadLength,
                                     NULL,
                                     &info);

  if (status != EMBER_SUCCESS) {
    emberAfCorePrintln("ERR: Registration failed: 0x%x", status);
  } else {
    emberAfCorePrintln("Registration Success");
  }
}

void emAfPluginResourceDirectoryClientRegisterCommand(void)
{
  EmberIpv6Address resourceDirectoryIp;
  emberGetIpv6AddressArgument(0, &resourceDirectoryIp);

  uint16_t resourceDirectoryPort = (uint16_t)emberUnsignedCommandArgument(1);

  emberAfPluginResourceDirectoryClientRegister(&resourceDirectoryIp, resourceDirectoryPort);
}

static uint8_t constructRegistrationUri(uint8_t *uri)
{
  uint8_t *finger = uri;
  finger += sprintf(finger, "/rd?");
  finger += sprintf(finger, "ep=ni:///sha-256;");
  finger += emZclUidToBase64Url(&emZclUid, EMBER_ZCL_UID_BITS, (uint8_t *)finger);
  return finger - uri;
}

static int16_t constructRegistrationPayload(uint8_t *payload)
{
  EmZclDiscPayloadContext_t dpc;
  emZclInitDiscPayloadContext(&dpc, EMBER_COAP_CONTENT_FORMAT_LINK_FORMAT_PLUS_CBOR, payload, EM_ZCL_MAX_WELL_KNOWN_REPLY_PAYLOAD);
  dpc.startPayload(&dpc);

  uint8_t i;
  for (i = 0; i < emZclEndpointCount; i++) {
    const EmZclEndpointEntry_t *epEntry = &emZclEndpointTable[i];
    const EmberZclClusterSpec_t **clusterSpecs = epEntry->clusterSpecs;
    const EmberZclClusterSpec_t *spec = NULL;
    spec = *clusterSpecs;

    while (spec != NULL) {
      dpc.startLink(&dpc);
      dpc.addResourceUri(&dpc, epEntry->endpointId, spec);
      dpc.addRt(&dpc, spec, true);
      dpc.addIf(&dpc, epEntry->endpointId, spec);
      dpc.addZe(&dpc, epEntry->endpointId, epEntry->deviceId);
      dpc.endLink(&dpc);

      clusterSpecs++;
      spec = *clusterSpecs;
    }
  }

  dpc.endPayload(&dpc);

  if (dpc.status == DISCOVERY_PAYLOAD_CONTEXT_STATUS_SUCCESS) {
    MEMCOPY(payload, dpc.payloadPointer(&dpc), dpc.payloadLength(&dpc));
    return dpc.payloadLength(&dpc);
  } else {
    emberAfCorePrintln("ERR: Failed to construct registration payload 0x%x", dpc.status);
    return -1;
  }
}
