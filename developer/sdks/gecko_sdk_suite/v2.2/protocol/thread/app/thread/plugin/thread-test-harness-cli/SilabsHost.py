'''
>> Thread Host Controller Interface
>> Device : Silabs THCI
>> Class : SiLabs
'''

from IThci import IThci
import telnetlib
import time
import serial
from GRLLibs.UtilityModules import Test
from GRLLibs.UtilityModules.Test import Thread_Device_Role,Device_Data_Requirement, MacType
from string import rstrip
from GRLLibs.UtilityModules.ModuleHelper import ModuleHelper,ThreadRunner
from GRLLibs.UtilityModules.enums import PlatformDiagnosticPacket_Direction,PlatformDiagnosticPacket_Type, AddressType
from GRLLibs.ThreadPacket.PlatformPackets import PlatformDiagnosticPacket, PlatformPackets
from Queue import Queue
from GRLLibs.UtilityModules.enums import *
from GRLLibs.UtilityModules.ConsoleLogger import ConsoleLogger
from GRLLibs.UtilityModules.Plugins.AES_CMAC import Thread_PBKDF2
from sets import *
import ipaddress
#from GRLLibs.UtilityModules.Device import Thread_Device_Names
#from enum import Enum
class SiLabs(IThci):   
   UIStatusMsg = ''

   #DEFINED VALUES FOR THIS THCI
   LOWEST_POSSIBLE_PARTATION_ID = 0
   LINK_QUALITY_CHANGE_TIME = 100

   Firmware = "SL-Thread version 2.6.0 build 60, change 181248"
   #def __init__(self,PORT,HOST,MAC_Address,DeviceType=Thread_Device_Names.SiLabs_IP):
   def __init__(self,**kwargs):
      try:
         self.IsBlackListingEnabled = False
         self.BlackList = set()
         self.Channel = ModuleHelper.Default_Channel
         self.NwkName = ModuleHelper.Default_NwkName
         self.NwkKey = hex(ModuleHelper.Default_NwkKey).lstrip('0x').rstrip('L').zfill(32)
         self.PanId = "0x"+hex(ModuleHelper.Default_PanId).lstrip('0x').zfill(4)
         self.XpanId = hex(ModuleHelper.Default_XpanId).lstrip('0x').rstrip('L').zfill(16)
         self.MLPrefix = ModuleHelper.Default_MLPrefix
         self.PSKc = ModuleHelper.Default_PSKc
         self.SecurityPolicySecs = ModuleHelper.Default_SecurityPolicy
         self.ActiveTimestamp = ModuleHelper.Default_ActiveTimestamp
         self.SED_Polling_Rate = ModuleHelper.Default_Harness_SED_Polling_Rate
         self.KeySequenceCounter = 0         
         self.Sig_strength = 3         
         self.channel_mask = 0      
         self.DeviceRole = None
         self.MAC = kwargs.get('EUI')            
         self.Sequence = 0         
         self.shortAddress  = -1
         self.PingSequence = 0         
         self.AutoDUTEnable = False
         self.LastBufferData = None
         self.deviceConnected = False
         self.SED_Polling_Rate = ModuleHelper.Default_Harness_SED_Polling_Rate
         self.__logThread = []
         self.__isComJoinModeSet = False
         self.DeviceType = (kwargs.get('Param5')).lower() if kwargs.get('Param5') != None else "USB"
         self.intialize(kwargs.get('SerialPort'),kwargs.get('SerialBaudRate'),kwargs.get('TelnetIP'),kwargs.get('TelnetPort'))    
         if self.deviceConnected:  
            self.setChannel(self.Channel)     
            self.setPANID(self.PanId)
      except Exception,e:
         self.deviceConnected = False
         ModuleHelper.WriteIntoDebugLogger('Silabs.py - Cannot Start SiLabs Device '+str(e))
         raise BaseException,"Check SiLabs device is connected or Check is it used by another instance"
         return False
      
   def __sendCommand(self,command,waitForOutput= 1):
      ConsoleLogger.checkLogger()
      self.readBuffer() 
      print hex(self.MAC) +" : "+command + "\n"
      self.deviceConnection.write("\n")           
      self.deviceConnection.write(str(command) +" \n")   
      #self.deviceConnection.write("\n")
      time.sleep(waitForOutput)   

      if self.DeviceType == 'ip':
         self.deviceConnection.write("\n")
      #f = open('Output.txt','a')
      #f.write(str(self.MAC) + ': '+ str(command) + '\n')
      #f.close()

      cmdOutput = self.readBuffer()
      print hex(self.MAC)+ ": " + " : ".join(cmdOutput) 
      print "--------------------------------------------------------------------------------------------\n"
      return cmdOutput

   def __sendLoginCommand(self,command):
      ConsoleLogger.checkLogger()
      self.readBuffer() 
      print hex(self.MAC) +" : "+command + "\n"          
      self.deviceConnection.write(str(command) +"\n")   
      #self.deviceConnection.write("\n")
      time.sleep(4)   

      #f = open('Output.txt','a')
      #f.write(str(self.MAC) + ': '+ str(command) + '\n')
      #f.close()

      cmdOutput = self.readBuffer()
      print hex(self.MAC)+ ": " + " : ".join(cmdOutput) 
      print "--------------------------------------------------------------------------------------------\n"
      return cmdOutput

   def intialize(self,SerialPort = 0,SerialBaudRate = 0, TelnetIp = 0 , TelnetPort = 0):
      try:
         self.LastBufferData = None

         if self.DeviceType == 'ip':
            self.deviceConnection = telnetlib.Telnet(host=TelnetIp,port=TelnetPort,timeout = 3) 
            self.deviceConnection.set_debuglevel(0)
            output=""
            time.sleep(3)
            self.deviceConnection.write("\n")
            ## Login name
            self.__sendLoginCommand("pi") 
            ##self.__sendLoginCommand("pi")
            ## password to login
            self.__sendLoginCommand("raspberrypi")   
            time.sleep(5) 
            ## Send the commands to start the ip-driver-app and host app
            self.__sendCommand("ip-driver-start", waitForOutput=2)
            output = self.__sendCommand("br-start", waitForOutput=25)
            if self.Firmware in self.getVersionNumber():
               self.UIStatusMsg = self.Firmware
               self.deviceConnected = True
            else:
               self.UIStatusMsg = self.getVersionNumber()
               #self.deviceConnected = False #Temporary fix
               self.deviceConnected = True
              
         else:          
            try:  
               self.deviceConnection = serial.Serial(SerialPort,115200,timeout=0.1)#,writeTimeout = 0.20)
               #print self.Firmware in self.getVersionNumber()               
               #self.UIStatusMsg = self.Firmware               

               if self.Firmware in self.getVersionNumber():
                  self.UIStatusMsg = self.Firmware
                  self.deviceConnected = True
                  #self.reset()                
               
               else:
                  self.UIStatusMsg = "Firmware Not Matching / Excepting "+ self.Firmware
                  ModuleHelper.WriteIntoDebugLogger("Err: Silabs device Firmware not matching..")

                                
            except Exception,e:
               self.deviceConnected = False
               self.UIStatusMsg = "Not Connected"
               return False
               raise BaseException,"Err : Silabs.py - intialize() - Could not connect to the device "+str(SerialPort)
         if self.deviceConnected:
            self.__sendCommand("reboot")
            time.sleep(8)        
            self.__sendCommand("reset_network")                
            self.__sendCommand("enable_app_callbacks 0")
            
            self.setMAC(self.MAC)

            self.joinKey = 'GRLpassWord'
            print "Accessing SiLabs terminal"
               
      except Exception,e:
         self.deviceConnected = False
         self.UIStatusMsg = "Not Connected"
         return False
         
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py intialize(): " + str(e))

   def setChannel(self,channel):
      try:
         self.Channel = channel
         self.__sendCommand("set_channel " + str(channel))           
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py setChannel(): " + str(e))

   def setNetworkName(self,networkName):
      self.NwkName = networkName

   def addIntoNetwork(self,RoleID):
      try:         
         Sig_Strength = "3"         
         channel_mask = "0"     
         self.DeviceRole = RoleID
       
         self.setMAC(self.MAC)           
         self.__sendCommand("randomize_mac_extended_id 0")#Disables use of Random MAC

         if RoleID == Test.Thread_Device_Role.Leader:             
            configCmd = "configure" + " " + str(self.Channel) + " " + channel_mask + " \"" + str(self.NwkName) + "\" " + "\"" + self.MLPrefix + "\"" + " " + "{" + self.XpanId + "}" + " " + "{" + self.NwkKey + "} " + self.PanId + " " +  str(self.KeySequenceCounter)           
            self.__sendCommand(configCmd)
            #formPanCmd = "form_pan" + " " + str(self.Channel) + " " + Sig_Strength + " 2 " + self.PanId + " " + "\"" + str(self.NwkName) + "\"" + "" + " " + "" + "\"" + "FD00:0DB8::" + "\""            
            formPanCmd = "form_pan" + " " + str(self.Channel) + " " + Sig_Strength + " 2 " + self.PanId + " " + "\"" + str(self.NwkName) + "\"" + "" + " " + "" + "\"" + self.MLPrefix + "\""            
            self.__sendCommand(formPanCmd)           
           
         elif RoleID == Test.Thread_Device_Role.Router:   
            configCmd = "configure" + " " + str(self.Channel) + " " + channel_mask + " \"" + str(self.NwkName) + "\" " + "\"" + self.MLPrefix + "\"" + " " + "{" + self.XpanId + "}" + " " + "{" + self.NwkKey + "} " + self.PanId           
            self.__sendCommand(configCmd)                 

         elif RoleID == Test.Thread_Device_Role.REED:                    
            configCmd = "configure" + " " + str(self.Channel) + " " + channel_mask + " \"" + str(self.NwkName) + "\" " + "\"" + self.MLPrefix + "\"" + " " + "{" + self.XpanId + "}" + " " + "{" + self.NwkKey + "} " + self.PanId
            self.__sendCommand('set_router_selection_parameters 1 64 64')      
            self.__sendCommand(configCmd)     
            

         elif RoleID == Test.Thread_Device_Role.SED:                      
            configCmd = "configure" + " " + str(self.Channel) + " " + channel_mask + " \"" + str(self.NwkName) + "\" " + "\"" + self.MLPrefix + "\"" + " " + "{" + self.XpanId + "}" + " " + "{" + self.NwkKey + "} " + self.PanId            
            self.__sendCommand(configCmd)                

         else: 
            RoleID == Test.Thread_Device_Role.EndDevice                  
            configCmd = "configure" + " " + str(self.Channel) + " " + channel_mask + " \"" + str(self.NwkName) + "\" " + "\"" + self.MLPrefix + "\"" + " " + "{" + self.XpanId + "}" + " " + "{" + self.NwkKey + "} " + self.PanId            
            self.__sendCommand(configCmd)        
            
            
         return True
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py addIntoNetwork(): " + str(e))
         return False
            
   def getDeviceConncetionStatus(self):
      return self.deviceConnected      
        


   def getChannel(self): 
      try:
         buff = self.__sendCommand("status")               
         channel = buff[1].strip().replace(',','')
         return str(channel)
      except UnboundLocalError,e:
         return "Err:Channel not set",e
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py getChannel(): " + str(e))

   def getMAC(self,bType = MacType.RandomMac):
      try:
           try:
               MAC = self.MAC
               if bType == MacType.RandomMac:
                  buff = self.__sendCommand("network_state")      
                  for line in buff:
                     if "mac extended id:" in line:           
                        MACinStrParse1 = line.split(':')[1]
                        MACinStr = MACinStrParse1.replace('{','').replace('}','')
                        MAC = int('0x'+MACinStr.strip(),16)
                        print "My MAC : "+str(hex(MAC))
                        break       
                  return MAC
               elif bType == MacType.FactoryMac:                  
                  buff = self.__sendCommand("print_eui")   
                  EUI = 0   
                  for line in buff:
                     if "EUI64:" in line:           
                        euiinStrreversed = line.split(':')[1].strip()
                        EUIinListRev = euiinStrreversed.split(' ')
                        EUIinList = EUIinListRev[::-1]
                        EUIinStr = ''.join(EUIinList)
                        EUI = int(EUIinStr,16)
                        print "My EUI : "+str(hex(EUI))
                        break       
                  return EUI
               elif bType == MacType.HashMac:
                  factoryMac = self.getMAC(MacType.FactoryMac)
                  HashMac = ModuleHelper.CalculateHashMac(factoryMac)
                  return HashMac
               return MAC
           except Exception,e:
              ModuleHelper.WriteIntoDebugLogger("Silabs.py - getMAC "+str(e))
              return False
      except Exception, e:
              ModuleHelper.WriteIntoDebugLogger("Silabs.py - getMAC "+str(e))
              return False


   def setMAC(self,EUI):
      try:
          
         self.__sendCommand("randomize_mac_extended_id 0")
         self.__sendCommand("set_eui " + "{" + self.__convertHexToMAC(EUI) + "}")     
         return True      
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py setMAC(): " + str(e))
         return False

   def __convertHexToMAC(self,EUI):
      try:
         
         if isinstance(EUI,long):        
            self.MAC = EUI      
            MAC_HEX =hex(self.MAC)         
         else:
            if "0x" in str(EUI):
               self.MAC = int(EUI,16) 
            else:
               self.MAC = int(EUI)
                
            MAC_HEX = str(self.MAC)
               
         MAC_HEX = MAC_HEX.rstrip("L")
         MAC_HEX = MAC_HEX.lstrip("0x")       
         return str(MAC_HEX)                     
      except Exception,e:
          ModuleHelper.WriteIntoDebugLogger("SiLABS.py __convertHexToMAC(): " + str(e))
          return False
      
   def getLL64(self): 
      try:         
         LL64 = ""
         buff = self.__sendCommand("print_ip_addresses")      
         for address in buff:
            if "ll64" in address:           
               LL64 = (str(address).split())[1].rstrip("\n\r")
               break
         print "LL64 is - " + str(LL64)       
         return str(LL64)
      except UnboundLocalError,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py getLL64(): " + str(e))
         return False
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py getLL64(): " + str(e))
         return False

   def getML16(self):pass

   def getRloc(self):
      try:    
         RLOC = ""     
         buff = self.__sendCommand("print_ip_addresses")       
         for address in buff:
            if "rloc" in address:           
               RLOC = (str(address).split(" "))[2].rstrip("\n\r")
               break
         print "RLOC is - " + str(RLOC)        
         return ModuleHelper.GetFullIpv6Address(RLOC)#Trip Prefix and Use only last 16 bits
      except UnboundLocalError,e:
         return "Err:RLOC not set",e
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py getLL64(): " + str(e))
         return False

   def getULA64(self): 
      try:
         ULA64 =""
         buff = self.__sendCommand("print_ip_addresses")                  
         for address in buff:
            if "ml64" in address:           
               ULA64 = (str(address).split())[1].rstrip("\n\r")
               break
         print "ULA64 is - " + ULA64
         return str(ULA64)
      except UnboundLocalError,e:
         return "Err:ULA64 not set",e
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py getULA64(): " + str(e))
         return False

   def getGUA(self, filterByPrefix = None):
      try:         
         buff = self.__sendCommand("print_global_addresses")        
         GUA = []
         for idx, elem in enumerate(buff):
            if 'gua:'in elem:
               GUA.append(buff[idx].split('/')[0].split(' ')[2])
         #GUA = str(buff[6]).split('/')[0]
         if filterByPrefix == None:
            return str(GUA[0])
         else:
            for eachGUA in GUA:
               if eachGUA.startswith(filterByPrefix):
                  return eachGUA
            return str(GUA[0])

      except UnboundLocalError,e:
         return "ERR:: GUA Not Available",e
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py getGUA(): " + str(e))
         return False 
   
   def setLinkQuality(self,EUIadr,LinkQuality):  
      try:    
         if not "0x" in str(EUIadr):
            MACHex = hex(EUIadr)
         else:
            MACHex = str(EUIadr)

         MACHex = MACHex.rstrip('L')
         MACHex = MACHex.lstrip('0x')
         self.__sendCommand("set_lq {" + MACHex.zfill(16) + "} " + str(LinkQuality))
         return True
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py setLinkQuality(): " + str(e))
         return False 
   

   def setNetworkKey(self,nwkKey= "00112233445566778899AABBCCDDEEFF"):
      try:
         self.NwkKey = nwkKey
         self.__sendCommand("set_key {"+ nwkKey +"} 0") 
         return True
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py setNetworkKey(): " + str(e))
         return False 

   def addBlockedMAC(self,MAC): 
      try:                  
         try:
            MACHex = hex(MAC)#if Long value is passed
         except Exception,e:
            MACHex = str(MAC)#If Hex value passed

         MACHex = MACHex.rstrip('L')
         MACHex = MACHex.lstrip('0x')         
         self.__sendCommand("blacklist_eui {" + str(MACHex).zfill(16) + "}")    
         self.IsBlackListingEnabled = True
         self.BlackList.add(MACHex)
         return True    
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLabs_HW.py addBlockedMAC() :" + str(e))
         return False

   def addAllowMAC(self,MAC): 
      try:          

         try:
            MACHex = hex(MAC)#if Long value is passed
         except Exception,e:
            MACHex = str(MAC)#If Hex value passed

         MACHex = MACHex.rstrip('L')
         MACHex = MACHex.lstrip('0x')         
         self.__sendCommand("whitelist_eui {" + str(MACHex).zfill(16) + "}")     
         self.IsBlackListingEnabled = False
         self.BlackList.add(MACHex)
         return True   
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLabs_HW.py addAllowMAC() :" + str(e))
         return False

   def clearBlockList(self):
         self.__sendCommand("clear_filters")
         self.BlackList.clear()

   def clearAllowList(self): 
        self.__sendCommand("clear_filters")
        self.BlackList.clear()

   def joinNetwork(self,role):
      #self.setDefaultValues()
      self.DeviceRole = role
      try:       
                   
         if role == Test.Thread_Device_Role.Leader:           
            self.addIntoNetwork(role)
            self.setActiveTimestamp(self.ActiveTimestamp)            
         elif role == Test.Thread_Device_Role.Router:     
            #self.setActiveTimestamp(self.ActiveTimestamp)                               
            self.addIntoNetwork(role)          
            if ((not self.AutoDUTEnable) or (not ModuleHelper.AutoDUT_EnableRouterJitter)):
               self.__sendCommand("set_router_selection_jitter_ms 6000")#TO Avoid ROUTER_SELECTION_JITTER wait time          
               ModuleHelper.UpdateStatus('Adding SiLabs Router and waiting for 20 secs to upgrade')
            cmdOutput = self.__sendCommand("join_commissioned 3 2",20) , # 15 secs delay for router to upgarde
            #time.sleep(10)#Waiting for router to upgrade
            Joined = False     
            for outputline in cmdOutput[0]:
               if "joined attached" in outputline:    
                  Joined = True
                  break                                    
            if not Joined:
               ModuleHelper.WriteIntoDebugLogger("Router device might not have attached correctly") 

         elif role == Test.Thread_Device_Role.SED:
            self.addIntoNetwork(role)         
            cmdOutput = self.__sendCommand("join_commissioned 3 4",waitForOutput=5)       
            Joined = False     
            for outputline in cmdOutput:
               if "joined attached" in outputline:    
                  Joined = True
                  break
                                    
            if Joined:
               self.setPollingRate(self.SED_Polling_Rate)     
            else:
               time.sleep(5)#Try wating for longer before configuring poll rate
               self.setPollingRate(self.SED_Polling_Rate)     
               ModuleHelper.WriteIntoDebugLogger("SED device might not have attached correctly")       
         elif role == Test.Thread_Device_Role.REED:
            self.addIntoNetwork(role)                
            self.__sendCommand("join_commissioned 3 2")            
         elif role == Test.Thread_Device_Role.EndDevice:            
            self.addIntoNetwork(role)         
            self.__sendCommand("join_commissioned 3 5")
            time.sleep(5)          
         elif role == Test.Thread_Device_Role.EndDevice_FED:            
            self.addIntoNetwork(role)         
            self.__sendCommand("join_commissioned 3 3")
            time.sleep(5)   
         elif role == Test.Thread_Device_Role.EndDevice_MED:            
            self.addIntoNetwork(role)         
            self.__sendCommand("join_commissioned 3 5")
            time.sleep(5)   
                      
         self.setPSKc(self.PSKc)
         #Secrity policy
         securityPolicyHeader = "0c 03 "#type, length, 0bit, nbit, restricted         
         frame4Payload, length = self.__ConvertToByteArrya(self.SecurityPolicySecs)
         self.__sendCommand('set_active_dataset_bytes {'+ securityPolicyHeader + frame4Payload+' F8}')
                  
      except Exception,e:
         print "SiLABS_HW.py joinNetwork():" + str(e)

   def getDeviceState(self): 
      try:
         buff = self.__sendCommand("network_state")        
         if buff[2] == "joined":
            print "Device is" + state
            state = buff[2].strip().replace(',','')
            return state
         else:
            print "Device not joined"
      except UnboundLocalError,e:
         return "Err:Channel not set",e
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py getDeviceState(): " + str(e))

   def getNetworkFragmentID(self):
      try:
         buff = str(self.__sendCommand("status")).split("|")                
         NW_ID = buff[7].split("\\")[0].split(":")[1]      
         print "Network Fragment ID is" + NW_ID
         return NW_ID
      except UnboundLocalError,e:
         return "Err:Channel not set",e
      except Exception,e:
          ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py getNetworkFragmentID(): " + str(e))

   def getParentAddress(self):
       try:
           buffArray=[]
           buff = self.__sendCommand("status")
           print buff
           for item in buff:
               if "parent long id" in item:
                   buffArray = item.split("|")
                   for listInBuff in buffArray:
                       if "parent long id" in listInBuff:
                           parentAddressList= listInBuff.split(":")
                           BuffPID= parentAddressList[1]
                           BuffPID = BuffPID.lstrip(" ")
                           BuffPID =  BuffPID.rstrip (" ")
                           BuffPIDS= BuffPID.split(" ")
                           ParentAddress="0X"
                           for PID in reversed(BuffPIDS):
                               ParentAddress+= str(PID)
                           return int(ParentAddress,16)
       except  UnboundLocalError,e:
            return "Error in getting Parent address",e
       except Exception , e:
            ModuleHelper.WriteIntoDebugLogger("SiLabs__HW.py getParentAddress():"+ str(e))
       pass

   def powerDown(self):
       self.shortAddress = self.getShortAddress()
       self.__sendCommand("reboot")      
       time.sleep(8)
       self.setMAC(self.MAC)#Re-assign the mac 
       #self.__sendCommand("reset_network")
       self.__sendCommand("enable_app_callbacks 0")
       
   def reboot(self):
      self.__sendCommand("reboot")     
      time.sleep(8)
      self.__sendCommand("reset_network")    
      self.__sendCommand("enable_app_callbacks 0")
   
   def ping(self,destination,length=20): 
      try:         
         self.Sequence +=1
         id = "1"
         hop_limit = "64"
         pingCmd = "ping" + " \"" + str(destination) + "\"" + " " + id + " " + str(self.Sequence) + " " + str(length) + " " + hop_limit         
         self.__sendCommand(pingCmd)
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py ping(): " + str(e))

   def setPANID(self,PAN):
      if isinstance(PAN,int):
         self.PanId = "0x"+hex(PAN).lstrip('0x').zfill(4)
      else:
         self.PanId = PAN
      self.__sendCommand("set_pan_id" + " " + str(self.PanId))
      return PAN

   def setMLPrefix(self,sMeshLocalPrefix):
      try:
         prefix = ipaddress.ip_address(unicode(sMeshLocalPrefix)).exploded[:20]
         self.MLPrefix = prefix+":"
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger('silabs.py setMLPrefix() - MLprefx not valid'+str(e))      


   def networkKeySwitch(self): pass

   def incrementKey(self): pass   

   def clearKey(self): pass

   def reset(self):
      self.ActiveTimestamp = ModuleHelper.Default_ActiveTimestamp #This value is set here because, setActiveTimestamp()  is called before CreateTopology() in testcases
      self.__sendCommand("reboot")     
      time.sleep(8)
      self.__sendCommand("reset_network")  
      self.__sendCommand("enable_app_callbacks 0")
      self.__isComJoinModeSet = False  
      self.__logThreadRunning = False

   def setSleepyNodePollTime(self): pass

   def getNumberOfChilds(self): pass

   def getChildIDs(self): pass
   #print_childs

   def setRouterPrefix(self): pass  

   def configExternalRouter(self,P_Prefix, P_stable=0,R_Preference=0):
      try:         
         BitFlag = 0  

         if P_stable == 0:
            BitFlag = BitFlag | 0x01

         #R_Preference 0 is Medium preference
         #R_Preference 1 is high preference
         #R_Preference 3 is low preference
         if R_Preference == 0:
            BitFlag = BitFlag | 0x00
         elif R_Preference ==1:
            BitFlag = BitFlag | 0x40
         else:
            BitFlag = BitFlag | 0xC0


         brCommand = "external_route " + str(BitFlag) + " {" + str(P_Prefix).lstrip('0x') + "}"
         buff = self.__sendCommand(brCommand)        
         time.sleep(5) # allow network to update             
      except UnboundLocalError,e:
         return "Err:Channel not set",e
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py configExternalRouter(): " + str(e)) 

   def configBorderRouter(self,P_Prefix=2001000000000000, P_stable=1, P_default=1, P_slaac_preferred=0,P_Dhcp=0,P_preference = 0,P_on_mesh=1,P_nd_dns=0,P_preferred=1):
      try:         
         BitFlag = 0
         StableFlag = 0
         if P_preferred ==1:
            BitFlag = BitFlag | 0x20       

         if P_slaac_preferred == 1:
            BitFlag = BitFlag | 0x10            
         elif P_Dhcp == 1:
            BitFlag = BitFlag | 0x08

         if P_default == 1:
            BitFlag = BitFlag | 0x02

         if P_on_mesh == 1:
            BitFlag = BitFlag | 0x01

         if P_nd_dns == 1:
            pass
            #BitFlag = BitFlag | 0x01

         if P_stable == 1:
            StableFlag = 1        

         #R_Preference 0 is Medium preference
         #R_Preference 1 is high preference
         #R_Preference 3 is low preference
         if P_preference == 0:
            BitFlag = BitFlag | 0x00
         elif P_preference ==1:
            BitFlag = BitFlag | 0x40
         else:
            BitFlag = BitFlag | 0xC0


         brCommand = "gateway " + str(BitFlag) + " "+ str(StableFlag) + " {" + str(P_Prefix).lstrip('0x') + "} 10000 10000"
         buff = self.__sendCommand(brCommand)        
         time.sleep(5) # allow network to update
         if len(buff) > 3:
            GUA = buff[3].strip().replace(',','')           
      except UnboundLocalError,e:
         return "Err:Channel not set",e
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py configBorderRouter(): " + str(e)) 

   def downgradeToDevice(self): pass

   def removeRouter(self,ShortAdd):         
      self.__sendCommand('remove_router_by_short_id ' + str(ShortAdd))     
   
   def readBuffer(self):
      if self.DeviceType == 'ip':
         buffdata = self.deviceConnection.read_until('Command success',2)
         ##print buffdata
         buff = []
         buff = buffdata.split('\r\n')
         return(buff)
      else:
         buff = self.deviceConnection.readlines() 
         return(buff)
   
   def setDefaultValues(self):
      try:
         self.IsBlackListingEnabled = False
         self.BlackList = set()
         self.Channel = ModuleHelper.Default_Channel
         self.NwkName = ModuleHelper.Default_NwkName
         self.NwkKey = hex(ModuleHelper.Default_NwkKey).lstrip('0x').rstrip('L').zfill(32)
         self.PanId = "0x"+hex(ModuleHelper.Default_PanId).lstrip('0x').zfill(4)
         self.XpanId = hex(ModuleHelper.Default_XpanId).lstrip('0x').rstrip('L').zfill(16)

         #self.MLPrefix = ModuleHelper.Default_MLPrefix
         self.setMLPrefix(ModuleHelper.Default_MLPrefix)
         self.PSKc = ModuleHelper.Default_PSKc
         self.SecurityPolicySecs = ModuleHelper.Default_SecurityPolicy
         #self.ActiveTimestamp = ModuleHelper.Default_ActiveTimestamp
         self.SED_Polling_Rate = ModuleHelper.Default_Harness_SED_Polling_Rate

         self.KeySequenceCounter = 0
         self.setChannel(ModuleHelper.Default_Channel)
         self.Sig_strength = 3
         self.setPANID(self.PanId)
         self.channel_mask = 0
         self.__sendCommand('log_off 1 "rip" "mle" "security" "join" "drop" "topology" "lowpan"')
         self.__sendCommand('log 1 "drop" "coap" "topology" "mle" "commission"')  
           
         self.setMAC(self.MAC)
         self.__sendCommand('set_min_delay_timer 2000') #only for 1.1
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger('Silabs.py - Cannot execute setDefaultValues '+str(e))
         raise BaseException,"Check SiLabs device is connected or Check is it used by another instance"
         return None
      
   def multicast_Ping(self,destination="FF03::1",dataLength=20):
      try:
         sequence = "2"
         id = "1"
         hop_limit = "64"       
         pingCmd = "ping" + " \"" + str(destination) + "\"" + " " + sequence + " " + id + " " + str(dataLength) + " " + hop_limit    
         self.__sendCommand(pingCmd)         
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py Multicast_Ping(): " + str(e))

   def setPollingRate(self,pollingRateInSec):
      try:                 
         self.SED_Polling_Rate = pollingRateInSec 
         pollingRateInSec = pollingRateInSec * 1000 # Convert sec to millisec
         datapollCmd = "data_poll " + str(int(pollingRateInSec))         
         self.__sendCommand(datapollCmd)         
      except Exception, e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py SetPoolingRate(): " + str(e))

   def upgradeToRouter(self):
      self.__sendCommand('option force_dark 0')  
   
   def setCommisionerMode(self): pass
   
   def startCollapsedCommissioner(self):
      self.__sendCommand('log_off 1 "rip" "mle" "security" "join" "drop" "topology" "lowpan" "coap"')  
      #self.__sendCommand('log 1 "rip" "mle" "security" "join" "drop" "topology" "lowpan" "coap"')       
      self.__sendCommand('log 1 "commission" "security" "coap" "drop"')        
      self.setNetworkKey(self.NwkKey)
      #'form_pan 14 3 2 0xFACE "GRL" "FD00:0DB8::" {000DB80000000000}'      
      self.__sendCommand('form_pan '+str(self.Channel)+' 3 2 '+str(self.PanId)+' "'+str(self.NwkName)+'" "FD00:0DB8::" {'+str(self.XpanId)+'}')
      self.__sendCommand('com_petition')
     


   def startNativeCommissioner(self,strPSKc= 'GRLpassword'):            
      self.addIntoNetwork(Thread_Device_Role.Router)      
      self.__sendCommand('use_mle_discovery 1')         
      self.__sendCommand("join_commissioned 3 7")
      time.sleep(5)
      self.__sendCommand('native_petition')  
      #self.__sendCommand('com_steering"')
      #self.setJoinKey(strPSKd)

   def startPetitioning(self): pass

   def startSteering(self): pass

   def setJoinKey(self,key="GRLpassWord"):
      self.__sendCommand('set_join_key "' + key )   

   def permitJoin(self): pass

   def joinCommissioned(self, strPSKd='GRLpassWordx',waitTime=20):  
      self.__sendCommand('log_off 1 "rip" "mle" "security" "join" "drop" "topology" "lowpan" "coap"')  
      #self.__sendCommand('log 1 "rip" "mle" "security" "join" "drop" "topology" "lowpan" "coap"')      
      self.__sendCommand('log 1 "commission" "security" "coap" "drop"')        

      channel_mask = 0      
      #self.__sendCommand("set_eui " + "{" + self.__convertHexToMAC(self.MAC) + "}")
      #join 13 3 2 "GRL" {000DB80000000000} 0xFACE "GRLpassWord"
      #self.__sendCommand('join ' + str(self.Channel) + ' 3 2 "' + self.NW_name + '" {000DB80000000000} 0xFACE "' + strPSKd +'"') 
      self.__sendCommand('use_mle_discovery 1')
      self.__sendCommand('join ' + str(self.Channel) + ' 3 2 "' + self.NwkName + '" {'+self.XpanId+'} 0xFFFF "' + strPSKd +'"')     
      if self.__logThreadRunning == False:      
         self.__logThread = ThreadRunner.run(target = self.__readCommissioningLogs, args = (120,))      
         self.__logThreadRunning = True
      time.sleep(waitTime)

   def rejectSolicit(self,state=1):
      self.__sendCommand('option reject_coap_solicit ' + str(state))      

   def allowCommission(self):  
      self.__sendCommand('allow_commissioner 1') 
      self.__sendCommand('com_petition')
           



   __logThreadRunning = False   
   def scanJoiner(self, xEUI, strPSKd):
       if xEUI == '*':
          if not self.__isComJoinModeSet:
            self.__sendCommand('com_join_mode 1 1')
            self.__isComJoinModeSet = True
          self.__sendCommand('set_join_key "' + strPSKd + '"')
          self.__sendCommand('com_steering')
          self.__sendCommand('')
       else:
          if isinstance(xEUI,long):                 
             MAC_HEX =hex(xEUI)         
          else:
             if "0x" in str(xEUI):
                self.MAC = int(xEUI,16) 
             else:
                self.MAC = int(xEUI)                 
             MAC_HEX = str(xEUI)
          MAC_HEX = MAC_HEX.rstrip("L")
          MAC_HEX = MAC_HEX.lstrip("0x")
          MAC_HEX = MAC_HEX.rjust(16,'0')
          if not self.__isComJoinModeSet:
            self.__sendCommand('com_join_mode 2 16')
            self.__isComJoinModeSet = True
          self.__sendCommand('set_join_key "' + strPSKd +'"'+ " {"+MAC_HEX+"}")
          self.__sendCommand("com_add_steering {"+MAC_HEX+"}")
          self.__sendCommand('com_steering')
          self.__sendCommand('')
       if self.__logThreadRunning == False:
            self.__logThread = ThreadRunner.run(target = self.__readCommissioningLogs, args = (120,))              
            self.__logThreadRunning = True

       
       
         
      

   def setProvisioningUrl(self, strURL='grl.com'):
      self.__sendCommand('set_prov_url "'+strURL+'"')

   def configureJoinerFinalization(self):pass

   def disableJoinerFinalization(self):pass

   def getJoinKey(self):pass

   def removeRouterPrefix(self,prefixEntry = 2001000000000000):
      removePrefixCmd = "remove_prefix {"+ str(prefixEntry) +"}"
      self.__sendCommand(removePrefixCmd)
      time.sleep(2)

   def resetAndRejoin(self,timeout):
      self.__sendCommand("reboot")
      time.sleep(timeout+5)
      self.__sendCommand("enable_app_callbacks 0")
      self.setMAC(self.MAC)      
      if self.DeviceRole == Test.Thread_Device_Role.Router or self.DeviceRole == Test.Thread_Device_Role.Leader:            
         self.__sendCommand("set_router_selection_jitter_ms 6000")#TO Avoid ROUTER_SELECTION_JITTER wait time

      for eachEntry in self.BlackList:
         if self.IsBlackListingEnabled == True:         
            self.addBlockedMAC(eachEntry)
         else:
            self.addAllowMAC(eachEntry)

      time.sleep(1)
      self.__sendCommand("resume_network")
      time.sleep(10)
      if self.DeviceRole == Test.Thread_Device_Role.SED:
         self.setPollingRate(self.SED_Polling_Rate)   
   
   def powerUp(self):
      self.setMAC(self.MAC)      
      if self.DeviceRole == Test.Thread_Device_Role.Router or self.DeviceRole == Test.Thread_Device_Role.Leader:            
         self.__sendCommand("set_router_selection_jitter_ms 6000")#TO Avoid ROUTER_SELECTION_JITTER wait time
      time.sleep(1)
      self.__sendCommand("resume_network")
      time.sleep(10)

   def setNetworkIDTimeout(self,iNwkIDTimeOut):
      self.__sendCommand("set_fragment_timeout " + str(iNwkIDTimeOut))

   def setKeepAliveTimeOut(self,iTimeOut):
      self.__sendCommand("set_keepalive_interval " + str(iTimeOut))
      self.__sendCommand("force_child_timeout " + str(iTimeOut * 4))    
      
   def getShortAddress(self):
      try:    
         ShortAddress = ""     
         buff = self.__sendCommand("print_ip_addresses")       
         for address in buff:
            if "rloc" in address:           
               ShortAddress = (str(address).split(":"))[8].rstrip("\n\r")
               break
         if (ShortAddress == ''):
             print "Short Address is - 0x" + str(self.shortAddress)
             return self.shortAddress
         print "Short Address is - 0x" + str(ShortAddress)        
         return ModuleHelper.ParseToInt("0x" + ShortAddress)#Trip Prefix and Use only last 16 bits
      except UnboundLocalError,e:
         return "Err:RLOC not set",e
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py getLL64(): " + str(e))    

   def setKeySequenceCounter(self,keySequenceValue):
      self.KeySequenceCounter = keySequenceValue
      self.__sendCommand("set_sequence " + str(keySequenceValue))
   
   def incrementKeySequenceCounter(self,incrementValue = 1):
      for i in range(0,incrementValue):
         self.__sendCommand("increment_sequence")
         time.sleep(0.5)

   def setNetworkDataRequirement(self,dataRequirement):
      if dataRequirement == Device_Data_Requirement.ALL_DATA:
         self.__sendCommand("need_all_network_data 1")
      else:
         self.__sendCommand("need_all_network_data 0")

   def requestRouterID(self):
      self.__sendCommand("request_router_id 1")

   def __platformVerification(self):
      pass

   ## getVersionNumber: Get version number of firmware the device running
   # @return str
   def getVersionNumber(self):
      try:
         version = self.__sendCommand('version')
         #print version[0].strip()
         return version[0].strip("ip>")
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger('SiLabs.py getVersionNumber --> Could be wrong device or version number '+str(e))
         return "Unknown Version"

   def __readCommissioningLogs(self, durationInSeconds):
      logs = Queue()
      if self.DeviceType != 'ip': 
         self.deviceConnection.flushInput()
         self.deviceConnection.flushOutput()
      t_end = time.time() + durationInSeconds
      print "Reading console logs",
      while time.time() < t_end:        
         try:      
            print ".",
            #while True:
            #   bytesToRead = self.deviceConnection.inWaiting()
            #   ser.read(bytesToRead)
            lines = self.deviceConnection.readline()
            if len(lines) > 0:
               print lines
               logs.put(lines) 
            time.sleep(0.3)

         except Exception,e:
            print e
            return logs

      return logs


   def getCommissioningLogs(self):
      ProcessedLogs = []
      print self.__logThread
      rawLogsQ = self.__logThread.get()
      completelogs = []
      while not rawLogsQ.empty():
           
         rawLogEach = rawLogsQ.get()# Gives all the lines in the logs a a big bunch is here
         completelogs.append(rawLogEach.strip())
            
      for packet in completelogs: # CSV
             
         stringFilterList = ['uri: c/','uri: a/']
            
         if any(string_ in str(packet) for string_ in stringFilterList):
         #if 'uri: ' in packet: #[Stack CoAP
            ##########
            print "\n\r ----------------------------"        
            print packet
            ################ 
            EncryptedPacket = PlatformDiagnosticPacket()   
            PacketType = "" 
            URI = "" 
            payLoadValue = ""
            for packetField in packet.split('|'):                  
               TypeValuePair = packetField.split(':')
               logType = ""
               logValue = ""
               if len(TypeValuePair) == 2:
                  logType = TypeValuePair[0].strip()    
                  logValue = str(TypeValuePair[1].strip())
               else:
                  logValue = str(TypeValuePair)               
               #if 'type' in logType:
               if 'CoAP' in packetField:
                  EncryptedPacket.Direction = PlatformDiagnosticPacket_Direction.IN if 'RX' in packetField \
                     else PlatformDiagnosticPacket_Direction.OUT if 'TX' in packetField \
                     else PlatformDiagnosticPacket_Direction.UNKNOWN
               elif ('CON/POST' in logValue) or ('type' in logType):
                  PacketType = 'Response' if 'ACK' in logValue \
                     else 'Request' if 'CON' in logValue \
                     else 'NA'
               elif 'from' in logType or logType.strip() == 'to':
                  #EncryptedPacket.EUI = self.__MactoHex(logValue)
                  if len(logValue) >16:
                     ipv6AddressFromLog = logValue.replace(' ','')
                     MACAddressFromipv6 = int('0x'+ipv6AddressFromLog[16:],16) | 0x0200000000000000
                     EncryptedPacket.EUI = MACAddressFromipv6
                  else:
                     EncryptedPacket.EUI = logValue
               elif 'IID' in logType:
                  EncryptedPacket.IID = ""
               elif 'uri' in logType:
                  URI = logValue                  
               elif 'length' in logType:
                  EncryptedPacket.TLVsLength = int(logValue)
               elif 'payload' in logType:
                  if logValue != '<null>':
                     payLoadValue = logValue                     
               else:
                  print logType,logValue
            if URI != "" and PacketType != "":
               EncryptedPacket.Type = PlatformDiagnosticPacket_Type.JOIN_FIN_req if (('c/jf' in URI) & (PacketType == 'Request')) \
                        else PlatformDiagnosticPacket_Type.JOIN_FIN_rsp if (('c/jf' in URI) & (PacketType == 'Response')) \
                        else PlatformDiagnosticPacket_Type.JOIN_ENT_req if (('c/je' in URI) & (PacketType == 'Request')) \
                        else PlatformDiagnosticPacket_Type.JOIN_ENT_rsp if (('c/je' in URI) & (PacketType == 'Response')) \
                        else PlatformDiagnosticPacket_Type.UNKNOWN
            if payLoadValue != '' and EncryptedPacket.Type != PlatformDiagnosticPacket_Type.UNKNOWN:
               payloadRaw = logValue.replace(']','').split(' ')
               payload = [int(eachOctat,16) for eachOctat in payloadRaw]
               EncryptedPacket.TLVs = PlatformPackets.read(EncryptedPacket.Type,payload) if payload != [] else []
            ProcessedLogs.append(EncryptedPacket)        
      return ProcessedLogs
      time.sleep(4)

   def forceSetSlaac(self,slaacAddress):
      self.__sendCommand('force_slaac_address "'+ str(slaacAddress) +'"')

   def setPSKc(self,strPSKc):
      value,length = self.__ConvertToByteArrya(strPSKc)
      self.__sendCommand('set_active_dataset_bytes {04 10 '+self.__stretchPskc(value)+'}')

   def __stretchPskc(self,nonStretchedPskcBytes): #JIRA DEV-402      
      rawStrectchedPskc = self.__sendCommand('stretch_pskc "'+nonStretchedPskcBytes+'"',5)
      for eachLine in rawStrectchedPskc:
         if 'Stretched PSKC TLV:' in eachLine:
            PSKc = (eachLine.split(':')[1]).strip()
            return PSKc
      ModuleHelper.WriteIntoDebugLogger('silabs.py __stretchPskc(): - Could not calculate stretched PSKC')
      return nonStretchedPskcBytes
            

   def setActiveTimestamp(self,activeTimestamp):
      self.ActiveTimestamp = activeTimestamp
      actTimeStampSecs = self.__ConvertToByteArrya(activeTimestamp,12)[0]
      actTimeStampTicksAndUBit = " 00 00"
      actTimeStamp = " 08 "+actTimeStampSecs+actTimeStampTicksAndUBit # lenght + ..
      self.__sendCommand('set_active_dataset_bytes {0E'+actTimeStamp+'}')

   def getChildrenInfo(self,xFilterByEui=None):pass


   def diagnosticGet(self,strDestinationAddr, listTLV_ids = []):
      #get_coap_diagnostics <string> <string>
      self.__sendCommand('log 1 "coap"')
      tlvs = " ".join(hex(tlv).lstrip("0x").zfill(2) for tlv in listTLV_ids)
      self.__sendCommand('get_coap_diagnostics "'+strDestinationAddr+'" {'+tlvs+'}') 
      self.__sendCommand('log 0 "coap"')

   def diagnosticQuery(self,strDestinationAddr, listTLV_ids = []):
      #get_coap_diagnostics <string> <string>
      self.__sendCommand('log 1 "coap"')
      tlvs = " ".join(hex(tlv).lstrip("0x").zfill(2) for tlv in listTLV_ids)
      self.__sendCommand('query_coap_diagnostics "'+strDestinationAddr+'" {'+tlvs+'}') 
      self.__sendCommand('log 0 "coap"')

   def diagnosticReset(self,strDestinationAddr,listTLV_ids = []):
      #reset_coap_diagnostics <string> <string>
      self.__sendCommand('log 1 "coap"')
      tlvs = " ".join(hex(tlv).lstrip("0x").zfill(2) for tlv in listTLV_ids)
      self.__sendCommand('reset_coap_diagnostics "'+strDestinationAddr+'" {'+tlvs+'}') 
      self.__sendCommand('log 0 "coap"')



   def setPartationId(self,partitionId):
      try:
       strPartID = str(hex(partitionId).lstrip("0x").rstrip("L")) + "40" #the last byte is the weighting byte set to 0x40
       strPartID = strPartID.zfill(10)
       self.__sendCommand("set_island_id {"+ strPartID +"}")
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger('Silabs.py - Cannot execute setPartitionID '+str(e))
         return None
       
   def MGMT_COMM_GET(self,Addr="ff02::1",TLVs = []):
      tlvs = " ".join(hex(tlv).lstrip("0x").zfill(2) for tlv in TLVs)
      self.__sendCommand("com_get {"+tlvs+"}") #"com_get \""+Addr+"\" {"+tlvs+"}" #cannot set destination address, command throws "Bad IP argument" error
   
   def MGMT_COMM_SET(self,Addr="ff02::1",xCommissionerSessionID=None,xSteeringData=None,xBorderRouterLocator=None,xChannelTlv=None,ExceedMaxPayload=False):
      payload = ""
      if xCommissionerSessionID != None:
         payload += " "+(hex(MgmtTLVs.CommissionerSessionId).lstrip("0x").zfill(2))#Add the TLV Type
         value,length = self.__ConvertToByteArrya(xCommissionerSessionID,4)#get TLV Length and Value
         payload += " " + length + " " + value  
      else:
         currentSessionIdRaw = self.__sendCommand('print_Session_id') 
         for eachLine in currentSessionIdRaw:
            if "session ID:" in eachLine:
               currentSessionIdRaw = eachLine.strip()[-6:] #read last 5 char has the 2byte session id
               value,length = self.__ConvertToByteArrya(int(currentSessionIdRaw.strip(),16),4)
               payload += " "+(hex(MgmtTLVs.CommissionerSessionId).lstrip("0x").zfill(2))+" 02 "+value
               break      
      if xSteeringData != None:
         payload += " " + (hex(MgmtTLVs.SteeringData).lstrip("0x").zfill(2))
         value,length = self.__ConvertToByteArrya(xSteeringData)#get TLV Length and Value
         payload += " " + length + " " + value
      if xBorderRouterLocator != None:
         payload += " "+(hex(MgmtTLVs.BorderAgentLocator).lstrip("0x").zfill(2))    
         value,length = self.__ConvertToByteArrya(xBorderRouterLocator,4)
         payload += " " + length + " " + value
      if xChannelTlv != None:
         payload += " "+(hex(MgmtTLVs.Channel).lstrip("0x").zfill(2))
         payload += " 03 00 00 "+hex(xChannelTlv).lstrip('0x').zfill(2) #length + channel page + channel(16 bit)
      if ExceedMaxPayload:
         Bigpayload = ""
         for i in range(0,255):
            Bigpayload += " 00"
         payload += " 08 FE"+Bigpayload
      self.__sendCommand("com_set {"+payload.strip()+"}")#"com_set \""+Addr+"\" {"+payload.strip()+"}" #cannot set destination address, command throws "Bad IP argument" error


   def MGMT_ACTIVE_GET(self,Addr="",TLVs = []):
      self.__sendCommand('log 1 "coap"')
      tlvs = " ".join(hex(tlv).lstrip("0x").zfill(2) for tlv in TLVs)
      self.__sendCommand('get_active_dataset "'+Addr+'" {'+tlvs+'}') 
   
   def MGMT_PANID_QUERY(self,sAddr,xCommissionerSessionId, listChannelMask, xPanId):
      channelMask = self.__setChannelMask(listChannelMask, isInverted = False)
      self.__sendCommand("send_pan_id_scan_request \""+sAddr+"\" "+str(hex(channelMask))+" "+str(hex(xPanId)))

   def sendBeacons(self,sAddr,xCommissionerSessionId, listChannelMask, xPanId):
      channelMask = self.__setChannelMask(listChannelMask, isInverted = False)
      self.__sendCommand("send_pan_id_scan_request \""+sAddr+"\" "+str(hex(channelMask))+" "+str(hex(xPanId)))

   def MGMT_ACTIVE_SET(self,sAddr='',xCommissioningSessionId=None,listActiveTimestamp=None,listChannelMask=None,xExtendedPanId=None,sNetworkName=None,sPSKc=None,listSecurityPolicy=None,xChannel=None,
                       sMeshLocalPrefix=None,xMasterKey=None,xPanId=None,xTmfPort=None,xSteeringData=None,BogusTLV=None,xDelayTimer=None):      

      payload = ""
      if listActiveTimestamp != None:
         payload += (hex(MgmtTLVs.ActiveTimestamp).lstrip("0x").zfill(2))
         actTimeStampSecs = self.__ConvertToByteArrya(listActiveTimestamp[0],12)[0]
         actTimeStampTicksAndUBit = " "+self.__ConvertToByteArrya(listActiveTimestamp[1],3)[0]+"0" #Pl dont ask me what is "0", spec is not clear for "U" bit when i wrote this code.
         actTimeStamp = " 08 "+actTimeStampSecs+actTimeStampTicksAndUBit # lenght + ..
         payload += actTimeStamp
      if xCommissioningSessionId != None:
         payload += " "+(hex(MgmtTLVs.CommissionerSessionId).lstrip("0x").zfill(2))#Add the TLV Type
         value,length = self.__ConvertToByteArrya(xCommissioningSessionId,4)#get TLV Length and Value
         payload += " " + length + " " + value  
      else:
         currentSessionIdRaw = self.__sendCommand('print_Session_id') 
         for eachLine in currentSessionIdRaw:
            if "session ID:" in eachLine:
               currentSessionIdRaw = eachLine.strip()[-6:] #read last 5 char has the 2byte session id
               value,length = self.__ConvertToByteArrya(int(currentSessionIdRaw.strip(),16),4)
               payload += " "+(hex(MgmtTLVs.CommissionerSessionId).lstrip("0x").zfill(2))+" 02 "+value
               break
      if listChannelMask != None:
         payload += " " + (hex(MgmtTLVs.ChannelMask).lstrip("0x").zfill(2))
         value,length = self.__ConvertToByteArrya(self.__setChannelMask(listChannelMask),8)
         payload += " 06 00 04 "+ value#Refer 8.10.1.18
      if xExtendedPanId != None:
         payload += " " + (hex(MgmtTLVs.XpanId).lstrip("0x").zfill(2))
         value,length = self.__ConvertToByteArrya(xExtendedPanId,16)#get TLV Length and Value
         payload += " " + length + " " + value
      if sNetworkName != None:
         payload += " " + (hex(MgmtTLVs.NetworkName).lstrip("0x").zfill(2))
         value,length = self.__ConvertToByteArrya(sNetworkName)#get TLV Length and Value
         payload += " " + length + " " + value
      if sPSKc != None:
         stretchedPskc = Thread_PBKDF2.get(sPSKc,ModuleHelper.Default_XpanId,ModuleHelper.Default_NwkName)
         value,length = self.__ConvertToByteArrya(stretchedPskc,32)#get TLV Length and Value         
         payload += " 04" + length + " " + value
      if listSecurityPolicy != None:
         payload += " " + (hex(MgmtTLVs.SecurityPolicy).lstrip("0x").zfill(2))
         payload += " 03" #Length

         frame4 = 0x0
         if isinstance(listSecurityPolicy,list) and len(listSecurityPolicy)>=1:#Rotation Time
            frame4 = listSecurityPolicy[0]
         frame4Payload, length = self.__ConvertToByteArrya(frame4)
         payload += " " + frame4Payload

         frame3 = 0b00000000
         if isinstance(listSecurityPolicy,list) and len(listSecurityPolicy)>1: #Seuruty Bits
            frame3 = listSecurityPolicy[1]
         payload += " "+hex(frame3).lstrip('0x')         
      if xChannel != None:
         payload += " "+(hex(MgmtTLVs.Channel).lstrip("0x").zfill(2))
         payload += " 03 00 00 "+hex(xChannel).lstrip('0x').zfill(2) #length + channel page + channel(16 bit)
      if sMeshLocalPrefix != None:
         payload += " "+(hex(MgmtTLVs.MeshLocalUla).lstrip("0x").zfill(2))
         meshPrefixParse1 = "0x"+sMeshLocalPrefix.replace(":","").ljust(16,'0') #2(0x)+16
         meshPrefixParse2,length = self.__ConvertToByteArrya(meshPrefixParse1,16)
         payload += " 08 "+meshPrefixParse2         
      if xMasterKey!= None:
         payload += " "+(hex(MgmtTLVs.MasterKey).lstrip("0x").zfill(2))    
         mkey,length = self.__ConvertToByteArrya(xMasterKey,32)
         payload += " 10 "+mkey
      if xPanId != None:
         payload += " "+(hex(MgmtTLVs.PanId).lstrip("0x").zfill(2))    
         value,length = self.__ConvertToByteArrya(xPanId,4)
         payload += " " + length + " " + value
      if BogusTLV != None:#Bogus TLV
         payload += " 130 2 aa 55"
      if xSteeringData != None:
         payload += " " + (hex(MgmtTLVs.SteeringData).lstrip("0x").zfill(2))
         value,length = self.__ConvertToByteArrya(xSteeringData)#get TLV Length and Value
         payload += " " + length + " " + value
      print payload     
      self.__sendCommand('set_active_dataset {'+payload.strip(" ")+'} 0xFC00')#Default to leader anicast address
      

   def MGMT_ANNOUNCE_BEGIN(self,sAddr,xCommissionerSessionId,listChannelMask, xCount, xPeriod):
      #send_announce_begin <destination> <channel page> <channel (not mask)> <count> <period>
      self.__sendCommand('send_announce_begin "'+sAddr+ '" 0 ' +str(listChannelMask[0])+ ' '+str(xCount)+' '+ str(xPeriod))
      


   def MGMT_ED_SCAN(self,sAddr, xCommissionerSessionId,listChannelMask, xCount, xPeriod, xScanDuration):           
      channelMask = self.__setChannelMask(listChannelMask,isInverted= False)
      self.__sendCommand("send_energy_scan_request \""+sAddr+"\" "+str(hex(channelMask))+" "+str(xCount)+" "+str(xPeriod)+" "+str(xScanDuration))


   def MGMT_PENDING_GET(self,Addr='', TLVs=[]): 
      tlvs = " ".join(hex(tlv).lstrip("0x").zfill(2) for tlv in TLVs)
      self.__sendCommand("get_pending_dataset" + " \"" + Addr + "\"{" + tlvs +"}")   
      #get_pending_dataset "FD00:0DB8:0000:0000:0000:00FF:FE00:0000" {00 35 01}
      #First argument: destination
      #Second argument: List of TLVs to get

   def MGMT_PENDING_SET(self,sAddr='', xCommissionerSessionId=None, listPendingTimestamp=None, listActiveTimestamp=None, xDelayTimer=None, xChannel=None,xPanId=None,xMasterKey= None,sMeshLocalPrefix=None, sNetworkName = None):            
      payload = ""
      if listActiveTimestamp != None:
         payload += (hex(MgmtTLVs.ActiveTimestamp).lstrip("0x").zfill(2))
         actTimeStampSecs = self.__ConvertToByteArrya(listActiveTimestamp[0],12)[0]
         actTimeStampTicksAndUBit = " "+self.__ConvertToByteArrya(listActiveTimestamp[1],3)[0]+"0" #Pl dont ask me what is "0", spec is not clear for "U" bit when i wrote this code.
         actTimeStamp = " 08 "+actTimeStampSecs+actTimeStampTicksAndUBit # lenght + ..
         payload += actTimeStamp
      if listPendingTimestamp != None:
         payload += " "+(hex(MgmtTLVs.PendingTimestamp).lstrip("0x").zfill(2))
         pendTimeStampSecs = self.__ConvertToByteArrya(listPendingTimestamp[0],12)[0]
         pendTimeStampTicksAndUBit = " "+self.__ConvertToByteArrya(listPendingTimestamp[1],3)[0]+"0" #Pl dont ask me what is "0", spec is not clear for "U" bit when i wrote this code.
         pendTimeStamp = " 08 "+pendTimeStampSecs+pendTimeStampTicksAndUBit # lenght + ..
         payload += pendTimeStamp
      if xCommissionerSessionId != None:
         payload += " "+(hex(MgmtTLVs.CommissionerSessionId).lstrip("0x").zfill(2))#Add the TLV Type
         value,length = self.__ConvertToByteArrya(xCommissionerSessionId,4)#get TLV Length and Value
         payload += " " + length + " " + value  
      else:
         currentSessionIdRaw = self.__sendCommand('print_Session_id') 
         for eachLine in currentSessionIdRaw:
            if "session ID:" in eachLine:
               currentSessionIdRaw = eachLine.strip()[-6:] #read last 5 char has the 2byte session id
               value,length = self.__ConvertToByteArrya(int(currentSessionIdRaw.strip(),16),4)
               payload += " "+(hex(MgmtTLVs.CommissionerSessionId).lstrip("0x").zfill(2))+" 02 "+value
               break
      if xChannel != None:
         payload += " "+(hex(MgmtTLVs.Channel).lstrip("0x").zfill(2))
         payload += " 03 00 00 "+hex(xChannel).lstrip('0x').zfill(2) #length + channel page + channel(16 bit)    
      if xMasterKey!= None:
         payload += " "+(hex(MgmtTLVs.MasterKey).lstrip("0x").zfill(2))    
         mkey,length = self.__ConvertToByteArrya(xMasterKey,32)
         payload += " 10 "+mkey
      if xPanId != None:
         payload += " "+(hex(MgmtTLVs.PanId).lstrip("0x").zfill(2))    
         value,length = self.__ConvertToByteArrya(xPanId,4)
         payload += " " + length + " " + value
      if sNetworkName != None:
         payload += " " + (hex(MgmtTLVs.NetworkName).lstrip("0x").zfill(2))
         value,length = self.__ConvertToByteArrya(sNetworkName)#get TLV Length and Value
         payload += " " + length + " " + value
      if xDelayTimer != None:
         payload += " "+(hex(MgmtTLVs.DelayTimer).lstrip("0x").zfill(2))    
         value,length = self.__ConvertToByteArrya(xDelayTimer,8)
         payload += " " + length + " " + value
      if sMeshLocalPrefix != None:
         payload += " "+(hex(MgmtTLVs.MeshLocalUla).lstrip("0x").zfill(2))
         meshPrefixParse1 = "0x"+sMeshLocalPrefix.replace(":","").ljust(16,'0') #2(0x)+16
         meshPrefixParse2,length = self.__ConvertToByteArrya(meshPrefixParse1,16)
         payload += " 08 "+meshPrefixParse2
      print payload
      self.__sendCommand('set_pending_dataset {'+payload.strip(" ")+'} 0xFC00')#Default to leader anycast address
      

   def __ConvertToByteArrya(self,value,Length = None):
    HexValue = ModuleHelper.ParseToInt(str(value))
    if HexValue >= 0:#if value is a number
       HexString = hex(HexValue).lstrip("0x").rstrip('L')
       if Length != None:
          HexString = HexString.zfill(Length)
          ByteString = " ".join(HexString[index:index+2] for index in range(0,len(HexString),2))#No zfill of 2, some time we need odd length (ex: see active timestmp last 16 bits)
       else:          
          if len(HexString) % 2 != 0:
             HexString = HexString.rjust((len(HexString)+1),'0')
          ByteString = " ".join(HexString[index:index+2].rjust(2,'0') for index in range(0,len(HexString),2))        
       length = hex(len(HexString)/2).lstrip("0x").zfill(2)
    else:#if Value is a string
       if Length != None:
         ByteString =  " ".join(format(ord(character), "x") for character in value.ljust(Length))
       else:
         ByteString =  " ".join(format(ord(character), "x") for character in value)

       length = hex(len(ByteString.replace(" ",''))/2).lstrip("0x").zfill(2)
    return ByteString,length


   def getNeighbouringDevices(self):
      NeighbourMAC = []      
      command_Log = self.__sendCommand("print_child")#Collect Child MACs [EDs and SED]
      for line in command_Log:
         if len(str(line).split(' ')) > 2:
            Mac = str(line).split(' ')[2]#Second Entry in the child table is its MAC
            temp = ModuleHelper.ParseToInt(Mac)
            if temp != -1:
               NeighbourMAC.append(temp)

      command_Log = self.__sendCommand("print_rip")#Collect Parent and Router MACs
      for line in command_Log:
         if "longId: " in line:
            Mac = str(line).split('longId: ')[1]#First Entry in the child table is its MAC
            temp = ModuleHelper.ParseToInt("0x" + Mac.rstrip("\r\n"))
            if temp != -1:
               NeighbourMAC.append(temp)

      return NeighbourMAC
       
   def __setChannelMask(self,channelsArray, isInverted = True):
      maskSet = 0
      for eachChannel in channelsArray:
         mask = 1 << eachChannel
         maskSet = (maskSet | mask)
      if isInverted:
         maskInverted =  int('{:032b}'.format(maskSet)[::-1], 2)
         finalChannelMask = maskInverted
      else:
         finalChannelMask = maskSet
      return finalChannelMask

   def setActiveDataset(self,listActiveDataset=[]):pass
   #set_min_delay_timer (uint32_t)

   def setUdpJoinerPort(self,portNumber):
      self.__sendCommand("jpake_port "+ str(portNumber))
   

   def commissionerUnregister(self):
      self.__sendCommand("com_remove")

   def getChildTimeoutValue(self):
       try:
            timeout = self.__sendCommand("get_child_timeout")
            for output in timeout:
               if " " in output:
                  for StringValues in output.split(" "):
                     timeoutValue =  ModuleHelper.ParseToInt(StringValues)
                     if timeoutValue > 0:
                        return timeoutValue         
       except Exception,e:
           ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py getChildTimeoutValue(): " + str(e))
           return 240
       finally :
           ModuleHelper.WriteIntoDebugLogger("Error while finding the Child ID timeout from the device using default value")
           return 240

   def enableAutoDUTObjectFlag(self):
       self.AutoDUTEnable = True 

   def updateRouterStatus(self):
      self.__sendCommand("send_address_solicit 3")

   def setRouterThresholdValues(self,upgradeThreshold,downgradeThreshold):
      self.__sendCommand('set_router_selection_parameters '+ str(upgradeThreshold) +' '+ str(downgradeThreshold) +' 64')      

   def ValidateDeviceFirmware(self):
      if "SL-Thread" in self.getVersionNumber():
         return True
      else:
         return False

   def setOutBoundLinkQuality(self,LinkQuality):
      try:
         self.__sendCommand("set_lq {FFFFFFFFFFFFFFFF} " + str(LinkQuality))
         return True
      except Exception,e:
         ModuleHelper.WriteIntoDebugLogger("SiLABS_HW.py setLinkQuality(): " + str(e))
         return False 


   #def setMinDelayTimer(self,iSeconds): 
   #   self.__sendCommand('set_min_delay_timer '+str(iSeconds))