#ISD afv6
# Simplicity Studio version: 4.27.0.qualifier

# Application configuration
appId: thread
frameworkRoot: protocol/thread/app/thread
architecture: unix
deviceName: thread-test-host
generationDirectory: PATH(ISC_RELATIVE):.

# Devices
device: Unix
device: Host
device: Router

# Plugin configuration
appPlugin: accelerometer-led=false
appPlugin: adc=false
appPlugin: address-configuration-debug=false
appPlugin: antenna=false
appPlugin: antenna-stub=true
appPlugin: ash-v3=false
appPlugin: ash-v3-stub=false
appPlugin: basic-server=false
appPlugin: battery-monitor=false
appPlugin: bootload-cli=false
appPlugin: bulb-pwm-driver=false
appPlugin: bulb-ui=false
appPlugin: button=false
appPlugin: button-interface=false
appPlugin: button-press=false
appPlugin: button-stub=false
appPlugin: buzzer=false
appPlugin: buzzer-stub=false
appPlugin: cjson=false
appPlugin: cli=true
appPlugin: coap-cli=false
appPlugin: coap-debug=false
appPlugin: coap-dispatch=false
appPlugin: coexistence=false
appPlugin: color-control-server=false
appPlugin: command-interpreter2=true
appPlugin: connection-manager-cli=false
appPlugin: connection-manager-jib=false
appPlugin: connection-manager-joob=false
appPlugin: debug-channel=false
appPlugin: debug-channel-stub=false
appPlugin: debug-print=true
appPlugin: dhcp-client=false
appPlugin: dhcp-library=false
appPlugin: dhcp-stub-library=false
appPlugin: diagnostic=false
appPlugin: diagnostic-stub=false
appPlugin: dmp-demo-ui=false
appPlugin: dns-cli=false
appPlugin: door-lock-server=false
appPlugin: dtls-auth-params=false
appPlugin: dtls-cli=false
appPlugin: eeprom=false
appPlugin: eeprom-powerdown=false
appPlugin: end-node-ui=false
appPlugin: ext-device=false
appPlugin: extdev=false
appPlugin: ezradiopro=false
appPlugin: fem-control=false
appPlugin: generic-interrupt-control=false
appPlugin: glib=false
appPlugin: global-address-prefix-debug=false
appPlugin: gpio-sensor=false
appPlugin: groups-server=false
appPlugin: hal-library=false
appPlugin: hal-ncp-library=false
appPlugin: heartbeat=false
appPlugin: heartbeat-node-type=false
appPlugin: host-network-management=true
appPlugin: host-network-management-stub=false
appPlugin: humidity-si7021-stub=false
appPlugin: i2c-driver=false
appPlugin: i2c-driver-stub=false
appPlugin: icmp-cli=false
appPlugin: icmp-debug=false
appPlugin: identify-server=false
appPlugin: idle-sleep=false
appPlugin: illuminance-measurement-server=false
appPlugin: illuminance-si1141-stub=false
appPlugin: infrared-led=false
appPlugin: key-matrix=false
appPlugin: led=false
appPlugin: led-blink=false
appPlugin: led-dim-pwm=false
appPlugin: led-rgb-pwm=false
appPlugin: led-stub=false
appPlugin: led-temp-pwm=false
appPlugin: level-control-server=false
appPlugin: libcoap=false
appPlugin: linked-list=false
appPlugin: log-cli=false
appPlugin: main=true
appPlugin: mbedtls-custom-library=false
appPlugin: mbedtls-dotdot-library=false
appPlugin: mbedtls-library=false
appPlugin: mfglib-library=true
appPlugin: mfglib-stub-library=false
appPlugin: microphone-codec-msadpcm=false
appPlugin: microphone-imaadpcm=false
appPlugin: mpsi=false
appPlugin: mpsi-ipc=false
appPlugin: mpsi-storage=false
appPlugin: ncp-library=false
appPlugin: ncp-spi-link=false
appPlugin: ncp-uart-link=false
appPlugin: network-diagnostics-cli=false
appPlugin: network-management-cli=false
appPlugin: occupancy-pyd1698=false
appPlugin: occupancy-pyd1698-stub=false
appPlugin: occupancy-sensing-server=false
appPlugin: on-off-server=false
appPlugin: ota-bootload-client=false
appPlugin: ota-bootload-client-policy=false
appPlugin: ota-bootload-core=false
appPlugin: ota-bootload-server=false
appPlugin: ota-bootload-server-policy=false
appPlugin: ota-bootload-storage-cli=false
appPlugin: ota-bootload-storage-core=false
appPlugin: ota-bootload-storage-eeprom=false
appPlugin: ota-bootload-storage-unix=false
appPlugin: pa=false
appPlugin: paho.mqtt.c=false
appPlugin: poll-control-client=false
appPlugin: poll-control-server=false
appPlugin: polling=false
appPlugin: power-configuration-server=false
appPlugin: power-meter-cs5463=false
appPlugin: psstore=false
appPlugin: pti=false
appPlugin: rail-library=false
appPlugin: rail-library-mp=false
appPlugin: relative-humidity-measurement-server=false
appPlugin: resource-directory-client=false
appPlugin: sb1-gesture-sensor=false
appPlugin: scan-debug=false
appPlugin: scenes-server=false
appPlugin: serial=true
appPlugin: sim-eeprom1=false
appPlugin: sim-eeprom2=false
appPlugin: sim-eeprom2-1to2-upgrade=false
appPlugin: sim-eeprom2-1to2-upgrade-stub=false
appPlugin: slaac-client=false
appPlugin: slot-manager=false
appPlugin: stm32f103ret-library=false
appPlugin: tamper-switch=false
appPlugin: temperature-measurement-server=false
appPlugin: temperature-si7053-stub=false
appPlugin: thermostat-server=false
appPlugin: thread-mbedtls-stack=false
appPlugin: thread-stack=false
appPlugin: thread-test-harness-callbacks=false
appPlugin: thread-test-harness-cli=true
appPlugin: time-client=false
appPlugin: time-server=false
appPlugin: transport-mqtt=false
appPlugin: udp-cli=false
appPlugin: udp-debug=false
appPlugin: unix-library=true
appPlugin: unix-netif-config=false
appPlugin: version-debug=false
appPlugin: window-covering-server=false
appPlugin: zcl-core=false
appPlugin: zcl-core-cli=false

# Setup configurations
{setupId:additionalFiles
}
{setupId:boardHeader
allowMissingHeader:false
useHeaderInPlace:false
}
{setupId:bookkeeping
}
{setupId:bootloader
}
{setupId:callbackConfiguration
}
{setupId:coapDispatch
}
{setupId:commandLineConfiguration
on
}
{setupId:debugConfiguration
on
area name off
appSerial=1
}
{setupId:eventConfiguration
}
{setupId:halOptions
}
{setupId:hwConfig
featureLevel=1
active=true
}
{setupId:information
\{key:description
Thread Test Host Application.

This is a very simple application that can be used to test the Silicon Labs Thread Stack against the Thread Test Harness for interoperability testing.  It's purpose is to provide a sample app that is ready to be tested against the harness.  It enables the Thread Test Harness CLI plugin and all libraries and plugins required to make these CLI commands operate correctly.

A reference guide for all of the commands needed by the Thread Test Harness can be found in documentation/ThreadTestAppGrlCliDocumentation.txt in the root of the Silicon Labs Thread stack directory.
\}
}
{setupId:macros
}
{setupId:serial
em317=0,false,*,*,*,*,*
em341=0,false,*,*,*,*,*
em342=0,false,*,*,*,*,*
em346=0,false,*,*,*,*,*
em351=0,false,*,*,*,*,*
em355=0,false,*,*,*,*,*
em3555=0,false,*,*,*,*,*
em357=0,false,*,*,*,*,*
em357p2p=0,false,*,*,*,*,*
em3581=0,false,*,*,*,*,*
em3582=0,false,*,*,*,*,*
em3585=0,false,*,*,*,*,*
em3586=0,false,*,*,*,*,*
em3587=0,false,*,*,*,*,*
em3588=0,false,*,*,*,*,*
em3591=0,false,*,*,*,*,*
em3592=0,false,*,*,*,*,*
em3595=0,false,*,*,*,*,*
em3596=0,false,*,*,*,*,*
em3597=0,false,*,*,*,*,*
em3598=0,false,*,*,*,*,*
sky66107=0,false,*,*,*,*,*
simulation=0,false,*,*,*,*,*
efr32=0,false,*,*,*,*,*
}
{setupId:template
}
{setupId:token
}
{setupId:zclip
configuredNetwork:type:ZIGBEE_PRO, name:Primary
beginEndpointType:Primary
device:HA-onoff
endEndpointType
configuredEndpoint:*ep:1,pi: -1,di:-1,dv:0,ept:Primary,nwk:Primary
}

# Plugin options
pluginOption(unix): EMBER_AF_PLUGIN_UNIX_LIBRARY_TOKEN_SUPPORT,true
