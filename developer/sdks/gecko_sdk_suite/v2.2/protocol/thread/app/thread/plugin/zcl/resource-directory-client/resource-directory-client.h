#ifndef __RESOURCE_DIRECTORY_CLIENT_H__
#define __RESOURCE_DIRECTORY_CLIENT_H__

/** @brief Register device clusters with the target Resource Directory.
 *
 * This function will enumerate zcl endpoints and their clusters
 * constructing CoRE Link formatted resources, and
 * send those resources to the specified Resource Directory.
 *
 * @param resourceDirectoryIp The IP address of the resource directory.
 * @param resourceDirectoryPort The port used by the resource directory.
 */
void emberAfPluginResourceDirectoryClientRegister(EmberIpv6Address *resourceDirectoryIp, uint16_t resourceDirectoryPort);

#endif //__RESOURCE_DIRECTORY_CLIENT_H__```
