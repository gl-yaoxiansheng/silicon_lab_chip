#ISD afv6
# Simplicity Studio version: 4.27.0.qualifier

# Application configuration
appId: thread
frameworkRoot: protocol/thread/app/thread
architecture: em3588+iar
deviceName: switch
generationDirectory: PATH(ISC_RELATIVE):.

# Devices
device: EM3xx
device: SoC
device: Sleepy End Device
device: ZCL/IP

# Plugin configuration
appPlugin: accelerometer-led=false
appPlugin: adc=true
appPlugin: address-configuration-debug=true
appPlugin: antenna=false
appPlugin: antenna-stub=true
appPlugin: ash-v3=false
appPlugin: ash-v3-stub=false
appPlugin: basic-server=true
appPlugin: battery-monitor=false
appPlugin: bootload-cli=false
appPlugin: bulb-pwm-driver=false
appPlugin: bulb-ui=false
appPlugin: button=true
appPlugin: button-interface=false
appPlugin: button-press=false
appPlugin: button-stub=false
appPlugin: buzzer=true
appPlugin: buzzer-stub=false
appPlugin: cjson=false
appPlugin: cli=true
appPlugin: coap-cli=false
appPlugin: coap-debug=false
appPlugin: coap-dispatch=true
appPlugin: coexistence=false
appPlugin: color-control-server=false
appPlugin: command-interpreter2=true
appPlugin: connection-manager-cli=false
appPlugin: connection-manager-jib=false
appPlugin: connection-manager-joob=false
appPlugin: debug-channel=true
appPlugin: debug-channel-stub=false
appPlugin: debug-print=true
appPlugin: dhcp-client=true
appPlugin: dhcp-library=true
appPlugin: dhcp-stub-library=false
appPlugin: diagnostic=false
appPlugin: diagnostic-stub=true
appPlugin: dmp-demo-ui=false
appPlugin: dns-cli=false
appPlugin: door-lock-server=false
appPlugin: dtls-auth-params=true
appPlugin: dtls-cli=true
appPlugin: eeprom=true
appPlugin: eeprom-powerdown=false
appPlugin: end-node-ui=false
appPlugin: ext-device=false
appPlugin: extdev=false
appPlugin: ezradiopro=false
appPlugin: fem-control=false
appPlugin: generic-interrupt-control=false
appPlugin: glib=false
appPlugin: global-address-prefix-debug=false
appPlugin: gpio-sensor=false
appPlugin: groups-server=false
appPlugin: hal-library=true
appPlugin: hal-ncp-library=false
appPlugin: heartbeat=true
appPlugin: heartbeat-node-type=false
appPlugin: host-network-management=false
appPlugin: host-network-management-stub=false
appPlugin: humidity-si7021-stub=false
appPlugin: i2c-driver=false
appPlugin: i2c-driver-stub=false
appPlugin: icmp-cli=false
appPlugin: icmp-debug=false
appPlugin: identify-server=true
appPlugin: idle-sleep=true
appPlugin: illuminance-measurement-server=false
appPlugin: illuminance-si1141-stub=false
appPlugin: infrared-led=false
appPlugin: key-matrix=false
appPlugin: led=true
appPlugin: led-blink=false
appPlugin: led-dim-pwm=false
appPlugin: led-rgb-pwm=false
appPlugin: led-stub=false
appPlugin: led-temp-pwm=false
appPlugin: level-control-server=false
appPlugin: libcoap=false
appPlugin: linked-list=false
appPlugin: log-cli=false
appPlugin: main=true
appPlugin: mbedtls-custom-library=false
appPlugin: mbedtls-dotdot-library=false
appPlugin: mbedtls-library=true
appPlugin: mfglib-library=true
appPlugin: mfglib-stub-library=false
appPlugin: microphone-codec-msadpcm=false
appPlugin: microphone-imaadpcm=false
appPlugin: mpsi=false
appPlugin: mpsi-ipc=false
appPlugin: mpsi-storage=false
appPlugin: ncp-library=false
appPlugin: ncp-spi-link=false
appPlugin: ncp-uart-link=false
appPlugin: network-diagnostics-cli=false
appPlugin: network-management-cli=false
appPlugin: occupancy-pyd1698=false
appPlugin: occupancy-pyd1698-stub=false
appPlugin: occupancy-sensing-server=false
appPlugin: on-off-server=false
appPlugin: ota-bootload-client=true
appPlugin: ota-bootload-client-policy=true
appPlugin: ota-bootload-core=true
appPlugin: ota-bootload-server=false
appPlugin: ota-bootload-server-policy=false
appPlugin: ota-bootload-storage-cli=false
appPlugin: ota-bootload-storage-core=true
appPlugin: ota-bootload-storage-eeprom=true
appPlugin: ota-bootload-storage-unix=false
appPlugin: pa=false
appPlugin: paho.mqtt.c=false
appPlugin: poll-control-client=false
appPlugin: poll-control-server=false
appPlugin: polling=true
appPlugin: power-configuration-server=false
appPlugin: power-meter-cs5463=false
appPlugin: psstore=false
appPlugin: pti=false
appPlugin: rail-library=true
appPlugin: rail-library-mp=false
appPlugin: relative-humidity-measurement-server=false
appPlugin: resource-directory-client=false
appPlugin: sb1-gesture-sensor=false
appPlugin: scan-debug=true
appPlugin: scenes-server=false
appPlugin: serial=true
appPlugin: sim-eeprom1=true
appPlugin: sim-eeprom2=false
appPlugin: sim-eeprom2-1to2-upgrade=false
appPlugin: sim-eeprom2-1to2-upgrade-stub=false
appPlugin: slaac-client=true
appPlugin: slot-manager=false
appPlugin: stm32f103ret-library=false
appPlugin: tamper-switch=false
appPlugin: temperature-measurement-server=false
appPlugin: temperature-si7053-stub=false
appPlugin: thermostat-server=false
appPlugin: thread-mbedtls-stack=false
appPlugin: thread-stack=true
appPlugin: thread-test-harness-callbacks=false
appPlugin: thread-test-harness-cli=false
appPlugin: time-client=false
appPlugin: time-server=false
appPlugin: transport-mqtt=false
appPlugin: udp-cli=false
appPlugin: udp-debug=true
appPlugin: unix-library=false
appPlugin: unix-netif-config=false
appPlugin: version-debug=true
appPlugin: window-covering-server=false
appPlugin: zcl-core=true
appPlugin: zcl-core-cli=false

# Setup configurations
{setupId:additionalFiles
PATH(ISC_RELATIVE):switch-implementation.c
}
{setupId:boardHeader
allowMissingHeader:false
useHeaderInPlace:false
}
{setupId:bookkeeping
}
{setupId:bootloader
efr32~series[1]~device_configuration[1]=application,
efr32~series[1]~device_configuration[2]=application,
efr32~series[1]~device_configuration[3]=application,
efr32~series[1]~device_configuration[4]=application,
em3581=application,
em3582=application,
em3585=application,
em3586=application,
em3587=application,
em3588=application,
em3591=application,
em3592=application,
em3595=application,
em3596=application,
em3597=application,
em3598=application,
}
{setupId:callbackConfiguration
emberAfNetworkStatusCallback:false
emberGetGlobalAddressReturn:false
emberResumeNetworkReturn:false
emberCommissionNetworkReturn:false
emberJoinNetworkReturn:false
emberAttachToNetworkReturn:false
emberResetNetworkStateReturn:false
emberAfPluginIdleSleepOkToSleepCallback:false
emberAfPluginPollingOkToLongPollCallback:false
emberZclGetPublicKeyCallback:false
emberZclNotificationCallback:false
emberZclIdentifyServerStartIdentifyingCallback:false
emberZclIdentifyServerStopIdentifyingCallback:false
halButtonIsr:false
}
{setupId:coapDispatch
}
{setupId:commandLineConfiguration
on
}
{setupId:debugConfiguration
on
area name off
appSerial=1
compile,EMBER_AF_PLUGIN_ZCL_CORE
enable,EMBER_AF_PLUGIN_ZCL_CORE
}
{setupId:eventConfiguration
downEventControl,downEventHandler
upEventControl,upEventHandler
ezModeEventControl,ezModeEventHandler
stateEventControl,stateEventHandler
}
{setupId:halOptions
}
{setupId:hwConfig
featureLevel=1
active=true
}
{setupId:information
\{key:description
Light/Switch Sample Applications

This switch application acts as a simple dimmer switch in a ZCL network.  It works with the light sample application to demonstrate basic ZCL over IP functionality in a Thread network.  The light and switch communicate using the Concise Binary Object Representation (CBOR) data format over the Constrained Application Protocol (CoAP), with UDP serving as the transport layer.  CBOR is provided by a plugin while CoAP and UDP are provided by the Silicon Labs Thread stack.

At startup, the switch will automatically start network operations.  If the node is starting for the first time, it will join a network based on fixed, pre-configured parameters.  If it had already joined a network previously, it will simply resume network operations using the network parameters stored in non-volatile memory in the stack.

The light will react to On/Off and Level Control messages from other nodes.  The switch will send On/Off and Level Control messages to a light in response to button pushes.  The down button, which is usually BUTTON0 or PB0, can be used to dim or turn off a light, and the up button, which is usually BUTTON1 or PB1, can be used to brighten or turn on a light.  Quickly pressing and releasing the button will send on/off messages, while a press and hold will send brighten/dim messages.

The switch must have a binding to a light for its buttons to perform any actions.  Bindings can be created using EZ Mode commissioning.  On both the light and the switch, pressing both buttons simultaneously will cause the node to enter EZ Mode.  The "zcl ez-mode start" CLI command will also cause the node to enter EZ Mode.  If two or more devices are in EZ Mode at the same time, they will create bindings to each other.  Once bindings are created, the switch with use the binding to communicate with the light.

The light and switch sample applications both contain OTA Bootload Client support, meaning that they support over-the-air upgrades by way of the OTA Bootload Cluster. Both the light and switch will automatically start OTA Bootload operations once on a network. See the OTA Bootload Client Policy plugin for a simple configuration interface to the OTA Bootload Cluster.

All application code is contained in the switch-implementation.c file within the application directory.
\}
}
{setupId:macros
-DEMBER_COMMAND_BUFFER_LENGTH=250
-DEMBER_SIM_EEPROM_4KB
}
{setupId:serial
}
{setupId:template
simeepromSize=8192
}
{setupId:token
}
{setupId:zclip
beginAttrList:OPTIONAL
  cl:0x0020, at:0x0004, di:server, mf:0x0000
  cl:0x0020, at:0x0005, di:server, mf:0x0000
  cl:0x0020, at:0x0006, di:server, mf:0x0000
endAttrList:OPTIONAL
configuredNetwork:type:ZIGBEE_PRO, name:Primary
beginEndpointType:Primary
device:HA-switch
overrideClientCluster:4,yes
overrideClientCluster:25,yes
endEndpointType
configuredEndpoint:*ep:1,pi: 2222,di:8888,dv:0,ept:Primary,nwk:Primary
}

# Plugin options
pluginOption(simulation): EMBER_AF_PLUGIN_OTA_BOOTLOAD_CLIENT_POLICY_CURRENT_IMAGE_MANUFACTURER_CODE,0x1234
pluginOption(simulation): EMBER_AF_PLUGIN_OTA_BOOTLOAD_CLIENT_POLICY_CURRENT_IMAGE_TYPE,0x5678
pluginOption(simulation): EMBER_AF_PLUGIN_OTA_BOOTLOAD_CLIENT_POLICY_CURRENT_IMAGE_VERSION,0x01010101
pluginOption(simulation): EMBER_AF_PLUGIN_OTA_BOOTLOAD_CLIENT_POLICY_CURRENT_HARDWARE_VERSION,0x0001
pluginOption(em3588): EMBER_AF_PLUGIN_DTLS_AUTH_PARAMS_CERT_PATH,PATH(ABSOLUTE):$FRAMEWORK/plugin/dtls-auth-params/sample-dotdot-certificates.c
