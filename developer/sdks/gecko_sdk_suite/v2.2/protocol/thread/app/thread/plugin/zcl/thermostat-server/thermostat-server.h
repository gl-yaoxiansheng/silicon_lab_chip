// Copyright 2016 Silicon Laboratories, Inc.

#ifndef ZCL_THERMOSTAT_SERVER_H
#define ZCL_THERMOSTAT_SERVER_H

#include EMBER_AF_API_ZCL_CORE

// Define Thermostat plugin Scenes sub-table structure.
typedef struct {
  bool hasOccupiedCoolingSetpointValue;
  int16_t occupiedCoolingSetpointValue;
  bool hasOccupiedHeatingSetpointValue;
  int16_t occupiedHeatingSetpointValue;
  bool hasSystemModeValue;
  uint8_t systemModeValue;
} EmZclThermostatSceneSubTableEntry_t;

#endif // ZCL_THERMOSTAT_SERVER_H
