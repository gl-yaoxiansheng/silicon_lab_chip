// Copyright 2017 Silicon Laboratories, Inc.

#ifndef ZCL_LEVEL_CONTROL_SERVER_H
#define ZCL_LEVEL_CONTROL_SERVER_H

#include EMBER_AF_API_ZCL_CORE

// Define Level Control plugin Scenes sub-table structure.
typedef struct {
  bool hasCurrentLevelValue;
  uint8_t currentLevelValue;
} EmZclLevelControlSceneSubTableEntry_t;

void emberZclLevelControlServerSetOnOff(EmberZclEndpointId_t endpointId, bool value);

#endif // ZCL_LEVEL_CONTROL_SERVER_H
