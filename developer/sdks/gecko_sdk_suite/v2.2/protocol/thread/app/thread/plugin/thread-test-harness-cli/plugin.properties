name=Thread Test Harness CLI
description=This plugin provides all of the CLI and logging output required for testing against the GRL Thread Test Harness.  A reference guide for all of the commands needed by the GRL Thread Test Harness can be found in documentation/ThreadTestAppGrlCliDocumentation.txt in the root of the thread stack directory.
category=Common
quality=internal

architecture=efr32,em357,em3581,em3582,em3585,em3586,em3587,em3588,em3591,em3592,em3595,em3596,em3597,em3598,simulation,unix

providedApis=thread-test-harness-cli
api.thread-test-harness-cli.header=thread-test-harness-cli.h

requiredApis=buffer-management,buffer-queue,coap,event,event-queue,hal,mfglib,network-management,stack

thread-test-harness-cli.c
$FRAMEWORK/../../stack/ip/network-diagnostics.c  (efr32,em357,em3581,em3582,em3585,em3586,em3587,em3588,em3591,em3592,em3595,em3596,em3597,em3598,simulation)
$FRAMEWORK/../util/ip/print-utilities.c          (efr32,em357,em3581,em3582,em3585,em3586,em3587,em3588,em3591,em3592,em3595,em3596,em3597,em3598,simulation)

setup(commandLineConfiguration) {
  force_child_timeout,              forceChildTimeoutCommand,             w
  get_child_timeout,                getChildTimeoutCommand
  request_router_id,                requestRouterIdCommand,               u
  blacklist_eui,                    blacklistEuiCommand,                  b*
  whitelist_eui,                    whitelistEuiCommand,                  b*
  clear_filters,                    clearFiltersCommand
  print_filters,                    printFiltersCommand
  set_lq,                           setLinkQualityCommand,                bu
  jpake_port,                       jpakePortCommand,                     w
  remove_router_by_short_id,        removeRouterByShortIdCommand,         v
  remove_prefix,                    removePrefixCommand,                  b
  version,                          versionCommand
  status,                           statusCommand
  print_child,                      printChildTableCommand
  set_sequence,                     setSequenceCommand,                   v
  com_remove,                       removeCommissionerCommand
  native_petition,                  petitionCommand
  set_prov_url,                     setProvisioningUrlCommand,            b
  need_all_network_data,            needAllNetworkDataCommand,            u
  set_fragment_timeout,             setFragmentTimeoutCommand,            w
  set_coap_ack_timeout,             setCoapAckTimeoutCommand,             w
  get_coap_ack_timeout,             getCoapAckTimeoutCommand
  get_coap_diagnostics,             getCoapDiagnosticsCommand,            bb
  reset_coap_diagnostics,           resetCoapDiagnosticsCommand,          bb
  get_active_dataset,               getActiveDatasetCommand,              bb
  get_pending_dataset,              getPendingDatasetCommand,             bb
  set_active_dataset,               setActiveDatasetCommand,              bv*
  set_pending_dataset,              setPendingDatasetCommand,             bv*
  set_active_dataset_bytes,         setActiveDatasetBytesCommand,         b
  print_active_dataset,             printActiveDatasetCommand
  print_pending_dataset,            printPendingDatasetCommand
  send_pan_id_scan_request,         sendPanIdScanRequestCommand,          bwv
  send_energy_scan_request,         sendEnergyScanRequestCommand,         bwuvv
  increment_sequence,               incrementSequenceCommand
  use_mle_discovery,                useMleDiscoveryCommand,               u*
  randomize_mac_extended_id,        randomizeMacExtendedIdCommand,        u
  set_island_id,                    setIslandIdCommand,                   b
  set_router_selection_parameters,  setRouterSelectionParametersCommand,  uuu
  send_announce_begin,              sendAnnounceBeginCommand,             bvuuv
  force_slaac_address,              forceSlaacAddressCommand,             b
  com_get,                          comGetCommand,                        bb*
  com_set,                          comSetCommand,                        bb*
  print_session_id,                 printSessionIdCommand
  set_min_delay_timer,              forceDelayTimerCommand,               w
  stretch_pskc,                     stretchPskcCommand,                   b
  set_router_selection_jitter_ms,   setRouterSelectionJitterMsCommand,    w
  form_pan,                         formPanCommand,                       usuvbb*
  set_channel,                      setChannelCommand,                    u
  set_eui,                          setEuiCommand,                        b
  print_rip,                        printRipCommand
  set_pan_id,                       setPanIdCommand,                      v
  network_state,                    networkStateCommand
  com_add_steering,                 addSteeringDataCommand,               b
  com_petition,                     commissionerPetitionCommand,          b*
  com_steering,                     sendSteeringDataCommand
  com_join_mode,                    setJoiningModeCommand,                uu
  set_key,                          setKeyCommand,                        bu*
  print_eui_hash,                   printEuiHashCommand
  print_ip_addresses,               printIpAddressesCommand
  print_global_addresses,           globalAddressTableCommand,            b*
  external_route,                   configureExternalRouteCommand,        ubu*
  allow_commissioner,               allowCommissionerCommand,             u
  configure,                        configureCommand,                     uwbbbbv*
  gateway,                          configureGatewayCommand,              vubww
  data_poll,                        dataPollCommand,                      wu*
  join,                             joinCommand,                          usubbvb
  set_join_key,                     setJoinKeyCommand,                    bb*
  join_commissioned,                joinCommissionedCommand,              suu*
  resume,                           resumeNetworkCommand
  reset_network,                    resetNetworkCommand
  option force_dark,                setOptionCommand,                     u
  option reject_coap_solicit,       setOptionCommand,                     u
  ping,                             pingCommand,                          bv*
  reboot,                           resetMicroCommand
  send_address_solicit,             sendAddressSolicitCommand,            u
  enable_app_callbacks,             enableAppCallbacksCommand,            u
  host_version,                     hostVersionCommand
}

setup(macros) {
  -DEMBER_COMMAND_BUFFER_LENGTH=300
}

setup(bookkeeping) {
  init=emberAfPluginThreadTestHarnessCliInit
  networkStatus=emberNetworkStatusHandlerTth
}

setup(eventConfiguration) {
  dataPollEvent, dataPollEventHandler
}

options=useAppCallbacks,fullCommandBuffer

useAppCallbacks.name=Call App Callbacks
useAppCallbacks.description=If you would like to call the app callbacks as well as the ones implemented by this plugin.
useAppCallbacks.type=BOOLEAN
useAppCallbacks.default=FALSE
useAppCallbacks.define=EMBER_AF_THREAD_TEST_CLI_USE_APP_CALLBACKS
useAppCallbacks.hidden=TRUE

fullCommandBuffer.name=Full Command Interpreter Buffer Suppoprt
fullCommandBuffer.description=Enable full command interpreter buffer suppoprt.
fullCommandBuffer.type=BOOLEAN
fullCommandBuffer.default=TRUE
fullCommandBuffer.hidden=TRUE
fullCommandBuffer.macro=-DEMBER_COMMAND_INTERPRETER_HAS_FULL_BUFFER_SUPPOPRT
fullCommandBuffer.enabledIf=conf.architectureMatches("unix")
