#include PLATFORM_HEADER
#include CONFIGURATION_HEADER
#include EMBER_AF_API_ZCL_CORE

/**************************************************************************//**
 * Get the public key used in the application.
 *
 * @param publicKey The returned pointer to the public key data
 * @param publicKeySize The returned size of the public key data
 *
 * The public key data is used to generate this device's UID.
 *
 * @note Both the publicKey and publicKeySize parameters are meant to be
 *       assigned by the implementation of this call. The expectation is that
 *       the public key is a global value, so that the pointer provided by the
 *       implementation of this callback will point to constant public key data.
 *****************************************************************************/
void emberZclGetPublicKeyCallback(const uint8_t **publicKey,
                                  uint16_t *publicKeySize)
{
}

/**************************************************************************//**
 * An attribute is about to change value.
 *
 * @param endpointId The endpoint to which the new attribute value applies
 * @param clusterSpec The cluster to which the new attribute value applies
 * @param attributeId The attribute ID
 * @param buffer The data representing the new attribute value
 * @param bufferLength The length of the new attribute value data
 * @return `true` if the attribute should take this new value, `false`
 *         otherwise.
 *
 * This callback gives the application an opportunity to prevent an attribute
 * from changing value by returning `false`.
 *
 * @sa emberZclPostAttributeChangeCallback
 *****************************************************************************/
bool emberZclPreAttributeChangeCallback(EmberZclEndpointId_t endpointId,
                                        const EmberZclClusterSpec_t *clusterSpec,
                                        EmberZclAttributeId_t attributeId,
                                        const void *buffer,
                                        size_t bufferLength)
{
  return true;
}

/**************************************************************************//**
 * An attribute has changed value.
 *
 * @param endpointId The endpoint to which the new attribute value applies
 * @param clusterSpec The cluster to which the new attribute value applies
 * @param attributeId The attribute ID
 * @param buffer The data representing the new attribute value
 * @param bufferLength The length of the new attribute value data
 *
 * This callback gives the application an opportunity to react to an attribute
 * changing value.
 *
 * @sa emberZclPreAttributeChangeCallback
 *****************************************************************************/
void emberZclPostAttributeChangeCallback(EmberZclEndpointId_t endpointId,
                                         const EmberZclClusterSpec_t *clusterSpec,
                                         EmberZclAttributeId_t attributeId,
                                         const void *buffer,
                                         size_t bufferLength)
{
}

/**************************************************************************//**
 * An external attribute value needs to be read.
 *
 * @param endpointId The endpoint to which the attribute value applies
 * @param clusterSpec The cluster to which the attribute value applies
 * @param attributeId The attribute ID
 * @param buffer The data buffer into which the attribute value will be read
 * @param bufferLength The length of the data buffer
 * @return An ::EmberZclStatus_t value representing the success or failure of
 *         the read operation.
 *
 * This callback alerts the application that an externally stored attribute
 * needs to be read. The application is expected to read the attribute value
 * from its external storage, populate the buffer parameter with the attribute
 * value, and return an ::EmberZclStatus_t value representing the success or
 * failure of the read operation.
 *
 * @sa emberZclWriteExternalAttributeCallback
 *****************************************************************************/
EmberZclStatus_t emberZclReadExternalAttributeCallback(EmberZclEndpointId_t endpointId,
                                                       const EmberZclClusterSpec_t *clusterSpec,
                                                       EmberZclAttributeId_t attributeId,
                                                       void *buffer,
                                                       size_t bufferLength)
{
  return EMBER_ZCL_STATUS_UNSUPPORTED_ATTRIBUTE;
}

/**************************************************************************//**
 * An external attribute value needs to be written.
 *
 * @param endpointId The endpoint to which the attribute value applies
 * @param clusterSpec The cluster to which the attribute value applies
 * @param attributeId The attribute ID
 * @param buffer The data buffer holding the attribute value to be written
 * @param bufferLength The length of the data buffer
 * @return An ::EmberZclStatus_t value representing the success or failure of
 *         the write operation.
 *
 * This callback alerts the application that an externally stored attribute
 * needs to be written. The application is expected to write the attribute value
 * to its external storage and return an ::EmberZclStatus_t value representing
 * the success or failure of the write operation.
 *
 * @sa emberZclReadExternalAttributeCallback
 *****************************************************************************/
EmberZclStatus_t emberZclWriteExternalAttributeCallback(EmberZclEndpointId_t endpointId,
                                                        const EmberZclClusterSpec_t *clusterSpec,
                                                        EmberZclAttributeId_t attributeId,
                                                        const void *buffer,
                                                        size_t bufferLength)
{
  return EMBER_ZCL_STATUS_UNSUPPORTED_ATTRIBUTE;
}

/**************************************************************************//**
 * Get the default reporting configuration for a cluster on an endpoint.
 *
 * @param endpointId The endpoint to which the reporting configuration applies
 * @param clusterSpec The cluster to which the reporting configuration applies
 * @param configuration The reporting configuration structure to populate with
 *                      the default reporting configuration
 *
 * This callback gives the application an opportunity to define the default
 * reporting configuration for each cluster on each endpoint. The configuration
 * parameter will be passed with default values and the application is expected
 * to update this parameter with its desired reporting configuration. This
 * callback is called when a reporting configuration is reset to its defaults.
 *
 * @sa emberZclGetDefaultReportableChangeCallback
 *****************************************************************************/
void emberZclGetDefaultReportingConfigurationCallback(EmberZclEndpointId_t endpointId,
                                                      const EmberZclClusterSpec_t *clusterSpec,
                                                      EmberZclReportingConfiguration_t *configuration)
{
}

/**************************************************************************//**
 * Get the default reportable change for an attribute.
 *
 * @param endpointId The endpoint to which the reportable change applies
 * @param clusterSpec The cluster to which the reportable change applies
 * @param attributeId The attribute ID
 * @param buffer The data buffer to populate with the reportable change value
 * @param bufferLength The length of the data buffer
 *
 * This callback gives the application an opportunity to define the reportable
 * change value for each attribute that is being reported. This callback is
 * called when a reporting configuration is reset to its defaults.
 *
 * @sa emberZclGetDefaultReportingConfigurationCallback
 *****************************************************************************/
void emberZclGetDefaultReportableChangeCallback(EmberZclEndpointId_t endpointId,
                                                const EmberZclClusterSpec_t *clusterSpec,
                                                EmberZclAttributeId_t attributeId,
                                                void *buffer,
                                                size_t bufferLength)
{
}

/**************************************************************************//**
 * A notification has been received.
 *
 * @param context Information about the notification
 * @param clusterSpec The cluster to which the attribute applies
 * @param attributeId The attribute ID to which the notification applies
 * @param buffer The data buffer containing the reported attribute value
 * @param bufferLength The length of the data buffer
 *****************************************************************************/
void emberZclNotificationCallback(const EmberZclNotificationContext_t *context,
                                  const EmberZclClusterSpec_t *clusterSpec,
                                  EmberZclAttributeId_t attributeId,
                                  const void *buffer,
                                  size_t bufferLength)
{
}
