var NAVTREE =
[
  [ "Thread", "index.html", [
    [ "API Reference", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group__USB__DEVICE.html#ga0e5f5bd847e7cc1c4c384bb6b7f0ce77",
"group__ashv3.html#ga1699610a55a5261242a32ef2513f1c8d",
"group__button-interface-callbacks.html#gab1572a9d2677345e578ceca3f0ca2d81",
"group__coap.html#gga149c56470fc6b1901eb38aff92a53d6caf4b96a010b3a4ebdb1f5c36ec5799fae",
"group__form__and__join.html#ga3e36f44ad39f3844d16feecdd2e5104d",
"group__idle-sleep-callbacks.html#ga15efb779046597903634736ee35fb9b6",
"group__network-management-callbacks.html#ga15d0ec064bd17388e03c6b72411aa0d6",
"group__platform__common.html#ga930cd568cc48ccfe26affc627e8ed85a",
"group__utilities.html#ga33fb8289dee931642c361e0694eae751",
"group__utilities.html#gga6b3ce50dfcce5e1068074a784936f35ea4a70f65901cb2c812be11afeb911e5e2",
"structEmberCommandEntry.html#a235b4af236c5529b9590c3ad09f2449f",
"structUSB__ConfigurationDescriptor__TypeDef.html#a6d4e694693ae9f2f45bbe1a4fe3881f2"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';