var structUSBD__Callbacks__TypeDef =
[
    [ "isSelfPowered", "structUSBD__Callbacks__TypeDef.html#a41cdc1ee710af186504583727ac64ea9", null ],
    [ "setupCmd", "structUSBD__Callbacks__TypeDef.html#a4735f0df935af594ea2f331581211206", null ],
    [ "sofInt", "structUSBD__Callbacks__TypeDef.html#a5506e8d86a3a7f08361a883efbddc1b5", null ],
    [ "usbReset", "structUSBD__Callbacks__TypeDef.html#a93a3dcf2bbef9f22b35f37df969b6ac7", null ],
    [ "usbStateChange", "structUSBD__Callbacks__TypeDef.html#af4bb516d0199344f9a7ca21c20b43f8f", null ]
];