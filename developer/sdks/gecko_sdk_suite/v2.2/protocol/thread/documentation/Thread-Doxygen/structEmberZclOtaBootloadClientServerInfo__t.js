var structEmberZclOtaBootloadClientServerInfo__t =
[
    [ "address", "structEmberZclOtaBootloadClientServerInfo__t.html#a9fafc355729c0acd1f5c15db6e60cad4", null ],
    [ "endpointId", "structEmberZclOtaBootloadClientServerInfo__t.html#abea5d029589a38bd82aba15eb5544c69", null ],
    [ "name", "structEmberZclOtaBootloadClientServerInfo__t.html#ab454405cf3eb9dd7fb2e0b5f838f75ab", null ],
    [ "nameLength", "structEmberZclOtaBootloadClientServerInfo__t.html#a2ce0ec724e94102c885b186a9d584ee4", null ],
    [ "port", "structEmberZclOtaBootloadClientServerInfo__t.html#ae017cabad5c2b0a5dfb35d25ec1b86a8", null ],
    [ "scheme", "structEmberZclOtaBootloadClientServerInfo__t.html#ad252cb7dfda1ee58b8d3e1b5bb845aa2", null ],
    [ "uid", "structEmberZclOtaBootloadClientServerInfo__t.html#a75771797dbe32cabcf79d732c58cf3c8", null ]
];