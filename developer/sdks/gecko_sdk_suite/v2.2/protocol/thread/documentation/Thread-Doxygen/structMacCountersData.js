var structMacCountersData =
[
    [ "numberOfRetries", "structMacCountersData.html#a9160cf01cd8baf4b5f8840f48f2242d5", null ],
    [ "packetsDroppedOnReceive", "structMacCountersData.html#a7eb624f5a7586960406199d3459f5d15", null ],
    [ "packetsDroppedOnTransmit", "structMacCountersData.html#ac1cf03dff2ed4b0af781861540d7ea6f", null ],
    [ "packetsReceived", "structMacCountersData.html#a70fa914551b54a503ce66e2f5a0806ec", null ],
    [ "packetsSent", "structMacCountersData.html#aad58ee1f214bf04bccf51b92c88b34ad", null ],
    [ "securityErrors", "structMacCountersData.html#a484e24455b8af419d1807fcbdfdb8840", null ]
];