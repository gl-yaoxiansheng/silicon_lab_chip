var structEmberCommissionData =
[
    [ "provisioningUrl", "structEmberCommissionData.html#a46e4d2264382f55a58ca230d16fecaa4", null ],
    [ "swVersion", "structEmberCommissionData.html#aaccf7411ed00e69ecf86c8c370d10e4c", null ],
    [ "tlvMask", "structEmberCommissionData.html#a42b7972d6e5d7785f4829883d6df8850", null ],
    [ "vendorData", "structEmberCommissionData.html#adb4b7b4eb7ab717cd5948fff1f7e9b77", null ],
    [ "vendorModel", "structEmberCommissionData.html#a98742b91dc0bdca2b0d9606e9c3d9b0d", null ],
    [ "vendorName", "structEmberCommissionData.html#afb75bb33b7cb39d963892c407b4f6982", null ],
    [ "vendorStackVersion", "structEmberCommissionData.html#a65173454e0a94520ad7ddcf99991811f", null ]
];