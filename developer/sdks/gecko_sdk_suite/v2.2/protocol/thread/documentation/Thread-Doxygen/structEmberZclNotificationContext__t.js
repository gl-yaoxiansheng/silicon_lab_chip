var structEmberZclNotificationContext__t =
[
    [ "endpointId", "structEmberZclNotificationContext__t.html#a4f13a32f9bd0f997570451b4ce32d3ec", null ],
    [ "groupId", "structEmberZclNotificationContext__t.html#ab1fac8a75657fcc7842f88adf0bc1207", null ],
    [ "remoteAddress", "structEmberZclNotificationContext__t.html#aad243364a0c54159010e33db0f74ef6a", null ],
    [ "sourceEndpointId", "structEmberZclNotificationContext__t.html#acae0e374de18f18c4aa3fc349c7126f4", null ],
    [ "sourceReportingConfigurationId", "structEmberZclNotificationContext__t.html#a60f83459ae657b9a7cc079ce58034ade", null ],
    [ "sourceTimestamp", "structEmberZclNotificationContext__t.html#a1b13f952435c4513fda8865742257486", null ]
];