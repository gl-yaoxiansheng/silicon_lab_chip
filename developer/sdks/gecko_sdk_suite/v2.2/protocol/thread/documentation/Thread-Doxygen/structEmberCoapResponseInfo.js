var structEmberCoapResponseInfo =
[
    [ "applicationData", "structEmberCoapResponseInfo.html#add3d46b588b8ca5f4e060a73ff0fe851", null ],
    [ "applicationDataLength", "structEmberCoapResponseInfo.html#a5d1a3abe1b5622d4e029ad4cf3138d28", null ],
    [ "localAddress", "structEmberCoapResponseInfo.html#afeac89bf691deb75e49e4eb9d41da212", null ],
    [ "localPort", "structEmberCoapResponseInfo.html#a44e94a1a6d3d72c2b9b5f84476dfbc8f", null ],
    [ "remoteAddress", "structEmberCoapResponseInfo.html#a725db4658c5ae4d7d739615887191d9e", null ],
    [ "remotePort", "structEmberCoapResponseInfo.html#a932bd99806c4d9e6ee7548913cf1ad00", null ]
];