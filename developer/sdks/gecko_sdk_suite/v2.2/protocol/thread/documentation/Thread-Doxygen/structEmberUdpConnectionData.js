var structEmberUdpConnectionData =
[
    [ "connection", "structEmberUdpConnectionData.html#aff7a4bf7df15a8b73b3146786d2d99cf", null ],
    [ "flags", "structEmberUdpConnectionData.html#a824e5735997dffe8af184726f9c5e0ee", null ],
    [ "internal", "structEmberUdpConnectionData.html#aec4dc9f0834eb0d2c8f8ef346b2ae6c0", null ],
    [ "localAddress", "structEmberUdpConnectionData.html#aa75133209ec29261e010e78b8a6196b1", null ],
    [ "localPort", "structEmberUdpConnectionData.html#a645774093f7cbcbd40b0d01652d0c04a", null ],
    [ "remoteAddress", "structEmberUdpConnectionData.html#a088c8c1d032a8572f6a674305f7fe4a4", null ],
    [ "remotePort", "structEmberUdpConnectionData.html#a897ad358244dc9934826e1d30d6710b7", null ]
];