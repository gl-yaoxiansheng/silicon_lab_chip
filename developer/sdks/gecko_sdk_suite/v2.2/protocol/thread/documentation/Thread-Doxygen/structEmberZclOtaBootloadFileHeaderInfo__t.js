var structEmberZclOtaBootloadFileHeaderInfo__t =
[
    [ "destination", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a76762a64577b7ce92c39ec1374eca9b2", null ],
    [ "fileSize", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a29d6e1c19e737491f0b880fbe3b5c8b2", null ],
    [ "hardwareVersionRange", "structEmberZclOtaBootloadFileHeaderInfo__t.html#aa0823132ada4328479bd940a06f08b61", null ],
    [ "headerSize", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a01352a55a56b79e641a4b2751a37e3fb", null ],
    [ "securityCredentialVersion", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a8fd9b915627901de40ef048f1be31805", null ],
    [ "spec", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a713da3ba094e0ec41a32889a23b57c11", null ],
    [ "stackVersion", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a90c581329d50c6dfac25f4313b17728c", null ],
    [ "string", "structEmberZclOtaBootloadFileHeaderInfo__t.html#ac55157385c7fcbc0b9f39750ff0c44aa", null ],
    [ "version", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a84b2434a7a0edccd42006c377dc54ad9", null ]
];