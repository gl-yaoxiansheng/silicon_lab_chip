var searchData=
[
  ['windex',['wIndex',['../structUSB__Setup__TypeDef.html#ae906e4bb150f8c859e7bc945bec15761',1,'USB_Setup_TypeDef']]],
  ['wlength',['wLength',['../structUSB__Setup__TypeDef.html#a144a954da389673e95ee9fbdae12d347',1,'USB_Setup_TypeDef']]],
  ['wmaxpacketsize',['wMaxPacketSize',['../structUSB__EndpointDescriptor__TypeDef.html#acbc03e1419bc9b2bc449e8ffff75aac4',1,'USB_EndpointDescriptor_TypeDef']]],
  ['wordsizebytes',['wordSizeBytes',['../structHalEepromInformationType.html#afb67006c7650887919c11079ade12657',1,'HalEepromInformationType']]],
  ['wtotallength',['wTotalLength',['../structUSB__ConfigurationDescriptor__TypeDef.html#acf246515d641b69d4a1d829808deeeca',1,'USB_ConfigurationDescriptor_TypeDef']]],
  ['wvalue',['wValue',['../structUSB__Setup__TypeDef.html#a668bf675fd2d40afb995d23e6e03a7d3',1,'USB_Setup_TypeDef']]]
];
