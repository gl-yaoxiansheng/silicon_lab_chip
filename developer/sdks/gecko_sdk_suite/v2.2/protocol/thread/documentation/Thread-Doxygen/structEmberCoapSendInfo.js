var structEmberCoapSendInfo =
[
    [ "localAddress", "structEmberCoapSendInfo.html#a3ce2ab7a2431738bce47185684d9340a", null ],
    [ "localPort", "structEmberCoapSendInfo.html#a71f9583cd0f66e5272f17bb1720f1101", null ],
    [ "multicastLoopback", "structEmberCoapSendInfo.html#a476885b5806427a1e7a0cd59edd62057", null ],
    [ "nonConfirmed", "structEmberCoapSendInfo.html#a2fa4d0921f0f520e85a85e56f249cbde", null ],
    [ "numberOfOptions", "structEmberCoapSendInfo.html#a3ee9dac1baf6f55fb8f08361730b579f", null ],
    [ "options", "structEmberCoapSendInfo.html#a070c9110c5291132950e2b2a403e854a", null ],
    [ "remotePort", "structEmberCoapSendInfo.html#abea5051936dd78b844baa4140a533236", null ],
    [ "responseAppData", "structEmberCoapSendInfo.html#af7bdf238e6a140cb6ec6718eaa71d702", null ],
    [ "responseAppDataLength", "structEmberCoapSendInfo.html#abae2c4cb1e6781f54ee7dfe44740df20", null ],
    [ "responseTimeoutMs", "structEmberCoapSendInfo.html#a5a5d82920b5093918462ea978d66bd17", null ],
    [ "transmitHandler", "structEmberCoapSendInfo.html#a1edb006a6d87f25d38eba48c63cc14f4", null ],
    [ "transmitHandlerData", "structEmberCoapSendInfo.html#ad3a3760790e842ec1097e36240cb94ef", null ]
];