var group__messaging =
[
    [ "ICMP messages", "group__icmp.html", "group__icmp" ],
    [ "UDP messages", "group__udp.html", "group__udp" ],
    [ "Constrained Application Protocol API", "group__coap.html", "group__coap" ],
    [ "DTLS API", "group__dtls.html", "group__dtls" ]
];