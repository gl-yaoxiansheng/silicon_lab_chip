var structEmberCoapRequestInfo =
[
    [ "ackData", "structEmberCoapRequestInfo.html#a6002c482b362e77b7880dd15982b6dac", null ],
    [ "localAddress", "structEmberCoapRequestInfo.html#a99982e64a142abc2864edf6fdacc5b9c", null ],
    [ "localPort", "structEmberCoapRequestInfo.html#a2376110ae1cb025e4bb0d3ac8d7af4b7", null ],
    [ "remoteAddress", "structEmberCoapRequestInfo.html#ac97ccd31a2c72cb4063e37c7f6e74103", null ],
    [ "remotePort", "structEmberCoapRequestInfo.html#a441363855407f6c60e7f04ed2aa583d6", null ],
    [ "token", "structEmberCoapRequestInfo.html#a2c9c5376d500d6c00b740e5bdeda4e18", null ],
    [ "tokenLength", "structEmberCoapRequestInfo.html#aae372b574fbac8ea4f98f14dc81bc09e", null ],
    [ "transmitHandler", "structEmberCoapRequestInfo.html#a47a5c765b5ad1f4d924c816d0af6cb7f", null ],
    [ "transmitHandlerData", "structEmberCoapRequestInfo.html#a7007471ced41b340938a7102e90513c9", null ]
];