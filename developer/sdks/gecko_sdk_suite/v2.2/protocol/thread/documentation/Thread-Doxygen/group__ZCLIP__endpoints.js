var group__ZCLIP__endpoints =
[
    [ "EMBER_ZCL_DEVICE_ID_NULL", "group__ZCLIP__endpoints.html#ga78e589c57d94d65a796d69cf6a1c77d4", null ],
    [ "EMBER_ZCL_ENDPOINT_INDEX_NULL", "group__ZCLIP__endpoints.html#ga58f01057e33143cd72a40f8aef317414", null ],
    [ "EMBER_ZCL_ENDPOINT_MAX", "group__ZCLIP__endpoints.html#ga65bc9d86bd92c789975f7f25ac63ef15", null ],
    [ "EMBER_ZCL_ENDPOINT_MIN", "group__ZCLIP__endpoints.html#ga7c64ad77dd44cc36c5d8877ba7cc6823", null ],
    [ "EMBER_ZCL_ENDPOINT_NULL", "group__ZCLIP__endpoints.html#gabd3790e653e45e9eefb8fc67cda40288", null ],
    [ "EmberZclDeviceId_t", "group__ZCLIP__endpoints.html#ga437846291f79f8a1a551e01447f703af", null ],
    [ "EmberZclEndpointId_t", "group__ZCLIP__endpoints.html#ga73b56cb993891506c2e6cd5a76e5bf53", null ],
    [ "EmberZclEndpointIndex_t", "group__ZCLIP__endpoints.html#ga5c8dcdc694410704c39d40218554b6a1", null ],
    [ "emberZclEndpointIdToIndex", "group__ZCLIP__endpoints.html#ga51b94289b7cde1d6596e79b599ad5c8e", null ],
    [ "emberZclEndpointIndexToId", "group__ZCLIP__endpoints.html#ga967ed564f98a447c7802985c11697e31", null ]
];