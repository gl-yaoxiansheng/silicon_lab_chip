var indexSectionsWithContent =
{
  0: "_abcdefghijlmnopqrstuvwz",
  1: "abcdehimrtu",
  2: "abcdefhilmnoprstuz",
  3: "_abcehimnprsu",
  4: "abcdefghijlmnopqrstuvw",
  5: "bcdehilpstuw",
  6: "abehlmsu",
  7: "abcdegilmrstu",
  8: "acdehimnstuv",
  9: "abcdefghilmnoprstuz",
  10: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

