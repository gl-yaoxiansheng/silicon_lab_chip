var group__ZCLIP__reporting =
[
    [ "EmberZclNotificationContext_t", "structEmberZclNotificationContext__t.html", [
      [ "endpointId", "structEmberZclNotificationContext__t.html#a4f13a32f9bd0f997570451b4ce32d3ec", null ],
      [ "groupId", "structEmberZclNotificationContext__t.html#ab1fac8a75657fcc7842f88adf0bc1207", null ],
      [ "remoteAddress", "structEmberZclNotificationContext__t.html#aad243364a0c54159010e33db0f74ef6a", null ],
      [ "sourceEndpointId", "structEmberZclNotificationContext__t.html#acae0e374de18f18c4aa3fc349c7126f4", null ],
      [ "sourceReportingConfigurationId", "structEmberZclNotificationContext__t.html#a60f83459ae657b9a7cc079ce58034ade", null ],
      [ "sourceTimestamp", "structEmberZclNotificationContext__t.html#a1b13f952435c4513fda8865742257486", null ]
    ] ],
    [ "EmberZclReportingConfiguration_t", "structEmberZclReportingConfiguration__t.html", [
      [ "maximumIntervalS", "structEmberZclReportingConfiguration__t.html#abc1024ffb01321592f219eb938ecfa24", null ],
      [ "minimumIntervalS", "structEmberZclReportingConfiguration__t.html#aa464d560dcfe255ac2e03283abe6846d", null ]
    ] ],
    [ "EMBER_ZCL_REPORTING_CONFIGURATION_DEFAULT", "group__ZCLIP__reporting.html#ga89bfa606b046305e2b3fdc61eb1c578b", null ],
    [ "EMBER_ZCL_REPORTING_CONFIGURATION_NULL", "group__ZCLIP__reporting.html#ga59a2f4eac6c65dba38e5e1687f14c46a", null ],
    [ "EmberZclReportingConfigurationId_t", "group__ZCLIP__reporting.html#ga4b8c69375c2d38b78cbb6bd87f66a812", null ],
    [ "emberZclReportingConfigurationsFactoryReset", "group__ZCLIP__reporting.html#gac510d9a977b27361fa18940f7b47d648", null ]
];