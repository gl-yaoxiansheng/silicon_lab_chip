var group__ZCLIP__groups =
[
    [ "EmberZclGroupEntry_t", "structEmberZclGroupEntry__t.html", [
      [ "endpointId", "structEmberZclGroupEntry__t.html#a07156ea9e7aeb494b5a888c3129218b8", null ],
      [ "groupId", "structEmberZclGroupEntry__t.html#abf20a1431024ac84a78a1c4ec9ac195b", null ],
      [ "groupName", "structEmberZclGroupEntry__t.html#a6e07e47504ad53587c1214ebf288e718", null ],
      [ "groupNameLength", "structEmberZclGroupEntry__t.html#ac181b8f03a8c856029a3bc4ca807b858", null ]
    ] ],
    [ "EMBER_ZCL_GROUP_ALL_ENDPOINTS", "group__ZCLIP__groups.html#gac3404a65a0dda688a2e2f63b1e96412b", null ],
    [ "EMBER_ZCL_GROUP_MAX", "group__ZCLIP__groups.html#ga92116244d588198790e254517bf367f9", null ],
    [ "EMBER_ZCL_GROUP_MIN", "group__ZCLIP__groups.html#gac238513636c005a9a8aaa66be1832346", null ],
    [ "EMBER_ZCL_GROUP_NULL", "group__ZCLIP__groups.html#gaa35e2b032292d1a2258c56c8c65bafa8", null ],
    [ "EMBER_ZCL_MAX_GROUP_NAME_LENGTH", "group__ZCLIP__groups.html#gaf421bb51306aa3d88952323ec2142bec", null ],
    [ "EmberZclGroupId_t", "group__ZCLIP__groups.html#ga30dc1afe7606492066cd67aadb35d4e1", null ],
    [ "emberZclAddEndpointToGroup", "group__ZCLIP__groups.html#gaad6a658981cd15a41d6cadb121592b91", null ],
    [ "emberZclGetGroupName", "group__ZCLIP__groups.html#gac31b2ba8ba8f9a05a34ca98c96fcf80a", null ],
    [ "emberZclIsEndpointInGroup", "group__ZCLIP__groups.html#ga5d5d86217b0829c58af7387a6b96b71e", null ],
    [ "emberZclRemoveAllGroups", "group__ZCLIP__groups.html#ga149424cdde56bb5fd3e1ad9f69e9c152", null ],
    [ "emberZclRemoveEndpointFromAllGroups", "group__ZCLIP__groups.html#gae0ae6e2750be4bb1cbcefcc2ea594ba4", null ],
    [ "emberZclRemoveEndpointFromGroup", "group__ZCLIP__groups.html#ga4b6868aee5860946c8c38ea8268c1d77", null ],
    [ "emberZclRemoveGroup", "group__ZCLIP__groups.html#gae2434fe0f57712b74fa0875650a14201", null ]
];