var group__ota_bootload_client_callbacks =
[
    [ "emberZclOtaBootloadClientDownloadCompleteCallback", "group__ota-bootload-client-callbacks.html#ga92b7b942db554ce4176941eb18f8c7fe", null ],
    [ "emberZclOtaBootloadClientGetQueryNextImageParametersCallback", "group__ota-bootload-client-callbacks.html#gabd28a6810df8faaa17c5f39df1e8bda9", null ],
    [ "emberZclOtaBootloadClientPreBootloadCallback", "group__ota-bootload-client-callbacks.html#ga0e8ce13a54e530b2ac54e46e1b28a9e1", null ],
    [ "emberZclOtaBootloadClientServerDiscoveredCallback", "group__ota-bootload-client-callbacks.html#ga8a82e9f7cda15ba3d5f21389a61731a9", null ],
    [ "emberZclOtaBootloadClientServerHasDiscByClusterId", "group__ota-bootload-client-callbacks.html#ga7d8265ce61a7a892f68da13079079c18", null ],
    [ "emberZclOtaBootloadClientServerHasDnsNameCallback", "group__ota-bootload-client-callbacks.html#gac8339318a56761ae4682dfd22ef73510", null ],
    [ "emberZclOtaBootloadClientServerHasStaticAddressCallback", "group__ota-bootload-client-callbacks.html#ga8d89f7948608501dac4ca162f8f3429f", null ],
    [ "emberZclOtaBootloadClientSetVersionInfoCallback", "group__ota-bootload-client-callbacks.html#ga55373c6e7558240595888c0101453f8c", null ],
    [ "emberZclOtaBootloadClientStartDownloadCallback", "group__ota-bootload-client-callbacks.html#gab577fde3eb7a62aa7de385fba9658114", null ]
];