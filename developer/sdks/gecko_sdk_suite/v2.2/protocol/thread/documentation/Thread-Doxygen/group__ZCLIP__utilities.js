var group__ZCLIP__utilities =
[
    [ "EMBER_ZCL_LONG_STRING_LENGTH_INVALID", "group__ZCLIP__utilities.html#ga6f3dfd936f117dc3cb2f4e96c010bbb9", null ],
    [ "EMBER_ZCL_LONG_STRING_LENGTH_MAX", "group__ZCLIP__utilities.html#gaf6dd1d81847282a3199a7d8287ba185a", null ],
    [ "EMBER_ZCL_LONG_STRING_OVERHEAD", "group__ZCLIP__utilities.html#ga111f5a78993cd7f81070c0193df12b65", null ],
    [ "EMBER_ZCL_STRING_LENGTH_INVALID", "group__ZCLIP__utilities.html#ga86290e6b7f3059c49380197f51790339", null ],
    [ "EMBER_ZCL_STRING_LENGTH_MAX", "group__ZCLIP__utilities.html#gaa395dc171e3132ff89d1168eb1aa1d14", null ],
    [ "EMBER_ZCL_STRING_OVERHEAD", "group__ZCLIP__utilities.html#gabcd439af00fe1568619197cdb2c7b558", null ],
    [ "EMBER_ZCL_URI_MAX_LENGTH", "group__ZCLIP__utilities.html#ga4d0465dfe65c1b02945fecba1c8cdb6c", null ],
    [ "EMBER_ZCL_URI_PATH_CLUSTER_ID_MAX_LENGTH", "group__ZCLIP__utilities.html#ga317102b78414994c235f719e572779a4", null ],
    [ "EMBER_ZCL_URI_PATH_MANUFACTURER_CODE_CLUSTER_ID_SEPARATOR", "group__ZCLIP__utilities.html#gace39d294a045b58960848fd959fc2fb9", null ],
    [ "EMBER_ZCL_URI_PATH_MAX_LENGTH", "group__ZCLIP__utilities.html#ga04759659a18ed6337deedb2e55842c2f", null ],
    [ "emberZclLongStringLength", "group__ZCLIP__utilities.html#gaa0c994aecf126ced03f7bc9d2add1d7d", null ],
    [ "emberZclLongStringSize", "group__ZCLIP__utilities.html#ga3fe2dbae1fe405f9189fd0fe416ad903", null ],
    [ "emberZclStringLength", "group__ZCLIP__utilities.html#ga1bdb8753f4b3b455bab668c8d10c770e", null ],
    [ "emberZclStringSize", "group__ZCLIP__utilities.html#ga75c704248472a142dd67002de1c1eab7", null ]
];