var structUSB__ConfigurationDescriptor__TypeDef =
[
    [ "bConfigurationValue", "structUSB__ConfigurationDescriptor__TypeDef.html#ac8659dab4c9d11ba596456bf4f9cc95c", null ],
    [ "bDescriptorType", "structUSB__ConfigurationDescriptor__TypeDef.html#a1156640fd0a4fd1bb412767d31b09728", null ],
    [ "bLength", "structUSB__ConfigurationDescriptor__TypeDef.html#a74a56aef20a25bcbaaf2f2847657270c", null ],
    [ "bmAttributes", "structUSB__ConfigurationDescriptor__TypeDef.html#a966f9804d3eea81996cee29b5d599b69", null ],
    [ "bMaxPower", "structUSB__ConfigurationDescriptor__TypeDef.html#a6d4e694693ae9f2f45bbe1a4fe3881f2", null ],
    [ "bNumInterfaces", "structUSB__ConfigurationDescriptor__TypeDef.html#a2c6c56494432c410f974da344187dac7", null ],
    [ "iConfiguration", "structUSB__ConfigurationDescriptor__TypeDef.html#a04fde689ac298063e3e055c1dd53f698", null ],
    [ "wTotalLength", "structUSB__ConfigurationDescriptor__TypeDef.html#acf246515d641b69d4a1d829808deeeca", null ]
];