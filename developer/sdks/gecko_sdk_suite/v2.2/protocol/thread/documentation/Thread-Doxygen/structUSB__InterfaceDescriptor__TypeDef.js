var structUSB__InterfaceDescriptor__TypeDef =
[
    [ "bAlternateSetting", "structUSB__InterfaceDescriptor__TypeDef.html#a1198465f4027db434800ace66531aa27", null ],
    [ "bDescriptorType", "structUSB__InterfaceDescriptor__TypeDef.html#a8a6d3c3083c3b4c13ceeb16b619d8292", null ],
    [ "bInterfaceClass", "structUSB__InterfaceDescriptor__TypeDef.html#a3e284a88208c9841dceeb5e6a326b69f", null ],
    [ "bInterfaceNumber", "structUSB__InterfaceDescriptor__TypeDef.html#ab7c3b5907069a0895e95f0524356071d", null ],
    [ "bInterfaceProtocol", "structUSB__InterfaceDescriptor__TypeDef.html#a2b83bd41458ccf86d70bb7a9b48da4d3", null ],
    [ "bInterfaceSubClass", "structUSB__InterfaceDescriptor__TypeDef.html#a7b874e4ff88e70d5e734b4d9dd4b2a91", null ],
    [ "bLength", "structUSB__InterfaceDescriptor__TypeDef.html#a9bf7796350d45356a013351ec7a59dc9", null ],
    [ "bNumEndpoints", "structUSB__InterfaceDescriptor__TypeDef.html#a4b8d9f53062baffaf74b2073e5686d42", null ],
    [ "iInterface", "structUSB__InterfaceDescriptor__TypeDef.html#a3960fcce5606ac9e84b1cbdad5c243ff", null ]
];