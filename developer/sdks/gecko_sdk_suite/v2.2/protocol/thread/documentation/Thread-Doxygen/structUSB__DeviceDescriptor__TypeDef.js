var structUSB__DeviceDescriptor__TypeDef =
[
    [ "bcdDevice", "structUSB__DeviceDescriptor__TypeDef.html#ac3c06609311cf168f74e99cb624f3d8f", null ],
    [ "bcdUSB", "structUSB__DeviceDescriptor__TypeDef.html#a5cd87d581caeb5940bbb86d6c43773ed", null ],
    [ "bDescriptorType", "structUSB__DeviceDescriptor__TypeDef.html#ad9842e78413979458f03ac5ee27544d0", null ],
    [ "bDeviceClass", "structUSB__DeviceDescriptor__TypeDef.html#a3d16ba4cee2e7b2126be35870e025454", null ],
    [ "bDeviceProtocol", "structUSB__DeviceDescriptor__TypeDef.html#ad45c63c10a6b8d1893cbf0186cd924e0", null ],
    [ "bDeviceSubClass", "structUSB__DeviceDescriptor__TypeDef.html#a85c104fd3160a9e2c9544fa169f06e59", null ],
    [ "bLength", "structUSB__DeviceDescriptor__TypeDef.html#a6e404a9da8a59fbc3081cd3cba3c093a", null ],
    [ "bMaxPacketSize0", "structUSB__DeviceDescriptor__TypeDef.html#abc268b6fa37575fdc8072b770abdb9d9", null ],
    [ "bNumConfigurations", "structUSB__DeviceDescriptor__TypeDef.html#a1238a2c1cf694c05a23bc7fcd8aff3f1", null ],
    [ "idProduct", "structUSB__DeviceDescriptor__TypeDef.html#a4a155878a5fe337f4e358920cb1a8191", null ],
    [ "idVendor", "structUSB__DeviceDescriptor__TypeDef.html#a1e99547988357fa1ab7dda3a95ac04f7", null ],
    [ "iManufacturer", "structUSB__DeviceDescriptor__TypeDef.html#a1c6080b3e081e23f1c270d23ab3eb2a8", null ],
    [ "iProduct", "structUSB__DeviceDescriptor__TypeDef.html#a16e9ce8bec78927eb2d789a3fb9e37bc", null ],
    [ "iSerialNumber", "structUSB__DeviceDescriptor__TypeDef.html#ab889285b543baf0d04b9b5e8defb198d", null ]
];