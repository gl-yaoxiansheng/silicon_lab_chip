var structUSBD__Init__TypeDef =
[
    [ "bufferingMultiplier", "structUSBD__Init__TypeDef.html#adac2ad6ec2b476697054bf4d442d2e61", null ],
    [ "callbacks", "structUSBD__Init__TypeDef.html#ab7362b4c08a63e16e79e5dab224331c8", null ],
    [ "configDescriptor", "structUSBD__Init__TypeDef.html#a0a79a532500f7d8618fdcf491a5a7b13", null ],
    [ "deviceDescriptor", "structUSBD__Init__TypeDef.html#a9b34de34ccc1726c268b646419f5a1b4", null ],
    [ "numberOfStrings", "structUSBD__Init__TypeDef.html#a7dffaf3d3163e86be4304269d89047a1", null ],
    [ "reserved", "structUSBD__Init__TypeDef.html#a20cf696f18ca10c83c4a34124809cd06", null ],
    [ "stringDescriptors", "structUSBD__Init__TypeDef.html#add4d80f6bf26ff2fe67485ec12284a4c", null ]
];