var group__OTA__Bootload__Types =
[
    [ "EmberZclOtaBootloadClientServerInfo_t", "structEmberZclOtaBootloadClientServerInfo__t.html", [
      [ "address", "structEmberZclOtaBootloadClientServerInfo__t.html#a9fafc355729c0acd1f5c15db6e60cad4", null ],
      [ "endpointId", "structEmberZclOtaBootloadClientServerInfo__t.html#abea5d029589a38bd82aba15eb5544c69", null ],
      [ "name", "structEmberZclOtaBootloadClientServerInfo__t.html#ab454405cf3eb9dd7fb2e0b5f838f75ab", null ],
      [ "nameLength", "structEmberZclOtaBootloadClientServerInfo__t.html#a2ce0ec724e94102c885b186a9d584ee4", null ],
      [ "port", "structEmberZclOtaBootloadClientServerInfo__t.html#ae017cabad5c2b0a5dfb35d25ec1b86a8", null ],
      [ "scheme", "structEmberZclOtaBootloadClientServerInfo__t.html#ad252cb7dfda1ee58b8d3e1b5bb845aa2", null ],
      [ "uid", "structEmberZclOtaBootloadClientServerInfo__t.html#a75771797dbe32cabcf79d732c58cf3c8", null ]
    ] ],
    [ "EmberZclOtaBootloadHardwareVersionRange_t", "structEmberZclOtaBootloadHardwareVersionRange__t.html", [
      [ "maximum", "structEmberZclOtaBootloadHardwareVersionRange__t.html#acb354b154bdce0a306b754bea9191659", null ],
      [ "minimum", "structEmberZclOtaBootloadHardwareVersionRange__t.html#ab232045a8d32090fe0f4e689d2a9b6ad", null ]
    ] ],
    [ "EmberZclOtaBootloadFileSpec_t", "structEmberZclOtaBootloadFileSpec__t.html", [
      [ "manufacturerCode", "structEmberZclOtaBootloadFileSpec__t.html#a35c307dda76afa01188c51862c5691dc", null ],
      [ "type", "structEmberZclOtaBootloadFileSpec__t.html#adb16338ba467b604b3035db695148b8e", null ],
      [ "version", "structEmberZclOtaBootloadFileSpec__t.html#af23883aecd4aa0a3efa92b21e7ec4aeb", null ]
    ] ],
    [ "EmberZclOtaBootloadFileHeaderInfo_t", "structEmberZclOtaBootloadFileHeaderInfo__t.html", [
      [ "destination", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a76762a64577b7ce92c39ec1374eca9b2", null ],
      [ "fileSize", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a29d6e1c19e737491f0b880fbe3b5c8b2", null ],
      [ "hardwareVersionRange", "structEmberZclOtaBootloadFileHeaderInfo__t.html#aa0823132ada4328479bd940a06f08b61", null ],
      [ "headerSize", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a01352a55a56b79e641a4b2751a37e3fb", null ],
      [ "securityCredentialVersion", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a8fd9b915627901de40ef048f1be31805", null ],
      [ "spec", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a713da3ba094e0ec41a32889a23b57c11", null ],
      [ "stackVersion", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a90c581329d50c6dfac25f4313b17728c", null ],
      [ "string", "structEmberZclOtaBootloadFileHeaderInfo__t.html#ac55157385c7fcbc0b9f39750ff0c44aa", null ],
      [ "version", "structEmberZclOtaBootloadFileHeaderInfo__t.html#a84b2434a7a0edccd42006c377dc54ad9", null ]
    ] ],
    [ "EmberZclOtaBootloadStorageInfo_t", "structEmberZclOtaBootloadStorageInfo__t.html", [
      [ "fileCount", "structEmberZclOtaBootloadStorageInfo__t.html#a1d46c638c54f4247172c8df3a34f2872", null ],
      [ "maximumFileSize", "structEmberZclOtaBootloadStorageInfo__t.html#a290e185d6980514162abb129ebe84ee7", null ]
    ] ],
    [ "EmberZclOtaBootloadStorageFileInfo_t", "structEmberZclOtaBootloadStorageFileInfo__t.html", [
      [ "size", "structEmberZclOtaBootloadStorageFileInfo__t.html#a567dc41ab37d6439bef61f597c8d8094", null ]
    ] ],
    [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_MAGIC_NUMBER", "group__OTA__Bootload__Types.html#gaed573d92f2501b3e67cd0344e640acb5", null ],
    [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_MAGIC_NUMBER_SIZE", "group__OTA__Bootload__Types.html#ga1adde2cec3d527ba04c3c927916119b3", null ],
    [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_VERSION", "group__OTA__Bootload__Types.html#ga40dd14aa8ff053d0f98d7d7be8dad227", null ],
    [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_VERSION_NULL", "group__OTA__Bootload__Types.html#ga8d7c8b7a652712d9153818e9b419afec", null ],
    [ "EMBER_ZCL_OTA_BOOTLOAD_HARDWARE_VERSION_NULL", "group__OTA__Bootload__Types.html#ga137412345365d2b6172a90bc3bc2d173", null ],
    [ "EMBER_ZCL_OTA_BOOTLOAD_HEADER_MAX_SIZE", "group__OTA__Bootload__Types.html#ga3205c9aff37de2440563262f2228bf81", null ],
    [ "EMBER_ZCL_OTA_BOOTLOAD_HEADER_STRING_SIZE", "group__OTA__Bootload__Types.html#ga641cee567f5bb57f8b9daf297a08fd5a", null ],
    [ "EmberZclOtaBootloadFileVersion_t", "group__OTA__Bootload__Types.html#ga99ee950738bbe321a743fe3d92c8ba6d", null ],
    [ "EmberZclOtaBootloadHardwareVersion_t", "group__OTA__Bootload__Types.html#ga1003aa2615e8bb3a62bd25001cc9edae", null ],
    [ "EmberZclOtaBootloadStorageDeleteCallback", "group__OTA__Bootload__Types.html#ga5ee0f83b503ea9e66acb67b7bd73cdfb", null ],
    [ "EmberZclOtaBootloadFileHeaderFieldControl_t", "group__OTA__Bootload__Types.html#ga3ddba77e22e96cffcb3df9b633845019", [
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_HEADER_FIELD_CONTROL_DESTINATION", "group__OTA__Bootload__Types.html#gga3ddba77e22e96cffcb3df9b633845019ab3c348d8cf3927ea3fdff6794207703a", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_HEADER_FIELD_CONTROL_HARDWARE_VERSION", "group__OTA__Bootload__Types.html#gga3ddba77e22e96cffcb3df9b633845019ab1636afd03d030dc0b80f4b0118c70c2", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_HEADER_FIELD_CONTROL_NULL", "group__OTA__Bootload__Types.html#gga3ddba77e22e96cffcb3df9b633845019a0e691dcd9c037070b05d419284694c5d", null ]
    ] ],
    [ "EmberZclOtaBootloadFileStatus_t", "group__OTA__Bootload__Types.html#gaa698ec87b09742eab4955fcb91d6a5ed", [
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_STATUS_VALID", "group__OTA__Bootload__Types.html#ggaa698ec87b09742eab4955fcb91d6a5edaf519f35fad18643dff39e10973b5c0d6", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_STATUS_INVALID_MAGIC_NUMBER", "group__OTA__Bootload__Types.html#ggaa698ec87b09742eab4955fcb91d6a5eda85d0883f92cf091f0a80459f327e1bdd", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_STATUS_INVALID_VERSION", "group__OTA__Bootload__Types.html#ggaa698ec87b09742eab4955fcb91d6a5edac1b3c1afb30d7101afc16aa067820977", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_STATUS_INVALID_HEADER_SIZE", "group__OTA__Bootload__Types.html#ggaa698ec87b09742eab4955fcb91d6a5edae8a54842cdc5affbf8b34e10267f0b0e", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_STATUS_INVALID_STACK_VERSION", "group__OTA__Bootload__Types.html#ggaa698ec87b09742eab4955fcb91d6a5eda18c76090c6cb4565c61a5e9016a6e9bc", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_STATUS_INVALID_SECURITY_CREDENTIAL_VERSION", "group__OTA__Bootload__Types.html#ggaa698ec87b09742eab4955fcb91d6a5eda8227ccb98404c85e18757f6959c31db5", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_STATUS_NULL", "group__OTA__Bootload__Types.html#ggaa698ec87b09742eab4955fcb91d6a5eda485c56e19afe6a5a511e47cb390d0099", null ]
    ] ],
    [ "EmberZclOtaBootloadFileType_t", "group__OTA__Bootload__Types.html#ga9e4ea857fca38a7dc26bfd184f5fa4ac", [
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_TYPE_MANUFACTURER_SPECIFIC_MAXIMUM", "group__OTA__Bootload__Types.html#gga9e4ea857fca38a7dc26bfd184f5fa4aca1721c1e742c6f62d766962e29cefa3b0", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_TYPE_SECURITY_CREDENTIALS", "group__OTA__Bootload__Types.html#gga9e4ea857fca38a7dc26bfd184f5fa4aca6d6a875ac777c92d25ddf79f95600865", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_TYPE_CONFIGURATION", "group__OTA__Bootload__Types.html#gga9e4ea857fca38a7dc26bfd184f5fa4acaea13b812ea2f64514b22fd6379ea07d4", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_TYPE_LOG", "group__OTA__Bootload__Types.html#gga9e4ea857fca38a7dc26bfd184f5fa4aca3ff499c35b6f22b04c0e5e4e59a8e21f", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_TYPE_PICTURE", "group__OTA__Bootload__Types.html#gga9e4ea857fca38a7dc26bfd184f5fa4acac611b0f73b66bc4d0e11794e7630807c", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_FILE_TYPE_WILDCARD", "group__OTA__Bootload__Types.html#gga9e4ea857fca38a7dc26bfd184f5fa4acaab9ad42fa85120e7fa69048a93752a5d", null ]
    ] ],
    [ "EmberZclOtaBootloadSecurityCredentialVersion_t", "group__OTA__Bootload__Types.html#gab2cab791e4c285df4f4149e6bbdefd89", [
      [ "EMBER_ZCL_OTA_BOOTLOAD_SECURITY_CREDENTIAL_VERSION_IP", "group__OTA__Bootload__Types.html#ggab2cab791e4c285df4f4149e6bbdefd89aa0a8b841e5dcc40bbf8dc66b7c9684a4", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_SECURITY_CREDENTIAL_VERSION_NULL", "group__OTA__Bootload__Types.html#ggab2cab791e4c285df4f4149e6bbdefd89acc49738883a42a87969f6665529d5758", null ]
    ] ],
    [ "EmberZclOtaBootloadStackVersion_t", "group__OTA__Bootload__Types.html#ga085de36d847a6b07b86a75f1faa94889", [
      [ "EMBER_ZCL_OTA_BOOTLOAD_STACK_VERSION_IP", "group__OTA__Bootload__Types.html#gga085de36d847a6b07b86a75f1faa94889a3e2ee697e14e36c2387640f1a4fc8ce1", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_STACK_VERSION_NONE", "group__OTA__Bootload__Types.html#gga085de36d847a6b07b86a75f1faa94889ab178cd5aa79cf30d9b55a16c7787061d", null ]
    ] ],
    [ "EmberZclOtaBootloadStorageStatus_t", "group__OTA__Bootload__Types.html#ga73b65c6c79c2edbead50aa78b22fcfc4", [
      [ "EMBER_ZCL_OTA_BOOTLOAD_STORAGE_STATUS_SUCCESS", "group__OTA__Bootload__Types.html#gga73b65c6c79c2edbead50aa78b22fcfc4afd597081e798b50fb91338816ad58cc1", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_STORAGE_STATUS_FAILED", "group__OTA__Bootload__Types.html#gga73b65c6c79c2edbead50aa78b22fcfc4aef7e42047cba59a988db0828ea2818a3", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_STORAGE_STATUS_OUT_OF_RANGE", "group__OTA__Bootload__Types.html#gga73b65c6c79c2edbead50aa78b22fcfc4a50cc407d5a1af09c1f47a2035c56f5ed", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_STORAGE_STATUS_INVALID_FILE", "group__OTA__Bootload__Types.html#gga73b65c6c79c2edbead50aa78b22fcfc4a5e70a41141ad06c544ba90ed00b55b54", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_STORAGE_STATUS_OUT_OF_SPACE", "group__OTA__Bootload__Types.html#gga73b65c6c79c2edbead50aa78b22fcfc4a124dfb047bb907dc2143f89ea51bdb3a", null ],
      [ "EMBER_ZCL_OTA_BOOTLOAD_STORAGE_STATUS_NULL", "group__OTA__Bootload__Types.html#gga73b65c6c79c2edbead50aa78b22fcfc4ac75feab36f473043908ab5ab4a441322", null ]
    ] ],
    [ "emberZclOtaBootloadFileSpecNull", "group__OTA__Bootload__Types.html#gabc7d22c6cac1b1413ad5c11f4843f7cc", null ]
];