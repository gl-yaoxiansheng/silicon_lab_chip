var group__ZCLIP__attributes =
[
    [ "EmberZclAttributeContext_t", "structEmberZclAttributeContext__t.html", [
      [ "attributeId", "structEmberZclAttributeContext__t.html#a3ddc64c7f55e4a95107dc3cfc076325f", null ],
      [ "clusterSpec", "structEmberZclAttributeContext__t.html#aa01279a01cce5bf16cb84bca58e415ce", null ],
      [ "code", "structEmberZclAttributeContext__t.html#adab49814894bb7d8f8aa80777adf78ab", null ],
      [ "endpointId", "structEmberZclAttributeContext__t.html#a871fdf3fd3b88155abd3f5c5b015b8be", null ],
      [ "groupId", "structEmberZclAttributeContext__t.html#a94fbe5ad412065db20f87f42ae775053", null ],
      [ "status", "structEmberZclAttributeContext__t.html#aae7ea9a19e3dd8efdb66c0994a03438e", null ]
    ] ],
    [ "EmberZclAttributeWriteData_t", "structEmberZclAttributeWriteData__t.html", [
      [ "attributeId", "structEmberZclAttributeWriteData__t.html#adc9dd862aaf1b4357c0e7c7be85aeaf6", null ],
      [ "buffer", "structEmberZclAttributeWriteData__t.html#aa092dd5f939d088bdef6b0e459d159b6", null ],
      [ "bufferLength", "structEmberZclAttributeWriteData__t.html#afa1ff8c99c9fd05f94b4191ffdb82e58", null ]
    ] ],
    [ "EMBER_ZCL_ATTRIBUTE_CLUSTER_REVISION", "group__ZCLIP__attributes.html#ga409f8b946b27561fde088187336b0b58", null ],
    [ "EMBER_ZCL_ATTRIBUTE_NULL", "group__ZCLIP__attributes.html#ga824741c3161b78396f52a37fa4f3c90f", null ],
    [ "EMBER_ZCL_ATTRIBUTE_REPORTING_STATUS", "group__ZCLIP__attributes.html#ga075a9bae83011e7c2f1ae63276df670c", null ],
    [ "EMBER_ZCL_CLUSTER_REVISION_NULL", "group__ZCLIP__attributes.html#ga5928988b59d0bc65970e6bb9c1916f81", null ],
    [ "EMBER_ZCL_CLUSTER_REVISION_PRE_ZCL6", "group__ZCLIP__attributes.html#ga3ef980778c0d9fc77eaafb17387a880c", null ],
    [ "EMBER_ZCL_CLUSTER_REVISION_ZCL6", "group__ZCLIP__attributes.html#gab9fcbdc46ae76187d2bd0c624676780d", null ],
    [ "EmberZclAttributeId_t", "group__ZCLIP__attributes.html#ga1906ddd517700729523e78159e204575", null ],
    [ "EmberZclClusterRevision_t", "group__ZCLIP__attributes.html#ga2273a5d899ae97865007ec43d866e25e", null ],
    [ "EmberZclReadAttributeResponseHandler", "group__ZCLIP__attributes.html#gac963dddbfc1e327964ae379654969693", null ],
    [ "EmberZclWriteAttributeResponseHandler", "group__ZCLIP__attributes.html#gaa31f09fae7147f13059847e05ec3cf64", null ],
    [ "emberZclExternalAttributeChanged", "group__ZCLIP__attributes.html#gacede8279e813aafcd000a0a69460bde8", null ],
    [ "emberZclReadAttribute", "group__ZCLIP__attributes.html#ga310f743b469f460f3ed88c317e7fb165", null ],
    [ "emberZclResetAttributes", "group__ZCLIP__attributes.html#gae417f5ffe65f489a4c77c92baa0cf2a1", null ],
    [ "emberZclSendAttributeRead", "group__ZCLIP__attributes.html#ga27130cd73a4697ff58854fe8e01e3540", null ],
    [ "emberZclSendAttributeWrite", "group__ZCLIP__attributes.html#gadec8980842ce2f20de30f8c0a414f8e9", null ],
    [ "emberZclWriteAttribute", "group__ZCLIP__attributes.html#ga11da29570dbcaa536fba79e01d8e59fa", null ]
];