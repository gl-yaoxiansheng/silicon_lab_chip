var structUSB__Setup__TypeDef =
[
    [ "bmRequestType", "structUSB__Setup__TypeDef.html#a82332b9c048fff0d95801daff2dcd433", null ],
    [ "bRequest", "structUSB__Setup__TypeDef.html#ae5029ab72d017ffc7ecbe319250b673a", null ],
    [ "Direction", "structUSB__Setup__TypeDef.html#a913c1d707e48c96242f64002e8b60e6f", null ],
    [ "dw", "structUSB__Setup__TypeDef.html#ac69bbca56b763a1d1142776a97d977fe", null ],
    [ "Recipient", "structUSB__Setup__TypeDef.html#a70a468962d098b9d66fb2b564412bc71", null ],
    [ "Type", "structUSB__Setup__TypeDef.html#ad727d3a73acb94b1d0ba37542a21959b", null ],
    [ "wIndex", "structUSB__Setup__TypeDef.html#ae906e4bb150f8c859e7bc945bec15761", null ],
    [ "wLength", "structUSB__Setup__TypeDef.html#a144a954da389673e95ee9fbdae12d347", null ],
    [ "wValue", "structUSB__Setup__TypeDef.html#a668bf675fd2d40afb995d23e6e03a7d3", null ]
];