/**
 * @file network-diagnostics.h
 * @brief Diagnostic Functionality Over CoAP API
 *
 * <!--Copyright 2016 Silicon Laboratories, Inc.                         *80*-->
 */

#ifndef NETWORK_DIAGNOSTICS_H
#define NETWORK_DIAGNOSTICS_H

typedef enum {
  DIAGNOSTIC_MAC_EXTENDED_ADDRESS        = 0,  // Same format and semantics as MAC Extended Address TLV
  DIAGNOSTIC_ADDRESS_16                  = 1,  // Same format and semantics as Address16 TLV
  DIAGNOSTIC_MODE                        = 2,  // Same format and semantics as Mode TLV
  DIAGNOSTIC_TIMEOUT                     = 3,  // For SEDs, indicates the maximum polling time period.
                                               // For non-SEDs, ignored. Same format and semantics
  // as Timeout TLV
  DIAGNOSTIC_CONNECTIVITY                = 4,  // Same format and semantics as the Connectivity TLV
  DIAGNOSTIC_ROUTING_TABLE               = 5,  // Same format and semantics as the Route64 TLV
  DIAGNOSTIC_LEADER_DATA                 = 6,  // Same format and semantics as the Leader Data TLV
  DIAGNOSTIC_NETWORK_DATA                = 7,  // Same format and semantics as the Network Data TLV
  DIAGNOSTIC_IPV6_ADDRESS_LIST           = 8,  // List of all link-local and higher scoped IPv6 (unicast)
  // addresses registered by the Thread device on this
  // Thread interface.
  DIAGNOSTIC_MAC_COUNTERS                = 9,  // Packet/event counters for 802.15.4 interface.
  DIAGNOSTIC_UNUSED_10                   = 10, // Unused
  DIAGNOSTIC_UNUSED_11                   = 11, // Unused
  DIAGNOSTIC_UNUSED_12                   = 12, // Unused
  DIAGNOSTIC_UNUSED_13                   = 13, // Unused
  DIAGNOSTIC_BATTERY_LEVEL               = 14, // Indication of remaining battery energy
  DIAGNOSTIC_VOLTAGE                     = 15, // Indication of the current supply voltage
  DIAGNOSTIC_CHILD_TABLE                 = 16, // List of all children of a Router
  DIAGNOSTIC_CHANNEL_PAGES               = 17, // List of supported frequency bands
  DIAGNOSTIC_TYPE_LIST                   = 18, // List of type identifiers used to request or reset
  // multiple diagnostic values
  DIAGNOSTIC_MAX_CHILD_TIMEOUT           = 19, // value of the longest MLE Timeout TLV a router has
} EmberDiagnosticValue;

// The following TLVs have been separated from Diagnostic TLVs
// as they belong to the Mac Counters TLV list
typedef enum {
  DIAGNOSTIC_PACKETS_SENT                = 9,  // 2-btye value
  DIAGNOSTIC_PACKETS_RECEIVED            = 10, // 2-btye value
  DIAGNOSTIC_PACKETS_DROPPED_ON_TRANSMIT = 11, // 2-btye value
  DIAGNOSTIC_PACKETS_DROPPED_ON_RECEIVE  = 12, // 2-btye value
  DIAGNOSTIC_SECURITY_ERRORS             = 13, // 2-btye value
  DIAGNOSTIC_NUMBER_OF_RETRIES           = 14, // 2-btye value
} MacCountersValue;

typedef struct {
  uint32_t tlvMask;

  // EUI64, 8 bytes
  const uint8_t *macExtendedAddress;

  // 2-byte address
  uint16_t address16;

  uint8_t mode;
  uint16_t timeout;

  // connectivity TLV (MLE TLV Type 15)
  // points to the length byte
  const uint8_t *connectivity;

  // Routing Table, Router64 TLV (MLE Type 9)
  // points to the length byte
  const uint8_t *routingTable;

  // Leader Data TLV, same as TLV Type 11
  // points to the length byte
  const uint8_t *leaderData;

  // Network Data TLV, same as TLV Type 12
  // points to the length byte
  const uint8_t *networkData;

  // IPv6 Address List, points to the length byte
  const uint8_t *ipv6AddressList;

  const uint8_t *macCounters;
  uint8_t batteryLevel;
  uint16_t voltage;

  // Child Table, points to the length byte
  const uint8_t *childTable;

  // Channel Pages TLV, point to the length byte
  const uint8_t *channelPages;
} EmberDiagnosticData;

typedef struct {
  uint16_t packetsSent;
  uint16_t packetsReceived;
  uint16_t packetsDroppedOnTransmit;
  uint16_t packetsDroppedOnReceive;
  uint16_t securityErrors;
  uint16_t numberOfRetries;
} MacCountersData;

#define emberDiagnosticDataHasTlv(data, tlv) \
  (((data)->tlvMask & BIT(tlv)) == BIT(tlv))

void emberSendDiagnostic(const EmberIpv6Address *destination,
                         const uint8_t *requestedTlvs,
                         uint8_t length,
                         const uint8_t *uri);
/**
 * @brief  This function sends a network diagnostics query. See emberDiagnosticAnswerHandler() for callback.
 *
 * @param destination   The destination, which may be unicast or multicast.
 *
 * @param requestedTlvs   An array of requested TLVs.
 *
 * @param length   The length of requestedTlvs.
 */
void emberSendDiagnosticQuery(const EmberIpv6Address *destination,
                              const uint8_t *requestedTlvs,
                              uint8_t length);

/**
 * @brief This function sends a network diagnostics get. See emberDiagnosticAnswerHandler() for callback.
 *
 * @param destination   The destination, which must be unicast.
 *
 * @param requestedTlvs   An array of requested TLVs.
 *
 * @param length   The length of requestedTlvs.
 */
void emberSendDiagnosticGet(const EmberIpv6Address *destination,
                            const uint8_t *requestedTlvs,
                            uint8_t length);

/**
 * @brief This function sends a network diagnostics reset. See emberDiagnosticAnswerHandler() for callback.
 *
 * @param destination   The destination, which may be unicast or multicast.
 *
 * @param requestedTlvs   An array of TLVs marked for reset.
 *
 * @param length   The length of requestedTlvs.
 */
void emberSendDiagnosticReset(const EmberIpv6Address *destination,
                              const uint8_t *requestedTlvs,
                              uint8_t length);

/**
 * @brief Application callback for emberSendDiagnosticQuery() and emberSendDiagnosticGet().
 *
 * @param status   Status of the query result.
 *
 * @param remoteAddress   The remote address that sent the answer.
 *
 * @param payload   The returned payload.
 *
 * @param payloadLength The returned payload length.
 */
void emberDiagnosticAnswerHandler(EmberStatus status,
                                  const EmberIpv6Address *remoteAddress,
                                  const uint8_t *payload,
                                  uint16_t payloadLength);

/**
 * @brief This is a helper function for parsing the network diagnostic reponse payload.
 *
 * @param data   Pointer to the location were the parsed contents of the
 *				 diagnostic reponse payload will be stored.
 *
 * @param payload.  The diagnostic reponse payload to be parsed.
 *
 * @param payloadLength   The diagnostic reponse payload length.
 */
bool emberParseDiagnosticData(EmberDiagnosticData *data,
                              const uint8_t *payload,
                              uint16_t length);

/**
 * @brief  Application callback for the stack to obtain the battery level
 * to use in a response to a network diagnostic request.  From the Thread spec:
 * "The battery level is an 8-bit unsigned integer that indicates remaining
 * battery energy in the Thread device as an integer percentage value 0-100
 * (0x00-0x64)."
 *
 * @param batteryLevel  The battery level value to return to the stack.
 *
 * This handler should return false if the battery level is not measured,
 * unknown, or the device does not operate on battery power.  If a valid
 * battery level is being supplied, it must return true.
 *
 * The application must define EMBER_APPLICATION_HAS_BATTERY_LEVEL_HANDLER
 * in its CONFIGURATION_HEADER to use this.
 *
 * This is an SOC-only API.
 */
bool emberBatteryLevelHandler(uint8_t *batteryLevel);

/**
 * @brief Used to set the following TLVs returned by MGMT_GET requests:
 *
 * COMMISSION_VENDOR_NAME_TLV          (max length 32)
 * COMMISSION_VENDOR_MODEL_TLV         (max length 32)
 * COMMISSION_VENDOR_SW_VERSION_TLV    (max length 16)
 * COMMISSION_VENDOR_DATA_TLV          (max length 64)
 * COMMISSION_VENDOR_STACK_VERSION_TLV (length 6)
 *
 * @param tlvs  The TLVs to install. Any previously set TLVs will be replaced.
 * Only TLVs with the above types, and no duplicates are permitted.  The
 * max length of each TLV is enforced. Note that the stack version TLV must
 * have a length of 6.
 *
 * @param totalLength  The total length of the TLVs in the previous argument.
 * Must match the sum of the lengths of the supplied TLVs.
 *
 * Note these TLVs are not stored across reboots.
 */
void emberSetVendorTlvs(const uint8_t *tlvs, uint16_t totalLength);

/** @brief  Application callback for emberSetVendorTlvs().
 *
 * @param status EMBER_SUCCESS if the supplied TLVs were valid and successfully
 * installed, EMBER_BAD_ARGUMENT otherwise. See ::emberSetVendorTlvs for validity
 * requirements.
 *
 * @param length The number of bytes in the supplied network data.
 */
void emberSetVendorTlvsReturn(EmberStatus status, uint16_t length);

/** @brief This function sends a Thread MGMT_GET request.
 *
 * @param requestedTlvs  An array of requested TLVS. The supported TLVs are:
 * - COMMISSION_PROVISIONING_URL_TLV
 * - COMMISSION_VENDOR_NAME_TLV
 * - COMMISSION_VENDOR_MODEL_TLV
 * - COMMISSION_SW_VERSION_TLV
 * - COMMISSION_VENDOR_DATA_TLV
 * - COMMISSION_VENDOR_STACK_VERSION_TLV
 *
 * @param length  The length of requestedTlvs.
 */
EmberStatus emberSendManagementGetRequest(const EmberIpv6Address *destination,
                                          const uint8_t *requestedTlvs,
                                          uint8_t length);

/** @brief  The response to a management get request previous sent via
 * emberSendManagementGetRequest().
 */
void emberManagementGetResponseHandler(EmberCoapStatus status,
                                       EmberCoapCode code,
                                       EmberCoapReadOptions *options,
                                       uint8_t *payload,
                                       uint16_t payloadLength,
                                       EmberCoapResponseInfo *info);

typedef struct {
  uint32_t tlvMask;

  // COMMISSION_PROVISIONING_URL_TLV
  // points to the length byte
  const uint8_t *provisioningUrl;

  // COMMISSION_VENDOR_NAME_TLV
  // points to the length byte
  const uint8_t *vendorName;

  // COMMISSION_VENDOR_MODEL_TLV
  // points to the length byte
  const uint8_t *vendorModel;

  // COMMISSION_SW_VERSION_TLV
  // points to the length byte
  const uint8_t *swVersion;

  // COMMISSION_VENDOR_DATA_TLV
  // points to the length byte
  const uint8_t *vendorData;

  // COMMISSION_VENDOR_STACK_VERSION_TLV
  // points to the length byte
  const uint8_t *vendorStackVersion;
} EmberCommissionData;

// 0x20 is the TLV (PROVISIONING_URL) value of the first TLV supported with MGMT_GET
#define emberCommissionDataHasTlv(data, tlv) \
  (((data)->tlvMask & BIT((tlv) - 0x20)) == BIT((tlv) - 0x20))

/**
 * @brief This is a helper function for parsing the MGMT_GET reponse payload.
 *
 * @param data   Pointer to the location were the parsed contents of the
 *				 MGMT_GET reponse payload will be stored.
 *
 * @param payload.  The MGMT_GET reponse payload to be parsed.
 *
 * @param payloadLength   The MGMT_GET reponse payload length.
 */
bool emberParseManagementGetData(EmberCommissionData *data,
                                 const uint8_t *payload,
                                 uint16_t length);

#endif // NETWORK_DIAGNOSTICS_H
