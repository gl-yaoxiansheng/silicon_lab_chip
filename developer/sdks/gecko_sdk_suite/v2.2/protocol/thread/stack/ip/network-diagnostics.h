/*
 * File: network-diagnostics.h
 * Description: Thread Diagnostic Functionality
 *
 * Copyright 2016 by Silicon Laboratories. All rights reserved.             *80*
 */

#ifndef SLTHREAD_NETWORK_DIAGNOSTICS_H
#define SLTHREAD_NETWORK_DIAGNOSTICS_H

#define DIAGNOSTIC_GET_URI    "d/dg"
#define DIAGNOSTIC_RESET_URI  "d/dr"
#define DIAGNOSTIC_QUERY_URI  "d/dq"
#define DIAGNOSTIC_ANSWER_URI "d/da"

bool emHandleThreadDiagnosticMessage(const uint8_t *uri,
                                     const uint8_t *payload,
                                     uint16_t payloadLength,
                                     const EmberCoapRequestInfo *info,
                                     Buffer header);

#endif // SLTHREAD_NETWORK_DIAGNOSTICS_H
