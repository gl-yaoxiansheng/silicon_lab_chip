/**
 * file: network-data-tlv.c
 * Utilities for reading and writing Thread Network Data TLVs.
 *
 * This file contains utility functions for reading and writing Thread
 * Network Data TLVs.
 *
 * Unlike most Thread API functions these return their results directly
 * to the caller.  They have no internal state and can be called from
 * any thread, so there are no emApi... versions.
 */

#include PLATFORM_HEADER
#include "include/ember.h"

// ignores 'type's stable flag
uint8_t *emberFindNetworkDataTlv(uint8_t type,
                                 const uint8_t *start,
                                 const uint8_t *end)
{
  type = emberNetworkDataBaseType(type);
  for (; start < end; start = emberNetworkDataNextTlv(start)) {
    if (type == emberNetworkDataBaseType(emberNetworkDataTlvType(start))) {
      return (uint8_t *) start;
    }
  }
  return NULL;
}

uint8_t *emberNetworkDataNextTlv(const uint8_t *tlv)
{
  return (uint8_t *) (tlv + 2 + emberNetworkDataTlvSize(tlv));
}

static uint8_t *addPrefix(uint8_t *finger,
                          bool isStable,
                          const uint8_t *prefix,
                          const uint8_t prefixLengthInBits,
                          uint8_t domainId)
{
  *finger++ = (isStable
               ? EMBER_NETWORK_DATA_PREFIX_STABLE
               : EMBER_NETWORK_DATA_PREFIX_TEMP);
  finger++;     // skip over length
  *finger++ = domainId;
  *finger++ = prefixLengthInBits;
  MEMCOPY(finger, prefix, EMBER_BITS_TO_BYTES(prefixLengthInBits));
  finger += EMBER_BITS_TO_BYTES(prefixLengthInBits);
  return finger;
}

static void setTlvLength(uint8_t *tlv, const uint8_t *nextTlv)
{
  tlv[1] = nextTlv - (tlv + 2);
}

uint8_t *emberAddBorderRouterTlv(uint8_t *finger,
                                 bool isStable,
                                 EmberBorderRouterTlvFlag borderRouterFlags,
                                 const uint8_t *prefix,
                                 const uint8_t prefixLengthInBits,
                                 uint8_t domainId)
{
  uint8_t *prefixTlv = finger;
  finger = addPrefix(finger, isStable, prefix, prefixLengthInBits, domainId);
  *finger++ = (isStable
               ? EMBER_NETWORK_DATA_BORDER_ROUTER_STABLE
               : EMBER_NETWORK_DATA_BORDER_ROUTER_TEMP);
  *finger++ = 4;        // length = two-byte node ID + two bytes of flags
  *finger++ = 0;        // node ID, filled in by stack
  *finger++ = 0;
  *finger++ = HIGH_BYTE(borderRouterFlags);
  *finger++ = LOW_BYTE(borderRouterFlags);
  setTlvLength(prefixTlv, finger);
  return finger;
}

uint8_t *emberAddHasRouteTlv(uint8_t *finger,
                             bool isStable,
                             EmberDefaultRouteTlvFlag routeFlags,
                             const uint8_t *prefix,
                             const uint8_t prefixLengthInBits,
                             uint8_t domainId)
{
  uint8_t *prefixTlv = finger;
  finger = addPrefix(finger, isStable, prefix, prefixLengthInBits, domainId);
  *finger++ = (isStable
               ? EMBER_NETWORK_DATA_HAS_ROUTE_STABLE
               : EMBER_NETWORK_DATA_HAS_ROUTE_TEMP);
  *finger++ = 3;        // length = two-byte node ID + one byte of flags
  *finger++ = 0;        // node ID, filled in by stack
  *finger++ = 0;
  *finger++ = routeFlags;
  setTlvLength(prefixTlv, finger);
  return finger;
}

uint8_t *emberAddServerTlv(uint8_t *finger,
                           bool isStable,
                           uint32_t enterpriseNumber,
                           const uint8_t *serviceData,
                           const uint8_t serviceDataLength,
                           const uint8_t *serverData,
                           const uint8_t serverDataLength)
{
  *finger++ = (isStable
               ? EMBER_NETWORK_DATA_SERVICE_STABLE
               : EMBER_NETWORK_DATA_SERVICE_TEMP);
  if (enterpriseNumber == EMBER_THREAD_ENTERPRISE_NUMBER) {
    *finger++ = 2 + serviceDataLength + 4 + serverDataLength;
    *finger++ = EMBER_NETWORK_DATA_SERVICE_THREAD_FLAG;
  } else {
    *finger++ = 6 + serviceDataLength + 4 + serverDataLength;
    *finger++ = 0;      // no flags
    emberStoreHighLowInt32u(finger, enterpriseNumber);
    finger += 4;
  }
  *finger++ = serviceDataLength;
  MEMCOPY(finger, serviceData, serviceDataLength);
  finger += serviceDataLength;
  *finger++ = (isStable
               ? EMBER_NETWORK_DATA_SERVER_STABLE
               : EMBER_NETWORK_DATA_SERVER_TEMP);
  *finger++ = 2 + serverDataLength;
  *finger++ = 0;        // node ID, filled in by stack
  *finger++ = 0;
  MEMCOPY(finger, serverData, serverDataLength);
  finger += serverDataLength;
  return finger;
}
