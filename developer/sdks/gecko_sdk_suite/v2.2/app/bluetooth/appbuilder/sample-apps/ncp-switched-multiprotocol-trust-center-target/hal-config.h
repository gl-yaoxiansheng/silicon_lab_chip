#ifndef HAL_CONFIG_H
#define HAL_CONFIG_H

#include "board_features.h"
#include "hal-config-board.h"

#if defined(FEATURE_IOEXPANDER)
#include "hal-config-ioexp.h"
#endif

#if defined(FEATURE_FEM)
#include "hal-config-fem.h"
#endif

#ifdef BSP_CLK_LFXO_CTUNE
#undef BSP_CLK_LFXO_CTUNE
#endif
#define BSP_CLK_LFXO_CTUNE                            (32)

#define HAL_EXTFLASH_FREQUENCY                        (1000000)

#define HAL_PA_ENABLE                                 (1)
#define HAL_PA_RAMP                                   (10)
#define HAL_PA_2P4_LOWPOWER                           (0)
#define HAL_PA_POWER                                  (252)
#define HAL_PA_CURVE_HEADER                            "pa_curves_efr32.h"
#ifdef FEATURE_PA_HIGH_POWER
#define HAL_PA_VOLTAGE                                (3300)
#else // FEATURE_PA_HIGH_POWER
#define HAL_PA_VOLTAGE                                (1800)
#endif // FEATURE_PA_HIGH_POWER

#define HAL_PTI_ENABLE                                (1)
#define HAL_PTI_MODE                                  (HAL_PTI_MODE_UART)
#define HAL_PTI_BAUD_RATE                             (1600000)

#define HAL_UARTNCP_BAUD_RATE             (115200)
#define HAL_UARTNCP_FLOW_CONTROL          (HAL_USART_FLOW_CONTROL_HWUART)

#define HAL_USART0_ENABLE                 (1)
#define HAL_USART0_BAUD_RATE              (115200)
#define HAL_USART0_FLOW_CONTROL           (HAL_USART_FLOW_CONTROL_NONE)

#define HAL_VCOM_ENABLE                   (1)
#define HAL_I2CSENSOR_ENABLE              (0)
#define HAL_SPIDISPLAY_ENABLE             (0)

#define NCP_USART_STOPBITS    usartStopbits1  // Number of stop bits
#define NCP_USART_PARITY      usartNoParity   // Parity configuration
#define NCP_USART_OVS         usartOVS16      // Oversampling mode
#define NCP_USART_MVDIS       0               // Majority Vote Disable for 16x, 8x

#if (HAL_UARTNCP_FLOW_CONTROL == HAL_USART_FLOW_CONTROL_HWUART)
  #define NCP_USART_FLOW_CONTROL_TYPE    uartdrvFlowControlHwUart
#else
  #define NCP_USART_FLOW_CONTROL_TYPE    uartdrvFlowControlNone
#endif

// Define if NCP sleep functionality is requested
//#define NCP_DEEP_SLEEP_ENABLED

#if defined(NCP_DEEP_SLEEP_ENABLED)
  #define NCP_WAKEUP_PORT
  #define NCP_WAKEUP_PIN
  #define NCP_WAKEUP_POLARITY
#endif

// Define if NCP wakeup functionality is requested
//#define NCP_HOST_WAKEUP_ENABLED

#if defined(NCP_HOST_WAKEUP_ENABLED)
  #define NCP_HOST_WAKEUP_PORT
  #define NCP_HOST_WAKEUP_PIN
  #define NCP_HOST_WAKEUP_POLARITY
#endif

#endif
