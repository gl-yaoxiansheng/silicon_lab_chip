/*This is the command list supported.Add new command operation here*/
command_operation command_list[]={
    {"scan",func_scan},
    {"stop",func_stop},
    {"connect",func_connect},
    {"disconnect",func_disconnect},
    {"primary",func_primary},
    {"char-read-uuid",func_char_read_uuid},
    {"char-read-hnd",func_char_read_hnd},
    {"char-write-cmd",func_char_write_cmd},
    {"char-write-req",func_char_write_req},
    {"end",NULL}
};