/***********************************************************************************************//**
 * \file   app.c
 * \brief  Event handling and application code for Empty NCP Host application example
 ***************************************************************************************************
 * <b> (C) Copyright 2016 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/

/* standard library headers */
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

/* BG stack headers */
#include "bg_types.h"
#include "gecko_bglib.h"

/* Own header */
#include "app.h"
#include "gatt_db.h"
#include "gatt_event.h"
// App booted flag
static bool appBooted = false;
extern uint32 event_list[];        
/***********************************************************************************************//**
 *  \brief  Event handler function.
 *  \param[in] evt Event pointer.
 **************************************************************************************************/
void appHandleEvents(struct gecko_cmd_packet *evt)
{
  if (NULL == evt) {
    return;
  }
  uint32 p=BGLIB_MSG_ID(evt->header);
  // Do not handle any events until system is booted up properly.
  if ((p != gecko_evt_system_boot_id)&& !appBooted) 
  {
    
    printf("Wait for reset event\n");
    int i=0;
    int len=sizeof(event_list)/sizeof(event_list[0]);
    while(i<len)
    {
      if(p==event_list[i])
      {
        printf("i is %d, appBooted is %d\n",i,appBooted);
        break;
      }
      i++;
    }
    return;
  }

  /* Handle events */
  switch (p) {
    case gecko_evt_system_boot_id:

      appBooted = true;
      printf("System booted. waiting for commands... \n");
      break;
    case gecko_evt_gatt_server_attribute_value_id:
      break;
    case gecko_evt_le_gap_scan_response_id:
      print_MAC(evt);
      printf("\t");
      //print_addrtype(evt);
      //printf("\n");
      print_rssi(evt);
      printf("\t");
      print_name(evt);
      printf("\n");
      break;
    case gecko_evt_gatt_service_id:
      print_primary(evt);
      break;
    case gecko_evt_gatt_characteristic_id:
      print_char(evt);
      break;
    case gecko_evt_gatt_characteristic_value_id:
      print_char_value(evt);
      break;
    case gecko_evt_le_connection_opened_id:
      print_connected_device(evt);
      break;
    case gecko_evt_le_connection_closed_id:
      if(0x216==(evt->data.evt_le_connection_closed.reason))
        printf("handle %02x is disconnected\n",evt->data.evt_le_connection_closed.connection);
      else 
        printf("return error %x\n",evt->data.evt_le_connection_closed.reason);
      break;
    case gecko_evt_gatt_descriptor_value_id:
      print_desc(evt);
      break;
    case gecko_evt_gatt_procedure_completed_id:
      if(!(evt->data.evt_gatt_procedure_completed.result))
        printf("operate excute successfully\n");
      else
        printf("error code : %x\n",evt->data.evt_gatt_procedure_completed.result);
      break;
    default:
      break;
  }
}
void print_rssi(struct gecko_cmd_packet *evt)
{
  printf("RSSI: %d dBm ",evt->data.evt_le_gap_scan_response.rssi); 
  return; 
}	
void print_MAC(struct gecko_cmd_packet *evt)
{
  int i=5;
  printf("MAC: ");
  while(i>=0)
  {
    printf("%02x",evt->data.evt_le_gap_scan_response.address.addr[i]);
    if(i>0)
      printf(":");
    i--;  
  }
  return;
}
void print_name(struct gecko_cmd_packet *evt)
{
  char* name[30]={0};
  uint8 data_len=0;
  uint8* p;
  data_len=evt->data.evt_le_gap_scan_response.data.len;
  p=evt->data.evt_le_gap_scan_response.data.data;
  int i=0;
  while(i<data_len)
  { 
      if(*(p+1)==0x08||*(p+1)==0x09)
      {
        strncpy(name,(p+2),(*p-1)>30?30:(*p-1));
        printf("name: %s",name);
        return;
      }
      p+=(*p+1);
      i+=(*p+1);
  }
  printf("name: Unknown");
}
void print_addrtype(struct gecko_cmd_packet *evt)
{
  char address_type[5][40]=
  {
    "le_gap_address_type_public", 
    "le_gap_address_type_random", 
    "le_gap_address_type_public_identity", 
    "le_gap_address_type_random_identity", 
    "le_gap_address_type_bredr"
  };
  printf("address type: %s",address_type[evt->data.evt_le_gap_scan_response.address_type]);
  return;
}
void print_primary(struct gecko_cmd_packet *evt)
{
  int len=0;
  uint8 *handle;
  len=evt->data.evt_gatt_service.uuid.len;
  //printf("uuid len %x\n",len);
  if((len<=0)||(len>128))
    return;
  handle=(uint8*)&(evt->data.evt_gatt_service.service);
  printf("Connect handle:%02x\t",evt->data.evt_gatt_service.connection);
  printf("Start handle: %02x%02x\t",handle[3],handle[2]);
  printf("End handle: %02x%02x\t",handle[1],handle[0]);
  //printf("service handle: %x\n",evt->data.evt_gatt_service.service);
  printf("UUID: ");
  int i=len-1;
  while(i>=0)
  {
    printf("%02x",evt->data.evt_gatt_service.uuid.data[i]);
    i--;
  }
  printf("\n");
}
void print_char(struct gecko_cmd_packet *evt)
{
  /*print connect handle*/
  printf("connect handle: %02x\t",evt->data.evt_gatt_characteristic.connection);

  /*print characteristic UUID*/
  printf("UUID: ");
  int len=evt->data.evt_gatt_characteristic.uuid.len-1;
  while(len>=0)
  {
    printf("%02x",evt->data.evt_gatt_characteristic.uuid.data[len]);
    len--;
  }
  printf("\n");


  /*print characteristic handle*/
  uint8* ptr;
  ptr=(uint8*)&(evt->data.evt_gatt_characteristic.characteristic);
  printf("characteristic handle: %02x%02x\t",ptr[1],ptr[0]);

  /*print properties of characteristic*/
  printf("properties: ");
  print_properties(evt->data.evt_gatt_characteristic.properties);
  printf("\n\n");

}
void print_char_value(struct gecko_cmd_packet *evt)
{
  printf("connect handle: %02x\t",evt->data.evt_gatt_characteristic_value.connection);
  printf("characteristic handle: %04x\n",evt->data.evt_gatt_characteristic_value.characteristic);
  printf("characteristic value: ");
  int len=evt->data.evt_gatt_characteristic_value.value.len;
  int i=0;
  while(i<len)
  {
    printf("%02x ",evt->data.evt_gatt_characteristic_value.value.data[i]); 
    i++; 
  }  
  printf("\n");
}
void print_connected_device(struct gecko_cmd_packet *evt)
{
  printf("%02x:%02x:%02x:%02x:%02x:%02x is connected.\n",
  evt->data.evt_le_connection_opened.address.addr[5],evt->data.evt_le_connection_opened.address.addr[4],
  evt->data.evt_le_connection_opened.address.addr[3],evt->data.evt_le_connection_opened.address.addr[2],
  evt->data.evt_le_connection_opened.address.addr[1],evt->data.evt_le_connection_opened.address.addr[0]);
  printf("connect handle: %02x\n",evt->data.evt_le_connection_opened.connection);
}
void print_desc(struct gecko_cmd_packet *evt)
{
  uint8 *ptr;
  ptr=(uint8*)&(evt->data.evt_gatt_descriptor_value.descriptor);
  printf("connect handle: %02x\t",evt->data.evt_gatt_descriptor_value.connection);
  printf("desc handle: %02x%02x\n",ptr[1],ptr[0]);
  printf("descriptor value: ");
  int len=evt->data.evt_gatt_descriptor_value.value.len;
  int i=0;
  while(i<len)
  {
    printf("%02x ",evt->data.evt_gatt_descriptor_value.value.data[i]);
    i++;
  }
  printf("\n");
}
void print_properties(uint8 properties)
{
  char *properties_flag[8]={"B","R","NR","W","N","I","AW","EP"};
  int i=0;
  uint8 tmp=1;
  while(i<8)
  {
    if(properties&tmp)
      printf("%s ",properties_flag[i]);
    tmp<<=1;
    i++;
  }
}

