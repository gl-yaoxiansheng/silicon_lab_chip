#ifndef BLE_HOST_H
#define BLE_HOST_H


#define BLE_SOCKET "/home/yao/ble_socket.domain"
#define RECV_BUF    128
#define COMMAND_SIZE 20
#define MAX_PARAMETER_NUM 100

/*operation functions for different command*/
int func_scan(unsigned char*);
int func_stop(unsigned char*);
int func_connect(unsigned char*);
int func_disconnect(unsigned char*);
int func_primary(unsigned char*);
int func_char_read(unsigned char*);
int func_char_read_uuid(unsigned char*);
int func_char_read_hnd(unsigned char*);
int func_desc_read_hnd(unsigned char*);
int func_char_write_hnd(unsigned char*);
int func_char_write_hnd_norsp(unsigned char*);

typedef struct {
    char command[20];
    int (*func)(char *buf);
}command_operation;


int ProcessCommands(unsigned char*,int);
static void* BleStackEvent(void*);
int safe_write(int,void *,int);
int safe_read(int,void *,int);

#endif