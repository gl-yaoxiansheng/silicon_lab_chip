/*
 * This an example application that demonstrates Bluetooth connectivity
 * using BGLIB C function definitions. The example enables Bluetooth advertisements
 * and connections.
 *
 * Most of the functionality in BGAPI uses a request-response-event pattern
 * where the module responds to a command with a command response indicating
 * it has processed the request and then later sending an event indicating
 * the requested operation has been completed. */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include<sys/un.h>
#include<stddef.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<pthread.h>

#include "infrastructure.h"

/* BG stack headers */
#include "gecko_bglib.h"

/* hardware specific headers */
#include "uart.h"

/* application specific files */
#include "app.h"

#include "ble_host.h"
/***************************************************************************************************
 * Local Macros and Definitions
 **************************************************************************************************/

#define BLE_SOCKET "/root/ble_socket.domain"
#define RECV_BUF    128
static pthread_t ntid;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
BGLIB_DEFINE();

/** The default serial port to use for BGAPI communication. */
#if ((_WIN32 == 1) || (__CYGWIN__ == 1))
static char* default_uart_port = "COM0";
#elif __APPLE__ == 1
static char* default_uart_port = "/dev/tty.usbmodem14171";
#elif __linux == 1
static char* default_uart_port = "/dev/ttyACM0";
#else
static char* default_uart_port = "";
#endif

/** The default baud rate to use. */
static uint32_t default_baud_rate = 115200;

/** The serial port to use for BGAPI communication. */
static char* uart_port = NULL;

/** The baud rate to use. */
static uint32_t baud_rate = 0;

/** Usage string */
#define USAGE "Usage: %s <serial port> <baud rate> [flow control: 1(on, default) or 0(off)]\n\n"

/***************************************************************************************************
 * Static Function Declarations
 **************************************************************************************************/
command_operation command_list[]={
    {"scan",func_scan},
    {"stop",func_stop},
    {"connect",func_connect},
    {"disconnect",func_disconnect},
    {"primary",func_primary},
    {"char-read",func_char_read},
    {"char-read-uuid",func_char_read_uuid},
    {"char-read-hnd",func_char_read_hnd},
    {"desc-read-hnd",func_desc_read_hnd},
    {"char-write-hnd",func_char_write_hnd},
    {"char-write-hnd-norsp",func_char_write_hnd_norsp},
    {"exit",NULL},
    {"end",NULL}
};

static int appSerialPortInit(int argc, char* argv[], int32_t timeout);
static void on_message_send(uint32_t msg_len, uint8_t* msg_data);

/***************************************************************************************************
 * Public Function Definitions
 **************************************************************************************************/

/***********************************************************************************************//**
 *  \brief  The main program.
 *  \param[in] argc Argument count.
 *  \param[in] argv Buffer contaning Serial Port data.
 *  \return  0 on success, -1 on failure.
 **************************************************************************************************/

int main(int argc, char* argv[])
{
  int socket_fd;
  int accept_fd;
  int ret;
  int len;
  static char recv_buf[RECV_BUF];
  struct sockaddr_un server_addr;
  //struct gecko_cmd_packet* evt;

  /* Initialize BGLIB with our output function for sending messages. */
  BGLIB_INITIALIZE_NONBLOCK(on_message_send, uartRx, uartRxPeek);

  /* Initialise serial communication as non-blocking. */
  if (appSerialPortInit(argc, argv, 100) < 0) {
    printf("Non-blocking serial port init failure\n");
    exit(EXIT_FAILURE);
  }

  // Flush std output
  fflush(stdout);

  printf("Starting up...\nResetting NCP target...\n");

  /*Thread 2 check events from the BLE stack*/
  ret=pthread_create(&ntid,NULL,BleStackEvent,NULL);
  if(ret<0)
  {
    printf("create thread failed.\n");
    return -1;       
  }
  gecko_cmd_system_reset(0);
  
  /*Creat socket*/
  socket_fd=socket(AF_UNIX,SOCK_STREAM,0);
  if(socket_fd<0)
  {
      printf("creat socket failed.\n");
      return -1;
  }

  /*set server addr parameter*/
  server_addr.sun_family=AF_UNIX;
  strcpy(server_addr.sun_path,BLE_SOCKET);
  len=offsetof(struct sockaddr_un, sun_path) + strlen(server_addr.sun_path);
  unlink(BLE_SOCKET);//in case it has been exsited.

  /*Bind the server addr*/
  ret=bind(socket_fd,(struct sockaddr*)&server_addr,len);
  if(ret<0)
  {
      printf("bind failed.\n");
      unlink(BLE_SOCKET);
      return -1;
  }

  /*Listen socket_fd*/
  ret=listen(socket_fd,1);
  if(ret<0)
  {
      printf("listen failed.\n");
      close(socket_fd);
      unlink(BLE_SOCKET);
      return -1;
  }
  while(1)
  {
    memset(recv_buf,0,RECV_BUF);
    accept_fd=accept(socket_fd,NULL,NULL);
    //printf("recieve new socket\n");
    if(0==safe_read(accept_fd,recv_buf,RECV_BUF))
    {
        close(accept_fd);
        accept_fd=0;
        continue;            
    }
    if(-1==ProcessCommands(recv_buf,RECV_BUF))
    {
      close(accept_fd);
      close(socket_fd);
      unlink(BLE_SOCKET);
      return 0;
    }
    close(accept_fd);
    accept_fd=0;
    continue;
  }
	
  return 0;

}

/***************************************************************************************************
 * Static Function Definitions
 **************************************************************************************************/

/***********************************************************************************************//**
 *  \brief  Function called when a message needs to be written to the serial port.
 *  \param[in] msg_len Length of the message.
 *  \param[in] msg_data Message data, including the header.
 **************************************************************************************************/
static void on_message_send(uint32_t msg_len, uint8_t* msg_data)
{
  /** Variable for storing function return values. */
  int32_t ret;

  ret = uartTx(msg_len, msg_data);
  if (ret < 0) {
    printf("Failed to write to serial port %s, ret: %d, errno: %d\n", uart_port, ret, errno);
    exit(EXIT_FAILURE);
  }
}

/***********************************************************************************************//**
 *  \brief  Serial Port initialisation routine.
 *  \param[in] argc Argument count.
 *  \param[in] argv Buffer contaning Serial Port data.
 *  \return  0 on success, -1 on failure.
 **************************************************************************************************/
static int appSerialPortInit(int argc, char* argv[], int32_t timeout)
{
  uint32_t flowcontrol = 1;

  /**
   * Handle the command-line arguments.
   */
  baud_rate = default_baud_rate;
  switch (argc) {
    case 4:
      flowcontrol = atoi(argv[3]);
    /** Falls through on purpose. */
    case 3:
      baud_rate = atoi(argv[2]);
      uart_port = argv[1];
    /** Falls through on purpose. */
    default:
      break;
  }
  if (!uart_port || !baud_rate || (flowcontrol > 1)) {
    printf(USAGE, argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Initialise the serial port with RTS/CTS enabled. */
  return uartOpen((int8_t*)uart_port, baud_rate, flowcontrol, timeout);
}

/*thread for the stack event*/
static void* BleStackEvent(void*arg)
{
  struct gecko_cmd_packet* evt;
   while (1) 
  {
    /* Check for stack event. gecko_wait_event() blocked, gecko_peek_event() not-blocked*/
    pthread_mutex_lock(&lock);
    evt = gecko_peek_event();    //do not use block func here,it will ocuppied the uart,Thread 0 can not get the uart.            
    /* Run application and event handler. */
    appHandleEvents(evt);
    pthread_mutex_unlock(&lock);
  }  
  return NULL;       
}

/*safe packing for the socket operation write*/
int safe_write(int socket_fd,void *buf,int len)
{
    int write_bytes;
    int bytes_left;
    char *ptr;

    ptr=buf;
    bytes_left=len;
    while(bytes_left>0)
    {
        write_bytes=write(socket_fd,ptr,len);
        if(write_bytes<=0)
         {       
                 if(errno==EINTR)
                         write_bytes=0;
                 else             
                         return -1;
         }
        bytes_left-=write_bytes;
        ptr+=write_bytes;
    }
    return 0;
}

/*safe packing for the socket operation read*/
int safe_read(int socket_fd,void *buf,int max_len)
{
    int read_bytes;
    int len=0;
    char *ptr;
    ptr=buf;
    while(1)
    {
        read_bytes=read(socket_fd,ptr,max_len);
        len+=read_bytes;
        if(read_bytes==0)
            break;
        else if(read_bytes<0)
        {
            if(errno==EINTR)
                read_bytes=0;
            else
                return -1;
        }
        else
            ptr+=read_bytes;
    }
    return len;
}
int ProcessCommands(unsigned char* buf,int buf_len)
{
  //printf("recieve command: %s",buf);
  char command[20];
  strncpy(command,buf,20);
  int i=0;
  while(1)
  {
    if(!strcmp(command,"exit"))
      {
        printf("shutdown the host\n");
        return -1;
      }
    if(!strcmp(command_list[i].command,"end"))
      break;
    if(!strcmp(command_list[i].command,command))
    {
      pthread_mutex_lock(&lock);
      command_list[i].func(buf);
      pthread_mutex_unlock(&lock);
      return 0;
    }
    i++;
  }
  printf("Invaild Command\n");
  return 0;
}
