#ifndef BLE_COMMAND_H
#define BLE_COMMAND_H


#define BLE_SOCKET "/root/ble_socket.domain"
#define BUFF_SIZE 128
#define COMMAND_SIZE 20
/*analyze result of input*/
enum{
    LEGAL_COMMAND=0,
    ILLEGAL_COMMAND,
    QUIT_INPUT
};
static char command_type[][20]={
    "stop",	           //stop the scan	
    "scan",            //ble scan
    "connect",         //connect remote ble device
    "disconnect",      // disconnect from remote device
    "primary",         //show all the services fo the remote ble device,need connect
    "char-read",       //read all the characteristic
    "char-read-uuid",  //read characteristics of the specified char UUID ,need connect
    "char-read-hnd",   //read characteristics of remote device by handle,need connect
    "desc-read-hnd",   //read the descriptor according to the characteristic handle
    "char-write-hnd",  //write characteristics by handle with response,need connect
    "char-write-hnd-norsp",   //request for write characteristics without response ,need connect
    "exit",             //shutdown the host
    "quit",             //quit command CLI
    "end"               //end flag of command list
};

int safe_write(int ,void*,int);
int safe_read(int,void*,int);
int analyze_input(char*,char*);
#endif
