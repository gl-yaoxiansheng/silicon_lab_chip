#include "gecko_bglib.h"

//extern pthread_mutex_t lock ;
struct gecko_cmd_packet* gecko_wait_message(void)//wait for event from system
{
  
 //pthread_mutex_lock(&lock);
  uint32_t msg_length;
  uint32_t header;
  uint8_t  *payload;
  struct gecko_cmd_packet *pck, *retVal = NULL;
  int      ret;
  //sync to header byte
  ret = bglib_input(1, (uint8_t*)&header);
  //pthread_mutex_unlock(&lock);
  if (ret < 0 || (header & 0x78) != gecko_dev_type_gecko) {
   	
  	 //pthread_mutex_unlock(&lock);
	 return 0;
  }
  //pthread_mutex_lock(&lock);
  ret = bglib_input(BGLIB_MSG_HEADER_LEN - 1, &((uint8_t*)&header)[1]);
  //pthread_mutex_unlock(&lock);
  if (ret < 0) {
    	
  	//pthread_mutex_unlock(&lock);
	return 0;
  }

  msg_length = BGLIB_MSG_LEN(header);

  if ((header & 0xf8) == (gecko_dev_type_gecko | gecko_msg_type_evt)) {
    //received event
    if ((gecko_queue_w + 1) % BGLIB_QUEUE_LEN == gecko_queue_r) {
     	
  	 //pthread_mutex_unlock(&lock);
	 return 0;      //NO ROOM IN QUEUE
    }
    pck = &gecko_queue[gecko_queue_w];
    gecko_queue_w = (gecko_queue_w + 1) % BGLIB_QUEUE_LEN;
  } else if ((header & 0xf8) == gecko_dev_type_gecko) {//response
    retVal = pck = gecko_rsp_msg;
  } else {
    //fail
	
    //pthread_mutex_unlock(&lock);
    return 0;
  }
  pck->header = header;
  payload = (uint8_t*)&pck->data.payload;
  /**
   * Read the payload data if required and store it after the header.
   */
  if (msg_length) {
    //pthread_mutex_lock(&lock);
    ret = bglib_input(msg_length, payload);
    //pthread_mutex_unlock(&lock);
    if (ret < 0) {
   	   
  	 //pthread_mutex_unlock(&lock);
	   return 0;
    }
  }

  // Using retVal avoid double handling of event msg types in outer function
	
  	 //pthread_mutex_unlock(&lock);
	  return retVal;
}

int gecko_event_pending(void)
{
  if (gecko_queue_w != gecko_queue_r) {//event is waiting in queue
    return 1;
  }

  //something in uart waiting to be read
  if (bglib_peek && bglib_peek()) {
    return 1;
  }

  return 0;
}

struct gecko_cmd_packet* gecko_get_event(int block)
{
  struct gecko_cmd_packet* p;

  while (1) {
    if (gecko_queue_w != gecko_queue_r) {
      p = &gecko_queue[gecko_queue_r];
      gecko_queue_r = (gecko_queue_r + 1) % BGLIB_QUEUE_LEN;
      return p;
    }
    //if not blocking and nothing in uart -> out
    if (!block && bglib_peek && bglib_peek() == 0) {
      return NULL;
    }

    //read more messages from device
    if ( (p = gecko_wait_message()) ) {
      return p;
    }
  }
}

struct gecko_cmd_packet* gecko_wait_event(void)
{
  return gecko_get_event(1);
}

struct gecko_cmd_packet* gecko_peek_event(void)
{
  return gecko_get_event(0);
}

struct gecko_cmd_packet* gecko_wait_response(void)
{
  struct gecko_cmd_packet* p;
  while (1) {
    p = gecko_wait_message();
    if (p && !(p->header & gecko_msg_type_evt)) {
      return p;
    }
  }
}

void gecko_handle_command(uint32_t hdr, void* data)
{
  //packet in gecko_cmd_msg is waiting for output
  //pthread_mutex_lock(&lock);
  bglib_output(BGLIB_MSG_HEADER_LEN + BGLIB_MSG_LEN(gecko_cmd_msg->header), (uint8_t*)gecko_cmd_msg);
  //pthread_mutex_unlock(&lock);
  gecko_wait_response();
}

void gecko_handle_command_noresponse(uint32_t hdr, void* data)
{
  //packet in gecko_cmd_msg is waiting for output
  //pthread_mutex_lock(&lock);
  bglib_output(BGLIB_MSG_HEADER_LEN + BGLIB_MSG_LEN(gecko_cmd_msg->header), (uint8_t*)gecko_cmd_msg);
  //pthread_mutex_unlock(&lock);
}
