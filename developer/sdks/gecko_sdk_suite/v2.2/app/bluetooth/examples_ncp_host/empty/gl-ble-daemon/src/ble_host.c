/*This file is the completions of the function defined in the ble_host.h*/
#include"ble_host.h"
#include "gecko_bglib.h"
#include <stdlib.h>

#define SCAN_MODE   1
#define SCAN_TIME   5

/*Now have no parameters*/
int func_scan(unsigned char* buf)
{
    struct gecko_cmd_packet* p;
    uint16 scan_interval=0x10;
    uint16 scan_window=0x10;
    uint8 active=1;
    gecko_cmd_le_gap_set_scan_parameters(scan_interval,scan_window,active);
    gecko_cmd_le_gap_discover(SCAN_MODE);
};
int func_stop(unsigned char* buf)
{
    gecko_cmd_le_gap_end_procedure();   
};
int func_connect(unsigned char* buf)
{
    uint16 scan_interval=0x10;
    uint16 scan_window=0x10;
    uint8 active=0;     //negetive scan
    bd_addr address;
    uint8 address_type;
    gecko_cmd_le_gap_set_scan_parameters(scan_interval,scan_window,active);
    int i=5;
    int j=0;
    while(i>=0)
    {
        address.addr[i]=buf[COMMAND_SIZE+j+1];
        i--;
        j++;
    }
    address_type=buf[COMMAND_SIZE+j+1];
    gecko_cmd_le_gap_open(address, address_type);
    printf("connecting...\n");
};
int func_disconnect(unsigned char* buf)
{
    uint8 connect_handle;
    connect_handle=buf[COMMAND_SIZE+1];
    gecko_cmd_le_connection_close(connect_handle);
};
int func_primary(unsigned char* buf)
{   
    uint8 connect_handle;
    connect_handle=buf[COMMAND_SIZE+1];
    gecko_cmd_gatt_discover_primary_services(connect_handle);
};

int func_char_read(unsigned char* buf)
{
    uint8 connect_handle;
    uint32 service_handle;
    uint8* ptr;
    ptr=(uint8*)&service_handle;
    connect_handle=buf[COMMAND_SIZE+1];
    int i=4,j=0;
    while(i>0)
    {
        ptr[j]=buf[COMMAND_SIZE+i+1];
        i--;
        j++;
    }
    gecko_cmd_gatt_discover_characteristics(connect_handle,service_handle); 
}

/*read character by the character UUID UUID_LEN defined 16 bits*/
int func_char_read_uuid(unsigned char* buf)
{
    /*uint8 connect_handle;
    uint32 service_handle;
    uint8 uuid_len;
    uint8 uuid_data[MAX_PARAMETER_NUM]={0};
    uint8* ptr;

    connect_handle=buf[COMMAND_SIZE+1];

    ptr=(uint8*)&service_handle;
    int i=4,j=0;
    while(i>0)
    {
        ptr[j]=buf[COMMAND_SIZE+i+1];
        i--;
        j++;
    }
    
    uuid_len=buf[COMMAND_SIZE]-5;
    i=0;
    while(i<uuid_len)
    {
        uuid_data[i]=buf[COMMAND_SIZE+6+i];
        i++;
    }
    gecko_cmd_gatt_read_characteristic_value_by_uuid(connect_handle, service_handle, uuid_len, uuid_data);*/
    
};
int func_char_read_hnd(unsigned char* buf)
{
    uint8 connect_handle;
    uint16 char_handle;
    uint8 *ptr;
    connect_handle=buf[COMMAND_SIZE+1];
    ptr=(uint8*)&char_handle;
    ptr[1]=buf[COMMAND_SIZE+2];
    ptr[0]=buf[COMMAND_SIZE+3];
    gecko_cmd_gatt_read_characteristic_value(connect_handle,char_handle);
};
int func_desc_read_hnd(unsigned char* buf)
{   
    uint8 connect_handle;
    uint16 desc_handle=0;
    uint8 ptr;

    connect_handle=buf[COMMAND_SIZE+1];
    desc_handle=buf[COMMAND_SIZE+2]*16+buf[COMMAND_SIZE+3]+1;
    gecko_cmd_gatt_read_descriptor_value(connect_handle,desc_handle);
};
int func_char_write_hnd(unsigned char* buf)
{
    uint8 connect_handle;
    uint16 char_handle;
    uint8 value_len;
    uint8 value[MAX_PARAMETER_NUM]={0};

    connect_handle=buf[COMMAND_SIZE+1];
    char_handle=buf[COMMAND_SIZE+2]*16+buf[COMMAND_SIZE+3];
    value_len=buf[COMMAND_SIZE]-3;
    //value=(uint8array*)malloc(sizeof(uint8array)+value_len);
    //memset(value,0,MAX_PARAMETER_NUM);
    
    //printf("value len is %u\n",value_len);
    uint8 i=0;
    while(i<value_len)
    {
        value[i]=buf[COMMAND_SIZE+4+i];
        i++;
    }
    gecko_cmd_gatt_write_characteristic_value(connect_handle,char_handle,value_len,value);
};
int func_char_write_hnd_norsp(unsigned char* buf)
{
    
};
