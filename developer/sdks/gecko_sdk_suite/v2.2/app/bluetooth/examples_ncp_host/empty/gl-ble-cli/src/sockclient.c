#include <unistd.h>
#include<stdio.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/un.h>
#include<stddef.h>
#include<errno.h>
#include"ble_command.h"
int is_Hex_char(char c)
{
    if((c<=0x39)&&(c>=0x30))//0-9
        return 0;
    if((c<=0x5a)&&(c>=0x41))//a-z
        return 0;
    if((c<=0x7a)&&(c>=0x61))//A-Z
        return 0;
    return 1;
}
int char_to_hex(char* p)
{
    char c=*p;
    if((c<=0x39)&&(c>=0x30))
        *p=*p-'0';
    if((c<=0x5a)&&(c>=0x41))
        *p=*p-'A'+10;
    if((c<=0x7a)&&(c>=0x61))
        *p=*p-'a'+10;
    return 0;
}
int check_command(char *s)
{
    int i=0;
    while(1)
    {
        if(!strcmp(s,"quit"))
            return QUIT_INPUT;
        if(!strcmp(command_type[i],"end"))
            break;
        if(!strncmp(s,command_type[i],COMMAND_SIZE))
            return LEGAL_COMMAND;
        
        i++;
    }
    return ILLEGAL_COMMAND;
}
int main()
{

    char user_input[BUFF_SIZE];
    int connect_fd;
    int ret;
    unsigned char send_buf[BUFF_SIZE];
    static struct sockaddr_un server_addr;
    int server_addr_len;
    
    /*set the address*/
    server_addr.sun_family=AF_UNIX;
    strcpy(server_addr.sun_path,BLE_SOCKET);
    server_addr_len=offsetof(struct sockaddr_un, sun_path) + strlen(server_addr.sun_path);

    ret=0;
    int i;
    while(1)
    {
        memset(user_input,0,BUFF_SIZE);
        memset(send_buf,0,BUFF_SIZE);
        printf("GL-BLE>> ");
	fflush(stdout);
        fgets(user_input,BUFF_SIZE,stdin);
        ret=analyze_input(user_input,send_buf);
        if(ILLEGAL_COMMAND==ret)
        {
            printf("invalid command\n");
            continue;
        }
        else if(QUIT_INPUT==ret)
        {
            goto end;
        }
        else
        {
            connect_fd=socket(AF_UNIX,SOCK_STREAM,0);
            if(connect_fd<0)
            {
                printf("creat socket failed.\n");
                return -1;
            }
            ret=connect(connect_fd,&server_addr,server_addr_len);
            if(ret<0)
            {
                printf("connect failed\n");
                close(connect_fd);
                return -1;
            }   
            safe_write(connect_fd,send_buf,BUFF_SIZE);
            close(connect_fd);
            connect_fd=0;
        }
    }
end:
    close(connect_fd);
    return 0;
}
int analyze_input(char* input,char* buf)
{
    char* ptr;
    int len;
    int ret;
    int i=0;

    ptr=input;
    len=0;
    memset(buf,0,BUFF_SIZE);
    
    /*skip the space or table*/
    while(((*ptr)<0x61)||((*ptr)>0x7a))  //command must been filled with lower-case letters
    {
        if(len>=(BUFF_SIZE-1))
            return ILLEGAL_COMMAND;
        ptr++;
        len++;
    }

    /*read the command letters*/
    while(((*ptr<=0x7a)&&(*ptr>=0x61))||*ptr==0x2d)
    {
        if((len>=BUFF_SIZE)||(i>=COMMAND_SIZE))
            break;
        *(buf+i)=*ptr;
        i++;
        ptr++;
        len++;
    }

    /*check for the command*/
    ret=check_command(buf);
    if(ret==QUIT_INPUT)
        return QUIT_INPUT;
    else if(ret==ILLEGAL_COMMAND)
        return ILLEGAL_COMMAND;
        
    /*read all the parameters,parameter are assigned byte by byte begin with buff[COMMANS_SIZE]*/
    int j=0;
    while(len<(BUFF_SIZE-1))
    {
        if(is_Hex_char(*ptr))
            {
                ptr++;
                len++;
                continue;
            }
        unsigned char  para=0;
        unsigned char p=*(ptr++);
        unsigned char q=*ptr;
        len++;
        if(is_Hex_char(q))
            return ILLEGAL_COMMAND;
        char_to_hex(&p);
        char_to_hex(&q);
        para=p*16+q;
        buf[COMMAND_SIZE+j+1]=para;
        j++;
        ptr++;
        len++;
    }
    buf[COMMAND_SIZE]=j;
    return LEGAL_COMMAND;
}
int safe_write(int socket_fd,void *buf,int len)
{
    int write_bytes;
    int bytes_left;
    char *ptr;

    ptr=buf;
    bytes_left=len;
    while(bytes_left>0)
    {
        write_bytes=write(socket_fd,ptr,len);
        if(write_bytes<=0)
         {       
                 if(errno==EINTR)
                         write_bytes=0;
                 else             
                         return -1;
         }
        bytes_left-=write_bytes;
        ptr+=write_bytes;
    }
    return 0;
}
int safe_read(int socket_fd,void *buf,int max_len)
{
    int read_bytes;
    int len=0;
    char *ptr;
    ptr=buf;
    while(1)
    {
        read_bytes=read(socket_fd,ptr,max_len);
        len+=read_bytes;
        if(read_bytes==0)
            break;
        else if(read_bytes<0)
        {
            if(errno==EINTR)
                read_bytes=0;
            else
                return -1;
        }
        else
            ptr+=read_bytes;
    }
    return len;
}
