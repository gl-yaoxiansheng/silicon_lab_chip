MicriumOS Blink example.

This example shows how to initialize and run MicriumOS on the kit. The 
application will start a single task which is blinking LED 0 at a regular 
interval. This examples is a good starting point for applications that want 
to use MicriumOS.

Board:  Silicon Labs EFM32GG_STK3700 Starter Kit
Device: EFM32GG990F1024
