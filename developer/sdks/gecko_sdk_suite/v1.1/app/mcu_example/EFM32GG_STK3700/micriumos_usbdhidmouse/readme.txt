MicriumOS USB device hid mouse example.

This example shows how to use the MicriumOS USB device stack with the
USB peripheral on the EFM32GG starter kit. This example will emulate
the behaviour of a USB mouse and move the mouse ponter up-left and
down-right periodicly when connected to a pc.

The output from the example application can be found by connecting a 
terminal to the VCOM port.

Board:  Silicon Labs EFM32GG_STK3700 Starter Kit
Device: EFM32GG990F1024
