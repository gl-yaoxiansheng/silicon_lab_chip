/**************************************************************************//**
* @file si72xx.x
* @brief Software driver for interfacing the si72xx family of i2c hall-effect sensors
* @version 5.2.2
******************************************************************************
* # License
* <b>Copyright 2017 Silicon Labs, Inc. http://www.silabs.com</b>
*******************************************************************************
*
* This file is licensed under the Silabs License Agreement. See the file
* "Silabs_License_Agreement.txt" for details. Before using this software for
* any purpose, you must agree to the terms of that agreement.
*
******************************************************************************/

#include "si72xx.h"
#include "i2cspm.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/

/** @cond DO_NOT_INCLUDE_WITH_DOXYGEN */

/** Default i2c address */
#define SI72XX_I2C_ADDR       0x30  /* Default i2c address */
#define OTP_BUSY_MASK         1
#define OTP_READ_EN_MASK      2
#define NUM_HALL_DEVICES      3
#define STOP_MASK             2
#define SLTIMEENA_MASK        1
#define SW_TAMPER_MASK        0xFC
#define SL_FAST_MASK          2
#define SLEEP_MASK            1
#define ONEBURST_MASK         4
/** @endcond */

/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/

/**************************************************************************//**
 * @brief
 *  Reads register from the Si72xx sensor.
 * @param[in] i2c
 *   The I2C peripheral to use (not used).
 * @param[in] addr
 *   The I2C address of the sensor.
 * @param[out] data
 *   The data read from the sensor.
 * @param[in] reg
 *   The register address to read from in the sensor.
 * @return
 *   Returns number of bytes read on success. Otherwise returns error codes
 *   based on the I2CDRV.
 *****************************************************************************/
uint32_t Si72xx_Read_Register(I2C_TypeDef *i2c, uint8_t addr, uint8_t reg, uint8_t *data)
{
  I2C_TransferSeq_TypeDef    seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t i2c_write_data[1];

  seq.addr  = addr;
  seq.flags = I2C_FLAG_WRITE_READ;
  /* Select register to start reading from */
  i2c_write_data[0] = reg;
  seq.buf[0].data = i2c_write_data;
  seq.buf[0].len  = 1;
  /* Select length of data to be read */
  seq.buf[1].data = data;
  seq.buf[1].len  = 1;

  ret = I2CSPM_Transfer(i2c, &seq);
  if (ret != i2cTransferDone) {
    *data = 0xff;
    return (uint32_t)ret;
  }
  return (uint32_t)0;
}

/**************************************************************************//**
 * @brief
 *  Writes register in the Si72xx sensor.
 * @param[in] i2c
 *   The I2C peripheral to use (not used).
 * @param[in] addr
 *   The I2C address of the sensor.
 * @param[in] data
 *   The data to write to the sensor.
 * @param[in] reg
 *   The register address to write to in the sensor.
 * @return
 *   Returns zero on success. Otherwise returns error codes
 *   based on the I2CDRV.
 *****************************************************************************/
uint32_t Si72xx_Write_Register(I2C_TypeDef *i2c, uint8_t addr, uint8_t reg, uint8_t data)
{
  I2C_TransferSeq_TypeDef    seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t i2c_write_data[2];
  uint8_t i2c_read_data[1];

  seq.addr  = addr;
  seq.flags = I2C_FLAG_WRITE;
  /* Select register and data to write */
  i2c_write_data[0] = reg;
  i2c_write_data[1] = data;
  seq.buf[0].data = i2c_write_data;
  seq.buf[0].len  = 2;
  seq.buf[1].data = i2c_read_data;
  seq.buf[1].len  = 0;

  ret = I2CSPM_Transfer(i2c, &seq);
  if (ret != i2cTransferDone) {
    return (uint32_t)ret;
  }
  return (uint32_t)0;
}

/**************************************************************************//**
 * @brief
 *  Wakes up the Si72xx sensor.
 * @param[in] i2c
 *   The I2C peripheral to use (not used).
 * @param[in] addr
 *   The I2C address of the sensor.
 * @return
 *   Returns zero on success. Otherwise returns error codes
 *   based on the I2CDRV.
 *****************************************************************************/
uint32_t Si72xx_WakeUp(I2C_TypeDef *i2c, uint8_t addr)
{
  I2C_TransferSeq_TypeDef    seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t i2c_write_data[1];
  uint8_t i2c_read_data[1];

  seq.addr  = addr;
  seq.flags = I2C_FLAG_WRITE;
  /* Select register and data to write */
  seq.buf[0].data = i2c_write_data;
  seq.buf[0].len  = 0;
  seq.buf[1].data = i2c_read_data;
  seq.buf[1].len  = 0;

  ret = I2CSPM_Transfer(i2c, &seq);
  if (ret != i2cTransferDone) {
    return (uint32_t)ret;
  }
  return (uint32_t)0;
}

/**************************************************************************
* @brief
*   Read out Si7210 Conversion Data - 15bits
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor
* @param[out] data
*        Mag-field conversion reading, signed 16-bit integer
**************************************************************************/
uint32_t Si72xx_Read_Data(I2C_TypeDef *i2c, uint8_t addr, int16_t *data)
{
  uint8_t read = 0;
  uint8_t flag = 0;
  uint32_t result = 0;
  result = Si72xx_Read_Register(i2c, addr, SI72XX_DSPSIGM, &read);
  flag = read >> 7;
  *data = (((uint16_t)read) & 0x7f) << 8;
  result |= Si72xx_Read_Register(i2c, addr, SI72XX_DSPSIGL, &read);
  *data |= read;
  *data = *data - 16384;
  if (flag == 0) {
    result = SI72XX_ERROR_NODATA;
  }
  return result;
}

/**************************************************************************
* @brief
* Puts Si7210 into Sleep(No-measurement) Mode
*   Needs Si72xx_WakeUp() to become responsive
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor.
**************************************************************************/
uint32_t Si72xx_Sleep(I2C_TypeDef *i2c, uint8_t addr)
{
  uint32_t result = 0;
  uint8_t read;
  result = Si72xx_Read_Register(i2c, addr, 0xC9, &read);
  read = (read & 0xFE);
  result |= Si72xx_Write_Register(i2c, addr, 0xC9, read);
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = ((read & 0xF8) | 0x01);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  return result;
}
/**************************************************************************
* @brief
* Puts Si7210 into Sleep w/ Measurement Mode: OUTPUT is updated 200msec
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor.
**************************************************************************/
uint32_t Si72xx_Sleep_sltimeena(I2C_TypeDef *i2c, uint8_t addr)
{
  uint8_t read;
  uint32_t result = 0;
  result = Si72xx_Read_Register(i2c, addr, 0xC9, &read);
  read = ((read & 0xFC) | 0x01);
  result |= Si72xx_Write_Register(i2c, addr, 0xC9, read);
  result = Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = (read & 0xF8);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  return result;
}

/**************************************************************************
* @brief
*   Perform burst-conversion using FIR, read mT-data, and then
*   put part into sltimeena-sleep mode w/ OUT updated (factory default).
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor
* @param[in] range200mT
*   range200mT=0x0 : full-scale equals 20mT
*   otherwise : full-scale equals 200mT
* @param[out] data
*        Mag-field conversion reading, signed 16-bit integer
*        mTdata(decimal) = data*(125/100)*(1/1000)
**************************************************************************/
uint32_t Si72xxEXP_mTData_Sltimeena(I2C_TypeDef *i2c, uint8_t addr, uint8_t range200mT, int16_t *data)
{
  uint8_t read;
  uint32_t result = 0;
  result = Si72xx_WakeUp(i2c, addr);
  result = Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = ((read & 0xF0) | 0x0A);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  /* FIR Avg 256 samples */
  result |= Si72xx_Write_Register(i2c, addr, 0xCD, 0xF0);
  if (range200mT) {
    result |= Si72xx_Set_200mT_Range(i2c, addr);
  }
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = ((read & 0xF0) | 0x0C);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  while ((read >> 7) && (result == 0)) {
    result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  }
  result |= Si72xx_Read_Data(i2c, addr, &*data);
  result |= Si72xx_Sleep_sltimeena(i2c, addr);
  return result;
}

/**************************************************************************
* @brief
*   Wake-up from Sleep, perform burst-conversion using FIR, read mT-data,
*   and then put part into sleep mode (no-measurement). Requires Wake-Up.
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor
* @param[in] range200mT
*   range200mT=0x0 : full-scale equals 20mT
*   otherwise : full-scale equals 200mT
* @param[out] data
*        Mag-field conversion reading, signed 16-bit integer
*        mTdata(decimal) = data*(125/100)*(1/1000)
**************************************************************************/
uint32_t Si72xxEXP_mTData_Sleep(I2C_TypeDef *i2c, uint8_t addr, uint8_t range200mT, int16_t *data)
{
  uint8_t read = 0;
  uint32_t result = 0;
  result = Si72xx_WakeUp(i2c, addr);
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = ((read & 0xF0) | 0x0A);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  /* FIR Avg 256 samples */
  result |= Si72xx_Write_Register(i2c, addr, 0xCD, 0xF0);
  if (range200mT) {
    result |= Si72xx_Set_200mT_Range(i2c, addr);
  }
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = ((read & 0xF0) | 0x0C);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  while ((read >> 7) && (result == 0)) {
    result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  }
  result |= Si72xx_Read_Data(i2c, addr, &*data);
  result |= Si72xx_Sleep(i2c, addr);
  return result;
}

/**************************************************************************
* @brief
*   Sensor is placed in Latch Output Configuration w/ 0.2mT hysteresis
*   Unipolar w/ center-point 0mT : Used for Quadrature Demo
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor
**************************************************************************/
uint32_t Si72xxEXP_Enter_QuadMode(I2C_TypeDef *i2c, uint8_t addr)
{
  uint8_t read;
  uint32_t result = 0;
  result = Si72xx_WakeUp(i2c, addr);
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = ((read & 0xF0) | 0x0A);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  /* 0xC6: OUT(hi) for low-field */
  result |= Si72xx_Write_Register(i2c, addr, 0xC6, 0xFF);
  result |= Si72xx_Write_Register(i2c, addr, 0xC7, 0x92);
  result |= Si72xx_Write_Register(i2c, addr, 0xCD, 0x44);
  result |= Si72xx_Write_Register(i2c, addr, 0xC9, 0xFD);
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = (read & 0xF8);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  return result;
}

/**************************************************************************
* @brief
*   Read out Si7210 Revision and ID
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor
* @param[out] id
*        Si7210 part ID
* @param[out] rev
*        Si7210 part Revision
**************************************************************************/
uint32_t Si72xx_Identify(I2C_TypeDef *i2c, uint8_t addr, uint8_t *id, uint8_t *rev)
{
  uint8_t read;
  uint32_t result = 0;
  result = Si72xx_WakeUp(i2c, addr);
  result |= Si72xx_Read_Register(i2c, addr, SI72XX_HREVID, &read);
  *rev = read & 0xF;
  *id = read >> 4;
  result |= Si72xx_Read_Register(i2c, addr, SI72XX_POWER_CTRL, &read);
  read = read & 0xF8;
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_POWER_CTRL, (read | STOP_MASK));
  return result;
}

/**************************************************************************
* @brief
*   Read OTP Data: Part must be awake for function to work
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor
* @param[in] otpAddr
*        The OTB Byte address of the coefficients
* @param[out] data
*        OTP data read out
**************************************************************************/
static uint32_t Si72xx_Read_OTP(I2C_TypeDef *i2c, uint8_t addr, uint8_t otpAddr, uint8_t *data)
{
  uint8_t read;
  uint32_t result = 0;
  result = Si72xx_Read_Register(i2c, addr, SI72XX_OTP_CTRL, &read);
  if (read & OTP_BUSY_MASK) {
    return SI72XX_ERROR_BUSY;
  }
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_OTP_ADDR, otpAddr);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_OTP_CTRL, OTP_READ_EN_MASK);
  result |= Si72xx_Read_Register(i2c, addr, SI72XX_OTP_DATA, &read);
  *data =  read;
  return result;
}

/**************************************************************************
* @brief
*   Change Mag-Field scale to 200mT.
*   If desired, must be performed after power-up or wake-up from sleep.
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor
**************************************************************************/
uint32_t Si72xx_Set_200mT_Range(I2C_TypeDef *i2c, uint8_t addr)
{
  uint8_t data;
  uint32_t result;
  uint8_t srcAddr = 0x27;
  result = Si72xx_Read_OTP(i2c, addr, srcAddr++, &data);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_A0, data);
  result |= Si72xx_Read_OTP(i2c, addr, srcAddr++, &data);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_A1, data);
  result |= Si72xx_Read_OTP(i2c, addr, srcAddr++, &data);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_A2, data);
  result |= Si72xx_Read_OTP(i2c, addr, srcAddr++, &data);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_A3, data);
  result |= Si72xx_Read_OTP(i2c, addr, srcAddr++, &data);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_A4, data);
  result |= Si72xx_Read_OTP(i2c, addr, srcAddr++, &data);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_A5, data);
  return result;
}

/**************************************************************************
* @brief
*   Reads Temperature conversion and compensates measurements from OTP
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor
* @param[out] temp
*        Temperature measurement in Celsius
**************************************************************************/
uint32_t Si72xx_ReadTempAndCompensate(I2C_TypeDef *i2c, uint8_t addr, int32_t *temp)
{
  uint8_t read;
  int16_t data;
  uint8_t flag;
  uint32_t result = 0;
  uint8_t caloff, calgain;
  int64_t t;

  result = Si72xx_WakeUp(i2c, addr);
  /* result |= Si72xx_Read_Register(i2c,addr,SI72XX_SLTIME,&read); */
  /* sltime_save[addr&0x3] = read; */

  result |= Si72xx_Write_Register(i2c, addr, SI72XX_CTRL4, 0x00);
  result |= Si72xx_Read_Register(i2c, addr, SI72XX_DSPSIGSEL, &read);
  read = ((read & 0xF8) | 0x01);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_DSPSIGSEL, read); /* Select temp conversion */
  result |= Si72xx_Read_Register(i2c, addr, SI72XX_POWER_CTRL, &read);
  read = (((read & (~STOP_MASK)) & ~(SLEEP_MASK)) | ONEBURST_MASK);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_POWER_CTRL, read);
  result |= Si72xx_Read_Register(i2c, addr, SI72XX_DSPSIGM, &read);
  flag = read >> 7;
  data = (((uint16_t)read) & 0x7f) << 8;
  result |= Si72xx_Read_Register(i2c, addr, SI72XX_DSPSIGL, &read);
  data |= read;
  data >>= 3;

  t = (((int64_t)data * (int64_t)data)) * -21 / 10000;  /* -2.1e-6 x^2 */
  t = t + ((1522 * data) / 10); /* 0.1522 x */
  t = t - 273000; /* - 273 */

  /* now do offset&gain, read OTP 0x1D, 0x1E */
  result |= Si72xx_Read_OTP(i2c, addr, 0x1D, &caloff);
  result |= Si72xx_Read_OTP(i2c, addr, 0x1E, &calgain);

  *temp = (int32_t)t;

  if (flag == 0) {
    result = SI72XX_ERROR_NODATA;
  }
  return result;
}

/**************************************************************************
* @brief
*   Perform burst-conversion(4samples), read mT-data, and then
*   put part into sltimeena-sleep mode where OUT is updated every 200msec.
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor
* @param[in] range200mT
*   range200mT=0x0 : full-scale equals 20mT
*   otherwise : full-scale equals 200mT
* @param[out] mTdata
*        Mag-field conversion reading, signed 32-bit integer
*        mTdata must be divided by 1000 to get decimal value in mT units
**************************************************************************/
uint32_t Si72xx_Read_mTData_Sltimeena(I2C_TypeDef *i2c, uint8_t addr, uint8_t range200mT, int32_t *mTdata)
{
  uint8_t read;
  int16_t data;
  uint32_t result = 0;
  result = Si72xx_WakeUp(i2c, addr);
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = ((read & 0xF0) | 0x0A);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  result |= Si72xx_Write_Register(i2c, addr, 0xCD, 0x44);
  if (range200mT) {
    result |= Si72xx_Set_200mT_Range(i2c, addr);
  }
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = ((read & 0xF0) | 0x0C);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  while ( (read >> 7) && (result == 0) ) {
    result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  }
  result |= Si72xx_Read_Data(i2c, addr, &data);
  /* To convert mTdata to decimal value, divide by 1000 */
  if (range200mT) {
    *mTdata = (data * 125 / 10);
  } else {
    *mTdata = (data * 125 / 100);
  }
  result |= Si72xx_Sleep_sltimeena(i2c, addr);
  return result;
}

/**************************************************************************
* @brief
*   Wake-up from Sleep, perform burst-conversion(4samples), read mT-data,
*   and then put part into sleep mode (no-measurement). Requires Wake-Up.
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor
* @param[in] range200mT
*   range200mT=0x0 : full-scale equals 20mT
*   otherwise : full-scale equals 200mT
* @param[out] mTdata
*        Mag-field conversion reading, signed 32-bit integer
*        mTdata must be divided by 1000 to get decimal value in mT units
**************************************************************************/
uint32_t Si72xx_Read_mTData_Sleep(I2C_TypeDef *i2c, uint8_t addr, uint8_t range200mT, int32_t *mTdata)
{
  uint8_t read;
  int16_t data;
  uint32_t result = 0;
  result = Si72xx_WakeUp(i2c, addr);
  result = Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = ((read & 0xF0) | 0x0A);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  result |= Si72xx_Write_Register(i2c, addr, 0xCD, 0x44);
  if (range200mT) {
    result |= Si72xx_Set_200mT_Range(i2c, addr);
  }
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = ((read & 0xF0) | 0x0C);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  while ( (read >> 7) && (result == 0) ) {
    result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  }
  result |= Si72xx_Read_Data(i2c, addr, &data);
  /* To convert mTdata to decimal value, divide by 1000 */
  if (range200mT) {
    *mTdata = (data * 125 / 10);
  } else {
    *mTdata = (data * 125 / 100);
  }
  result |= Si72xx_Sleep(i2c, addr);
  return result;
}

/**************************************************************************
* @brief
*   Puts sensor into continuous mode, conversions performed every 7usec
* @param[in] i2c
*   The I2C peripheral to use (not used).
* @param[in] addr
*   The I2C address of the sensor
**************************************************************************/
uint32_t Si72xx_Enter_ContMode(I2C_TypeDef *i2c, uint8_t addr)
{
  uint8_t read;
  uint32_t result = 0;
  result = Si72xx_WakeUp(i2c, addr);
  result |= Si72xx_Read_Register(i2c, addr, 0xC4, &read);
  read = ((read & 0xF0) | 0x0A);
  result |= Si72xx_Write_Register(i2c, addr, 0xC4, read);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_DSPSIGSEL, 4);
  result |= Si72xx_Write_Register(i2c, addr, 0xC6, 0x7F);
  result |= Si72xx_Write_Register(i2c, addr, 0xC7, 0x92);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_SLTIME, 0x00);
  result |= Si72xx_Write_Register(i2c, addr, 0xC9, 0xFE);
  result |= Si72xx_Read_Register(i2c, addr, SI72XX_POWER_CTRL, &read);
  read = (read & 0xF8);
  result |= Si72xx_Write_Register(i2c, addr, SI72XX_POWER_CTRL, read);
  return result;
}
