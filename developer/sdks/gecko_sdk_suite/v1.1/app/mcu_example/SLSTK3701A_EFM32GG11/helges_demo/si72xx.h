/**************************************************************************//**
* @file si72xx.h
* @brief Software driver for interfacing the si72xx family of i2c hall-effect sensors
* @version 5.2.2
******************************************************************************
* # License
* <b>Copyright 2017 Silicon Labs, Inc. http://www.silabs.com</b>
*******************************************************************************
*
* This file is licensed under the Silabs License Agreement. See the file
* "Silabs_License_Agreement.txt" for details. Before using this software for
* any purpose, you must agree to the terms of that agreement.
*
******************************************************************************/

#ifndef __SI72XX_H
#define __SI72XX_H

#include "em_device.h"

/***************************************************************************//**
 * @addtogroup Drivers
 * @{
 ******************************************************************************/

/***************************************************************************//**
 * @addtogroup Si7013
 * @{
 ******************************************************************************/

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/

/** I2C device address for Si72xx */
#define SI7200_ADDR_0      (0x30 << 1) /* PS I2C-Option */
#define SI7200_ADDR_1      (0x31 << 1) /* U2 silk on EXP */
#define SI7200_ADDR_2      (0x32 << 1) /* U1 silk on EXP */

/** I2C registers for Si72xx */
#define SI72XX_HREVID             0xC0
#define SI72XX_DSPSIGM            0xC1
#define SI72XX_DSPSIGL            0xC2
#define SI72XX_DSPSIGSEL          0xC3
#define SI72XX_POWER_CTRL         0xC4
#define SI72XX_ARAUTOINC          0xC5
#define SI72XX_CTRL1              0xC6
#define SI72XX_CTRL2              0xC7
#define SI72XX_SLTIME             0xC8
#define SI72XX_CTRL3              0xC9
#define SI72XX_A0                 0xCA
#define SI72XX_A1                 0xCB
#define SI72XX_A2                 0xCC
#define SI72XX_CTRL4              0xCD
#define SI72XX_A3                 0xCE
#define SI72XX_A4                 0xCF
#define SI72XX_A5                 0xD0
#define SI72XX_OTP_ADDR           0xE1
#define SI72XX_OTP_DATA           0xE2
#define SI72XX_OTP_CTRL           0xE3
#define SI72XX_TM_FG              0xE4

#define SI72XX_ERROR_BUSY         0xfe
#define SI72XX_ERROR_NODATA       0xfd

#define SI7210_20MT         0x00
#define SI7210_200MT        0x01

/*******************************************************************************
 *****************************   PROTOTYPES   **********************************
 ******************************************************************************/
uint32_t Si72xx_Write_Register(I2C_TypeDef *i2c, uint8_t addr, uint8_t reg, uint8_t data);
uint32_t Si72xx_Read_Register(I2C_TypeDef *i2c, uint8_t addr, uint8_t reg, uint8_t *data);
uint32_t Si72xx_WakeUp(I2C_TypeDef *i2c, uint8_t addr);
uint32_t Si72xx_Read_Data(I2C_TypeDef *i2c, uint8_t addr, int16_t *data);
uint32_t Si72xx_Sleep (I2C_TypeDef *i2c, uint8_t addr);
uint32_t Si72xx_Sleep_sltimeena (I2C_TypeDef *i2c, uint8_t addr);
uint32_t Si72xxEXP_mTData_Sltimeena (I2C_TypeDef *i2c, uint8_t addr, uint8_t range200mT, int16_t *data);
uint32_t Si72xxEXP_mTData_Sleep (I2C_TypeDef *i2c, uint8_t addr, uint8_t range200mT, int16_t *data);
uint32_t Si72xxEXP_Enter_QuadMode (I2C_TypeDef *i2c, uint8_t addr);
uint32_t Si72xx_Identify(I2C_TypeDef *i2c, uint8_t addr, uint8_t *id, uint8_t *rev);
uint32_t Si72xx_Set_200mT_Range (I2C_TypeDef *i2c, uint8_t addr);
uint32_t Si72xx_ReadTempAndCompensate (I2C_TypeDef *i2c, uint8_t addr, int32_t *temp);

uint32_t Si72xx_Read_mTData_Sltimeena (I2C_TypeDef *i2c, uint8_t addr, uint8_t range200mT, int32_t *mTdata);
uint32_t Si72xx_Read_mTData_Sleep (I2C_TypeDef *i2c, uint8_t addr, uint8_t range200mT, int32_t *mTdata);
uint32_t Si72xx_Enter_ContMode (I2C_TypeDef *i2c, uint8_t addr);

/** @} (end group Si7013) */
/** @} (end group Drivers) */
#endif /* __SI7013_H */
