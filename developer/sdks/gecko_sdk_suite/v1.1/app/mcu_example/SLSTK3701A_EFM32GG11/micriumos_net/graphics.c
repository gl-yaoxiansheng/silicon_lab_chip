/***************************************************************************//**
 * @brief Draws the graphics on the display
 * @version 5.2.2
 *******************************************************************************
 * # License
 * <b>Copyright 2015 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include "display.h"
#include "dmd.h"
#include "glib.h"
#include "em_device.h"
#include <rtos/kernel/include/os.h>
#include <rtos/net/include/net_if.h>
#include <rtos/net/include/net_ipv4.h>
#include <rtos/net/include/net_ipv6.h>
#include <rtos/common/include/rtos_utils.h>
#include <string.h>
#include <stdio.h>
#include <inttypes.h>

static GLIB_Context_t glibContext;

#define GLIB_FONT_WIDTH           (glibContext.font.fontWidth + glibContext.font.charSpacing)
#define GLIB_FONT_HEIGHT          (glibContext.font.fontHeight)
#define CENTER_X                  (glibContext.pDisplayGeometry->xSize / 2)
#define CENTER_Y                  (glibContext.pDisplayGeometry->ySize / 2)
#define MAX_X                     (glibContext.pDisplayGeometry->xSize - 1)
#define MAX_Y                     (glibContext.pDisplayGeometry->ySize - 1)
#define MIN_X                     0
#define MIN_Y                     0
#define MAX_STR_LEN               48

/*******************************************************************************
 **************************   LOCAL FUNCTIONS   ********************************
 ******************************************************************************/
static void GRAPHICS_DrawString(const char * s, int y);
static void GRAPHICS_DrawLinkStatus(void);
static void GRAPHICS_DrawIPv4Status(void);
static void GRAPHICS_DrawIPv6Status(void);
static void GRAPHICS_DrawTxRxStatus(void);
static void GRAPHICS_DrawTitle(void);


/***************************************************************************//**
 * @brief Initializes the glib and DMD.
 ******************************************************************************/
void GRAPHICS_Init(void)
{
  EMSTATUS status;

  // Initialize the DMD module for the DISPLAY device driver.
  status = DMD_init(0);
  if (DMD_OK != status) {
    while (1) {}
  }

  status = GLIB_contextInit(&glibContext);
  if (GLIB_OK != status) {
    while (1) {}
  }
}

/***************************************************************************//**
 * @brief Update the whole display with current status.
 ******************************************************************************/
void GRAPHICS_ShowStatus(void)
{
  GLIB_clear(&glibContext);
  GRAPHICS_DrawTitle();
  GRAPHICS_DrawLinkStatus();
  GRAPHICS_DrawIPv4Status();
  GRAPHICS_DrawIPv6Status();
  GRAPHICS_DrawTxRxStatus();
  DMD_updateDisplay();
}

/***************************************************************************//**
 * @brief Draw title
 ******************************************************************************/
static void GRAPHICS_DrawTitle(void)
{
  GLIB_setFont(&glibContext, (GLIB_Font_t *)&GLIB_FontNormal8x8);
  GRAPHICS_DrawString("Ethernet", 0);
  GLIB_setFont(&glibContext, (GLIB_Font_t *)&GLIB_FontNarrow6x8);
  GRAPHICS_DrawString("MicriumOS Network", 10);
}

/***************************************************************************//**
 * @brief Draw a string at a specific y coordinate
 ******************************************************************************/
static void GRAPHICS_DrawString(const char * s, int y)
{
  GLIB_drawString(&glibContext, s, strlen(s), 0, y, false);
}

/***************************************************************************//**
 * @brief Draw Link status, linke is either "up" or "down"
 ******************************************************************************/
static void GRAPHICS_DrawLinkStatus(void)
{
  RTOS_ERR err;
  NET_IF_LINK_STATE link;
  const char * msg;

  link = NetIF_LinkStateGet(1, &err);

  if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE)
  {
    msg = "Link error";
  }
  else if (link == NET_IF_LINK_UP)
  {
    msg = "Link up";
  }
  else
  {
    msg = "Link down";
  }
  GRAPHICS_DrawString(msg, 20);
}

/***************************************************************************//**
 * @brief Draw IPv4 address
 ******************************************************************************/
static void GRAPHICS_DrawIPv4Status(void)
{
  RTOS_ERR          err;
  NET_IPv4_ADDR     addrTable[4];
  NET_IP_ADDRS_QTY  addrTableSize = 4;
  CPU_BOOLEAN       ok;
  CPU_CHAR          addrString[NET_ASCII_LEN_MAX_ADDR_IPv4];

  ok = NetIPv4_GetAddrHost(1, addrTable, &addrTableSize, &err);

  if (!ok) {
    return;
  }

  if (addrTableSize > 0)
  {
    NetASCII_IPv4_to_Str(addrTable[0], addrString, DEF_NO, &err);
    APP_RTOS_ASSERT_CRITICAL((err.Code == RTOS_ERR_NONE), ;);
    GRAPHICS_DrawString(addrString, 30);
  }
}

/***************************************************************************//**
 * @brief Draw IPv6 addresses
 ******************************************************************************/
static void GRAPHICS_DrawIPv6Status(void)
{
  RTOS_ERR          err;
  NET_IPv6_ADDR     addrTable[4];
  NET_IP_ADDRS_QTY  addrTableSize = 4;
  CPU_BOOLEAN       ok;
  CPU_CHAR          addrString[NET_ASCII_LEN_MAX_ADDR_IPv6];

  ok = NetIPv6_GetAddrHost(1, addrTable, &addrTableSize, &err);

  if (!ok) {
    return;
  }

  int y = 40;
  for (int i = 0; i < addrTableSize; i++)
  {
    NetASCII_IPv6_to_Str(&addrTable[i], addrString, DEF_NO, DEF_NO, &err);
    APP_RTOS_ASSERT_CRITICAL((err.Code == RTOS_ERR_NONE), ;);

    /* IPv6 Address is too long to print on one line with narrow font */
    GLIB_drawString(&glibContext, &addrString[0], 20, 0, y, false);
    GLIB_drawString(&glibContext, &addrString[20], strlen(addrString) - 20, 0, y + 10, false);
    y += 20;
  }
}

/***************************************************************************//**
 * @brief Draw Tx/Rx statistics
 ******************************************************************************/
static void GRAPHICS_DrawTxRxStatus(void)
{
  char str[20] = {0};
  static uint32_t framesTx = 0;
  static uint32_t framesRx = 0;

  framesTx += ETH->FRAMESTXED64;
  framesTx += ETH->FRAMESTXED65;
  framesTx += ETH->FRAMESTXED128;
  framesTx += ETH->FRAMESTXED256;
  framesTx += ETH->FRAMESTXED512;
  framesTx += ETH->FRAMESTXED1024;
  framesTx += ETH->FRAMESTXED1519;

  framesRx += ETH->FRAMESRXED64;
  framesRx += ETH->FRAMESRXED65;
  framesRx += ETH->FRAMESRXED128;
  framesRx += ETH->FRAMESRXED256;
  framesRx += ETH->FRAMESRXED512;
  framesRx += ETH->FRAMESRXED1024;
  framesRx += ETH->FRAMESRXED1519;

  snprintf(str, sizeof(str), "frames tx: %"PRIu32, framesTx);
  GRAPHICS_DrawString(str, 110);
  snprintf(str, sizeof(str), "frames rx: %"PRIu32, framesRx);
  GRAPHICS_DrawString(str, 120);
}
