MicriumOS Network example.

This example shows how to use the MicriumOS network stack with the ETH 
peripheral on the EFM32GG11B starter kit. This example will initialize 
the RMII interface to the external PHY and setup a 100 Mbit connection.

The example will output messages on the VCOM port and it will show status 
on the memory lcd display on the kit. The display will show the current 
link status along with the current IPv4 address and the currently 
configured IPv6 addresses.

This example application may exceed the maximum current limit for CR2032 coincell batteries.

Board:  Silicon Labs SLSTK3701A_EFM32GG11 Starter Kit
Device: EFM32GG11B820F2048GL192
