/*
*********************************************************************************************************
*                                             EXAMPLE CODE
*********************************************************************************************************
* Licensing terms:
*   This file is provided as an example on how to use Micrium products. It has not necessarily been
*   tested under every possible condition and is only offered as a reference, without any guarantee.
*
*   Please feel free to use any application code labeled as 'EXAMPLE CODE' in your application products.
*   Example code may be used as is, in whole or in part, or may be used as a reference only. This file
*   can be modified as required.
*
*   You can find user manuals, API references, release notes and more at: https://doc.micrium.com
*
*   You can contact us at: http://www.micrium.com
*
*   Please help us continue to provide the Embedded community with the finest software available.
*
*   Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                       NETWORK CORE EXAMPLE
*                                              SOCKET
*
* File : ex_net_socket.h
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*********************************************************************************************************
*                                               MODULE
*********************************************************************************************************
*********************************************************************************************************
*/

#ifndef  EX_NET_SOCK_H
#define  EX_NET_SOCK_H


/*
*********************************************************************************************************
*********************************************************************************************************
*                                            INCLUDE FILES
*********************************************************************************************************
*********************************************************************************************************
*/

#include  <rtos/cpu/include/cpu.h>
#include  <rtos/net/include/net_type.h>


/*
*********************************************************************************************************
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                        TCP EXAMPLE FUNCTIONS
*********************************************************************************************************
*/

CPU_BOOLEAN  Ex_Net_SockTCP_Client          (CPU_CHAR      *p_ip_addr);

void         Ex_Net_SockTCP_ServerIPv4      (void);

void         Ex_Net_SockTCP_ServerIPv6      (void);

/*
*********************************************************************************************************
*                                        UDP EXAMPLE FUNCTIONS
*********************************************************************************************************
*/

CPU_BOOLEAN  Ex_Net_SockUDP_Client          (CPU_CHAR      *p_ip_addr);

void         Ex_Net_SockUDP_ServerIPv4      (void);

void         Ex_Net_SockUDP_ServerIPv6      (void);

/*
*********************************************************************************************************
*                                      SECURE EXAMPLE FUNCTIONS
*********************************************************************************************************
*/

CPU_INT16S   Ex_Net_SockSecureClientConnect (void);

CPU_INT16S   Ex_Net_SockSecureServerInit    (void);

/*
*********************************************************************************************************
*                                     MULTICAST EXAMPLE FUNCTIONS
*********************************************************************************************************
*/

void         Ex_Net_SockMCastEchoServer     (CPU_CHAR      *p_name,
                                             CPU_CHAR      *p_group_addr,
                                             NET_PORT_NBR   port);


/*
*********************************************************************************************************
*********************************************************************************************************
*                                             MODULE END
*********************************************************************************************************
*********************************************************************************************************
*/

#endif  /* EX_NET_SOCK_H */
