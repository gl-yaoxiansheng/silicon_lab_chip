MicriumOS USB host MSC example.

This example shows how to use the MicriumOS USB host stack with the
USB controller on the EFM32GG11B starter kit. This example will detect
a connected USB memory and print some information about the decive.

The output from the example application can be found by connecting a 
terminal to the VCOM port.

Board:  Silicon Labs SLSTK3701A_EFM32GG11 Starter Kit
Device: EFM32GG11B820F2048GL192
