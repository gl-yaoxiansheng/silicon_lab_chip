================ energyAware Battery Estimator ================================

This zip-file contains the energyAware Battery Estimator, a standalone 
application developed to help developers estimate the battery life time of
an application. 

================ Installation of the energyAware Designer ====================

Installation is straight-forward. Simply unzip the contents of the zip-file
to any directory and run the eABattery.exe.



================ Software updates ============================================

Silicon Labs continually works to provide updated and improved versions of the 
energyAware Battery Estimator, example code and other software of use for EFM32 
customers. Please check the download section of 

        http://www.energymicro.com/downloads/software

for the latest releases and updates. 

               (C) Copyright Silicon Laboratories, Inc 2013


================ Licenses ====================================================

DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Silicon Laboratories has no
obligation to support this Software. Silicon Laboratories is providing the
Software "AS IS", with no express or implied warranties of any kind,
including, but not limited to, any implied warranties of merchantability
or fitness for any particular purpose or warranties against infringement
of any proprietary rights of a third party.

Silicon Laboratories will not be liable for any consequential, incidental, or
special damages, or any other relief, or for any claim by any third party,
arising from your use of this Software.


The energyAware Battery Estimator uses the Qt GUI/Widget which is licensed 
under the LGPL;

       http://qt-project.org/

According to the LGPL, you have the right to receive full source code for the 
LGPL licensed licensed libraries we use. Contact us to get access to the source
code for these libraries, or alternatively, download them from their respective
project pages.

The energyAware Battery Estimator uses Qwt, which is licensed under the Qwt
License:

       http://qwt.sourceforge.net

To contact us, please go to:

http://support.energymicro.com,

or send a letter to:
Silicon Laboratories Norway
P.O.Box 4633 Nydalen
N-0405 Oslo
NORWAY

