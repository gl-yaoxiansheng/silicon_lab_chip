#!/bin/bash
pushd "`dirname \"$0\"`"/../.. > /dev/null
STUDIO=`pwd -P`
popd > /dev/null

if [ "$1" = "-studio" ]; then
	STUDIO=$2
	shift 2
fi

DATA_SEEN="false"
ECLIPSE_ARGS=
while true; do
	if [ "$1" = "-data" ]; then
		ECLIPSE_ARGS="$ECLIPSE_ARGS $1 $2"
		DATA_SEEN="true"
		shift 2
	elif [ "$1" = "-consoleLog" ]; then
		ECLIPSE_ARGS="$ECLIPSE_ARGS $1"
		shift 
	else
		break
	fi
done

if [ $DATA_SEEN = "false" ]; then
	ECLIPSE_ARGS="$ECLIPSE_ARGS -data $HOME/.studio_server"
fi

if [ -z "$CONFIG" ]; then 
	CONFIG="$STUDIO/configuration"
fi

OS=$(uname)

if [ -z "$JAVA" ]; then 
	JAVA="$JAVA_HOME/bin/java"
    if [ "$OS" = "Darwin" ]; then
        JAVA="$STUDIO/jre/Contents/Home/bin/java"
    fi
    if [ "$OS" = "Linux" ]; then
        JAVA="$STUDIO/jre/bin/java"
    fi
	if [ ! -f "$JAVA" ]; then
		JAVA=`which java`
		if [ -z "$JAVA" ] ; then
			echo "Please install Java 8 or set the JAVA_HOME environment variable first." 1>&2
			exit 1
		fi
	fi
fi

if [ -z "$VMARGS" ]; then
	VMARGS="-Xmx1G"
fi

if [ "$OS" = "Darwin" ]; then
    VMARGS="$VMARGS -XstartOnFirstThread"
fi

if [ -z "$EQUINOX" ] ; then
	EQUINOX=$STUDIO/plugins/org.eclipse.equinox.launcher_1.3.100.v20150511-1540.jar
fi 

"$JAVA" $VMARGS -jar "$EQUINOX" -configuration "$CONFIG" -install "$STUDIO" \
 	-nosplash $ECLIPSE_ARGS -application com.silabs.ss.platform.scripting.core.scriptServerApp
