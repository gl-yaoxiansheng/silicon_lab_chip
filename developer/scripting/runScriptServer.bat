@echo off

setlocal

set STUDIO=%~dp0%..\..
:getstudio
	if not "%1"=="-studio" goto nooverride
	set STUDIO=%2
	shift 
	shift 
:nooverride

set DATA_USED=
set ECLIPSE_ARGS=
:getarg
	if "%1"=="-data" goto add2
	if "%1"=="-consoleLog" goto add1
	goto end
:add1
	set ECLIPSE_ARGS=%ECLIPSE_ARGS% %1
	shift
	goto getarg
:add2
	set ECLIPSE_ARGS=%ECLIPSE_ARGS% %1 %2
	set DATA_USED=TRUE
	shift
	shift
	goto getarg
:end

if not "%DATA_USED%" == "" (
  set ECLIPSE_ARGS=%ECLIPSE_ARGS% -data %UserProfile%\studio_server
)

if "%CONFIG%" == "" ( 
	set CONFIG=%STUDIO%\configuration
)

if "%JAVA%" == "" (set JAVA=%STUDIO%\features\com.silabs.external.java.windows.x86.feature_1.8.0.92\jre\bin\java.exe)

if "%VMARGS%" == "" (set VMARGS=-Xmx768m)
if "%EQUINOX%" == "" (set EQUINOX=%STUDIO%\plugins\org.eclipse.equinox.launcher_1.3.100.v20150511-1540.jar) 

set RUN="%JAVA%" %VMARGS% -jar "%EQUINOX%" -configuration "%CONFIG%" -install "%STUDIO%" 

%RUN% -nosplash %ECLIPSE_ARGS% -application com.silabs.ss.platform.scripting.core.scriptServerApp
