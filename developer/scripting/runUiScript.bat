@echo off

setlocal

set STUDIO=%~dp0%..\..
:getstudio
	if not "%1"=="-studio" goto nooverride
	set STUDIO=%2
	shift 
	shift 
:nooverride

set ECLIPSE_ARGS=
:getarg
	if "%1"=="-data" goto add2
	if "%1"=="-consoleLog" goto add1
	goto end
:add1
	set ECLIPSE_ARGS=%ECLIPSE_ARGS% %1
	shift
	goto getarg
:add2
	set ECLIPSE_ARGS=%ECLIPSE_ARGS% %1 %2
	shift
	shift
	goto getarg
:end

rem The %* option apparently does not work... and even this hack might not work, if %9 is part of a quoted argument
:getuserargs
set USERARGS=%USERARGS% %1 %2 %3 %4 %5 %6 %7 %8 %9
shift 
shift
shift
shift
shift
shift
shift
shift
shift
if not "%1"=="" goto getuserargs

if "%CONFIG%" == "" ( 
	set CONFIG=%STUDIO%\configuration
)

if exist "%STUDIO%\features\com.silabs.external.java.windows.x86.feature_1.8.0.92" (
	set JRE_LOC=%STUDIO%\features\com.silabs.external.java.windows.x86.feature_1.8.0.92\jre\bin\java.exe
) else (
	set JRE_LOC=%STUDIO%\features\com.silabs.external.java.windows.x86_64.feature_1.8.0.92\jre\bin\java.exe
)

if "%JAVA%" == "" (set JAVA=%JRE_LOC%)

if "%VMARGS%" == "" (set VMARGS=-Xmx512m)
if "%EQUINOX%" == "" (set EQUINOX=%STUDIO%\plugins\org.eclipse.equinox.launcher_1.3.100.v20150511-1540.jar) 

set RUN="%JAVA%" %VMARGS% -jar "%EQUINOX%" -configuration "%CONFIG%" -install "%STUDIO%" 

%RUN% -nosplash %ECLIPSE_ARGS% -application com.silabs.ss.platform.scripting.ui.runUiScript %USERARGS%
